package com.evidu.bank.entitiy;

public class TemporyAccount {
// id                       
	private String CREATED_BY;
	private String MSISDN;
	private String CREATED_DATE;
	private String REF_NO;
	private String NIC;
	private String CREATED_VIA;
	private String INFO1;
	private String INFO2;
	private String STATUS;
	private String CARD_PAN;
	private String ADDRESS;
	private byte[]  USER_IMAGR;
	private byte[]  USER_NID;
	private String NAME;
	private String BANK;
	private String MOTHERS_NAME;
	private String TANSACTION_ID;
	private String ADDRESS_HOUSE_NO;
	private String ADDRESS_STREET;
	private String ADDRESS_CITY;
	private String ADDRESS_STATE;
	private String CUSTOMER_NAME;
	
	
	
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getBANK() {
		return BANK;
	}
	public void setBANK(String bANK) {
		BANK = bANK;
	}
	public String getMOTHERS_NAME() {
		return MOTHERS_NAME;
	}
	public void setMOTHERS_NAME(String mOTHERS_NAME) {
		MOTHERS_NAME = mOTHERS_NAME;
	}
	public String getTANSACTION_ID() {
		return TANSACTION_ID;
	}
	public void setTANSACTION_ID(String tANSACTION_ID) {
		TANSACTION_ID = tANSACTION_ID;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getMSISDN() {
		return MSISDN;
	}
	public void setMSISDN(String mSISDN) {
		MSISDN = mSISDN;
	}
	public String getCREATED_DATE() {
		return CREATED_DATE;
	}
	public void setCREATED_DATE(String cREATED_DATE) {
		CREATED_DATE = cREATED_DATE;
	}
	public String getREF_NO() {
		return REF_NO;
	}
	public void setREF_NO(String rEF_NO) {
		REF_NO = rEF_NO;
	}
	public String getNIC() {
		return NIC;
	}
	public void setNIC(String nIC) {
		NIC = nIC;
	}
	public String getCREATED_VIA() {
		return CREATED_VIA;
	}
	public void setCREATED_VIA(String cREATED_VIA) {
		CREATED_VIA = cREATED_VIA;
	}
	public String getINFO1() {
		return INFO1;
	}
	public void setINFO1(String iNFO1) {
		INFO1 = iNFO1;
	}
	public String getINFO2() {
		return INFO2;
	}
	public void setINFO2(String iNFO2) {
		INFO2 = iNFO2;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getCARD_PAN() {
		return CARD_PAN;
	}
	public void setCARD_PAN(String cARD_PAN) {
		CARD_PAN = cARD_PAN;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public byte[] getUSER_IMAGR() {
		return USER_IMAGR;
	}
	public void setUSER_IMAGR(byte[] uSER_IMAGR) {
		USER_IMAGR = uSER_IMAGR;
	}
	public byte[] getUSER_NID() {
		return USER_NID;
	}
	public void setUSER_NID(byte[] uSER_NID) {
		USER_NID = uSER_NID;
	}
	public String getADDRESS_HOUSE_NO() {
		return ADDRESS_HOUSE_NO;
	}
	public void setADDRESS_HOUSE_NO(String aDDRESS_HOUSE_NO) {
		ADDRESS_HOUSE_NO = aDDRESS_HOUSE_NO;
	}
	public String getADDRESS_STREET() {
		return ADDRESS_STREET;
	}
	public void setADDRESS_STREET(String aDDRESS_STREET) {
		ADDRESS_STREET = aDDRESS_STREET;
	}
	public String getADDRESS_CITY() {
		return ADDRESS_CITY;
	}
	public void setADDRESS_CITY(String aDDRESS_CITY) {
		ADDRESS_CITY = aDDRESS_CITY;
	}
	public String getADDRESS_STATE() {
		return ADDRESS_STATE;
	}
	public void setADDRESS_STATE(String aDDRESS_STATE) {
		ADDRESS_STATE = aDDRESS_STATE;
	}
	public String getCUSTOMER_NAME() {
		return CUSTOMER_NAME;
	}
	public void setCUSTOMER_NAME(String cUSTOMER_NAME) {
		CUSTOMER_NAME = cUSTOMER_NAME;
	}
	
	
}
