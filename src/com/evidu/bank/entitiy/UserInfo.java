package com.evidu.bank.entitiy;

public class UserInfo {
	/*id  
	 * CREATED_DATE 
	 *  CREATED_BY
	 *  MODIFIED_BY
	 *  MODIFIED_DATE
	 *  USER_TYPE
	 *  USER_NAME  
	 *  PASSWORD  
	 *  FIRST_NAME  
	 *  LAST_NAME   
	 *  MSISDN      
	 *  STATUS  
	 *  EMAIL_ID           
	 *  SECRET_QUES  
	 *  SECRET_ANS  
	 *  CREATE_BY_USER_NAME  
	 *  PARENT_USER_NAME  
	 *  COUNTRY_CODE  
	 *  ACCESS_ROLES  
	 *  LOGIN_DATE           
	 *  LAST_LOGIN_DATE  
	 *  EXPIRY_DATE  
	 *  DISPLAY_NAME  
	 *  INSTITUTION  
	 *  INST_ADDRESS  
	 *  ADDRESS  
	 *  HIDE_MSISDN_IN_TXN_REP  
	 *  MIN_TXN_VAL_FOR_MER_REP  
	 *  MAX_TXN_VAL_FOR_MER_REP  
	 *  INCLUDE_IN_REV_REP  
	 *  REP_USER_TYPE  
	 *  REP_AVAILABLE  
	 *  SEND_EOD_REP  
	 *  REP_TIME  
	 *  REP_ACTIVATION_DATE
	 *  WALLET_ID
	 *  BANK_NAME
	 *  BRANCH_CODE 
	 *  PARENT_USER_ID  
*/
	private String user_name;
	private String user_password;
	private String user_id;
	private String user_type;
	private String last_login_date;
	private String created_by;
	private String create_date;
	private String branch_code;
	private String display_name;
	private String status;
	
	private String imagePath;	
	private String msisdn;
	private String email_id;
	private String bank_name;
	private String parent_user_name;
	private String wallet_id;
	private String parent_user_id;
	
	
	
	
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
	public String getDisplay_name() {
		return display_name;
	}
	public void setDisplay_name(String display_name) {
		this.display_name = display_name;
	}
	public String getCreate_date() {
		return create_date;
	}
	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}
	
	public String getUser_type() {
		return user_type;
	}
	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}
	
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public String getParent_user_name() {
		return parent_user_name;
	}
	public void setParent_user_name(String parent_user_name) {
		this.parent_user_name = parent_user_name;
	}
	public String getBranch_code() {
		return branch_code;
	}
	public void setBranch_code(String branch_code) {
		this.branch_code = branch_code;
	}
	public String getWallet_id() {
		return wallet_id;
	}
	public void setWallet_id(String wallet_id) {
		this.wallet_id = wallet_id;
	}
	public String getParent_user_id() {
		return parent_user_id;
	}
	public void setParent_user_id(String parent_user_id) {
		this.parent_user_id = parent_user_id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getLast_login_date() {
		return last_login_date;
	}
	public void setLast_login_date(String last_login_date) {
		this.last_login_date = last_login_date;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	

}
