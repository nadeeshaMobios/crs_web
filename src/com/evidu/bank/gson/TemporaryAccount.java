package com.evidu.bank.gson;

public class TemporaryAccount {
	
	private String customerBank;
	private String customerName;
	private String customerMobile;
	private String customerNRIC;
	private String customerMotherName;
	private String customerNominee;
	private String customerHouseNo;
	private String customerStreet;
	private String customerCity;
	private String customerState;
	private String customerRefno;
	private String customerGender;
	private String customerReqStatus;
	private String jsonResponseStatus;
	
	public String getCustomerBank() {
		return customerBank;
	}
	public void setCustomerBank(String customerBank) {
		this.customerBank = customerBank;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerMobile() {
		return customerMobile;
	}
	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}
	public String getCustomerNRIC() {
		return customerNRIC;
	}
	public void setCustomerNRIC(String customerNRIC) {
		this.customerNRIC = customerNRIC;
	}
	public String getCustomerMotherName() {
		return customerMotherName;
	}
	public void setCustomerMotherName(String customerMotherName) {
		this.customerMotherName = customerMotherName;
	}
	public String getCustomerNominee() {
		return customerNominee;
	}
	public void setCustomerNominee(String customerNominee) {
		this.customerNominee = customerNominee;
	}
	public String getCustomerHouseNo() {
		return customerHouseNo;
	}
	public void setCustomerHouseNo(String customerHouseNo) {
		this.customerHouseNo = customerHouseNo;
	}
	public String getCustomerStreet() {
		return customerStreet;
	}
	public void setCustomerStreet(String customerStreet) {
		this.customerStreet = customerStreet;
	}
	public String getCustomerCity() {
		return customerCity;
	}
	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}
	public String getCustomerState() {
		return customerState;
	}
	public void setCustomerState(String customerState) {
		this.customerState = customerState;
	}
	public String getCustomerRefno() {
		return customerRefno;
	}
	public void setCustomerRefno(String customerRefno) {
		this.customerRefno = customerRefno;
	}
	public String getCustomerReqStatus() {
		return customerReqStatus;
	}
	public void setCustomerReqStatus(String customerReqStatus) {
		this.customerReqStatus = customerReqStatus;
	}
	public String getJsonResponseStatus() {
		return jsonResponseStatus;
	}
	public void setJsonResponseStatus(String jsonResponseStatus) {
		this.jsonResponseStatus = jsonResponseStatus;
	}
	public String getCustomerGender() {
		return customerGender;
	}
	public void setCustomerGender(String customerGender) {
		this.customerGender = customerGender;
	}
	

}
