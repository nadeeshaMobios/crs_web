package com.evidu.bank.gson;

public class ResponseTemporaryAccJson {
	private String customerMobile;
	private String customerNRIC;
	private String customerRefno;
	private String customerResponseStatus;
	public String getCustomerMobile() {
		return customerMobile;
	}
	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}
	public String getCustomerNRIC() {
		return customerNRIC;
	}
	public void setCustomerNRIC(String customerNRIC) {
		this.customerNRIC = customerNRIC;
	}
	public String getCustomerRefno() {
		return customerRefno;
	}
	public void setCustomerRefno(String customerRefno) {
		this.customerRefno = customerRefno;
	}
	public String getCustomerResponseStatus() {
		return customerResponseStatus;
	}
	public void setCustomerResponseStatus(String customerResponseStatus) {
		this.customerResponseStatus = customerResponseStatus;
	}
	
}
