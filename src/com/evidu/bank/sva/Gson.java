
package com.evidu.bank.sva;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.codec.binary.Base64;


public class Gson {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Connection con = null;
		
		String bytesEncoded="iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAIAAAC0Ujn1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAxhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDM0MiwgMjAxMC8wMS8xMC0xODowNjo0MyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpDRTM5OTY1RDVCQzUxMUUxOERGNjg4QzY2NDA5NTUzQiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpDRTM5OTY1RTVCQzUxMUUxOERGNjg4QzY2NDA5NTUzQiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkNFMzk5NjVCNUJDNTExRTE4REY2ODhDNjY0MDk1NTNCIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkNFMzk5NjVDNUJDNTExRTE4REY2ODhDNjY0MDk1NTNCIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+zeTTgQAAA4dJREFUeNqklr9rFEEUx+e9nb3bu1PvTmNiNNGEkGACitgptgqCiiDaWGgh+gf4H9hZ2CqIrWhpYaGIIlZioZUoKaIQQhKJIZefd7vzw/dm9hK9JMt5mcxO9mbnfea735mdGTjz+AWghAABAwEAiFwCCEG5rWSFBctJWEN/1lCprdaSiCADLgMqkdiCMjIX2qUzF5hrhDFgjNEcKB2R6BKR0YJLYHq7olm241rAVC8KQeolKw0CdKUXTlwuWTa0w01Vsw+E5xjyhrqRHuTsJrRsop3XW6DtpjGwTjWxtWA3uR9AKxBl6i/Tndc0mFzjuRsUbW1Cstx9jsPgH6/JYj86/AIBO0NtGOFluhvXhx/MNJhwDW268rmxSuFoOZpvqE9zK5MrjVzgXXPvjzQ7ONpaZN+dOMkCEZq2uHnnZ4hzTVsekFsjPecPlvfmpUfdVObNdO3R+C/qlZ0jQwwynaxgL1gj3cl0sFKJXrvnAocIe2ek+8rh6t/uliRe7qcaeDg+GzCFeqC384Ecah0RU6KfD01/3QXK2sOlXAt3PV3urxyrFhvm3+kP6S/f3baJ0H3FXEaDkd0RtdnuKWZ+ZKIcBqLThNmPM0RRmmso7ARtefRHy1EG+kgp16FqGpKeKMxo0B2FthM00IwWb2eWMtCf51cylpks1VGA72cXn/+c3/Lp0x+/P/xajhA7MoSG0djX04ubB7Ou7cupWsIrXaczJESkRWN2TbXUT642aokOETqffBS7pPT3xXpL/cRSvKYM7GRe88pnxY/lRkvldD22//HJ2PRa3zrSr4bYwm4KA1e/0XzzrdsW3E4hNkrOdCljVhLdG8nT+3e1oE9UC/3FsK40ZeMAPsrDfBdyvba5K9PyLWhLrlu7Lx+e691zY2h/V3OlXk/Hq8UnpwbfzdReTS1MLNZXlc4DHxmEOy34g4OkO3C/wfipBGvalPPhhUN7rw50HShs+zXSqn2xr0r528Las4nZjzO1WKlQ8N7hhUr+ZzyX0bE2lwZ7rg/3HCjk2lzhRiuFeycHxmurD778/DpXi4CBvJVZv8lrTUeI1UZ89lD17vH+9rkba3e5eP/UMHVTjxPBpxGD/shDXJWoisTbY30dL9CVfHhtuFcpZTSp1oTWRnOOk3hod767mBc7SKcP7usr5RKV0OkM2QqtrFJJHB+t7hI7S6VQDu0pxnFM2D8CDAB6C+Fr8bTMIQAAAABJRU5ErkJggg==";
		byte[] valueDecoded= Base64.decodeBase64(bytesEncoded );
		System.out.println("Decoded value is " + new String(valueDecoded));
		
		try {
			DatabaseController d = DatabaseController.getInstance();	
			con=(Connection)d.getConnection();	
			
		    String sql = "INSERT INTO account_user_image (ACC_NO, NIC_IMAGE) values (?, ?)";
		    PreparedStatement statement = con.prepareStatement(sql);
		    statement.setString(1, "123");
		    
		        statement.setBytes(2, valueDecoded);
		  
		    int row = statement.executeUpdate();
		    
		} catch (SQLException e) {
			System.out.println("Error On insert user Image Data : "+e.toString());
		}
		finally{  
			if (con != null) { 
				try { 
					con.close(); 
				}catch (SQLException e) {
					
				}
				con = null;
			} 
		}
		
	}

}
