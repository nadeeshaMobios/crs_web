package com.evidu.bank.sva;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.evidu.bank.entitiy.UserInfo;


public class loginController {
	
	public UserInfo loginUser(String SQL)  {	
		UserInfo userData =new UserInfo();
		Connection con = null;  
		Statement st = null;
		try {
			DatabaseController d = DatabaseController.getInstance();
			con=d.getConnection();
			ResultSet rs = null;
			st = (Statement)con.createStatement();			
			rs = st.executeQuery(SQL);
			while( rs.next() ){
					userData.setUser_id(rs.getString("ID"));
					userData.setUser_name(rs.getString("USER_NAME"));
					userData.setUser_password(rs.getString("PASSWORD"));
					userData.setDisplay_name(rs.getString("DISPLAY_NAME"));
					userData.setUser_type(rs.getString("USER_TYPE"));
					userData.setBranch_code(rs.getString("BRANCH_CODE"));
					userData.setCreate_date(rs.getString("CREATED_DATE"));
					userData.setStatus(rs.getString("STATUS"));
					userData.setLast_login_date(rs.getString("LAST_LOGIN_DATE"));
					
			} 
			
		} catch (Exception e) {
			System.out.println("Error On loginUser java : "+e.toString());
		}
		finally{  
			if (con != null){  
				try { 
					con.close(); 
                } catch (SQLException e) {
        	    }
	            con = null;
	          }  
		}
		return userData;
	}

}
