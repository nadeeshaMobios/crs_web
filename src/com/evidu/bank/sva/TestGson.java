package com.evidu.bank.sva;

import com.evidu.bank.entitiy.TemporyAccount;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class TestGson {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TemporyAccount temp_acc = new TemporyAccount();
		temp_acc.setMSISDN("12345678");
		temp_acc.setADDRESS("sssss");
		temp_acc.setCARD_PAN("2345677");
		temp_acc.setNAME("test customer");
		
		 
		Gson gson = new Gson();
		 
		System.out.println(gson.toJson(temp_acc));
	}

}
