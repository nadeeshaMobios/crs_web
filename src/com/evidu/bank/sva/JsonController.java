package com.evidu.bank.sva;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

import com.evidu.bank.entitiy.Account;
import com.evidu.bank.gson.TemporaryAccount;

public class JsonController {
	
public TemporaryAccount getAccountDetail(String SQL)  {	
		
	TemporaryAccount Acc = new TemporaryAccount();
		Connection con = null;  
		Statement st = null;
		
		try {
			DatabaseController d = DatabaseController.getInstance();
			con=d.getConnection();
			ResultSet rs = null;
			st = (Statement)con.createStatement();			
			rs = st.executeQuery(SQL);
			System.out.println("getAccountDetail SQL "+SQL);
			while( rs.next() ){
				Acc.setCustomerMobile(""+rs.getString("MSISDN")) ;
				Acc.setCustomerNRIC(""+rs.getString("NIC"));
				Acc.setCustomerRefno(""+rs.getString("REF_NO"));
				Acc.setCustomerReqStatus(""+rs.getString("STATUS"));
				
			} 
		} catch (Exception e) {
			System.out.println("Error On account details : "+e.toString());
		}
		finally{  
			if (con != null){  
				try { 
					con.close(); 
               } catch (SQLException e) {
       	    }
	            con = null;
	          }  
		}
		return Acc;
	}

public String getRandomRefNo(){
	
	boolean b = true;
	
	double random=Math.random()*100000;
	int randomd=(int) random;
	
   String SQL="SELECT * FROM tempory_account WHERE REF_NO='"+randomd+"'";
   System.out.println("randomd :"+randomd);
   System.out.println("SQL :"+SQL);
   String returnRefNo=""; 
   System.out.println("b 11 :"+b);
   
   returnRefNo=checkRefNumberIsAvailable(SQL);
   while(b== true){
	   System.out.println("returnRefNo ee :"+returnRefNo);
	   
	   if(returnRefNo.equals("")){
		   System.out.println("if b :"+b);
		   b=false;
	   }else{
		  b=true;
		  System.out.println("else b :"+b);
	   }
   }
   
	return ""+randomd;
	
}

public String checkRefNumberIsAvailable(String SQL)  {	
	
	Connection con = null;  
	Statement st = null;
	String refNo="";
	try {
		DatabaseController d = DatabaseController.getInstance();
		con=d.getConnection();
		ResultSet rs = null;
		st = (Statement)con.createStatement();			
		rs = st.executeQuery(SQL);
		
		while( rs.next() ){
			refNo=""+rs.getString("REF_NO");
			
		} 
		
	} catch (Exception e) {
		System.out.println("Error On Check Ref number availability : "+e.toString());
	}
	finally{  
		if (con != null){  
			try { 
				con.close(); 
           } catch (SQLException e) {
   	    }
           con = null;
         }  
	}
	return refNo;
}



}
