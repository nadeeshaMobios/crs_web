package com.evidu.bank.sva;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.evidu.bank.entitiy.Agent;
import com.evidu.bank.entitiy.DSR;
import com.evidu.bank.entitiy.Dealer;

public class AgentController {
	
	/*public ArrayList<DSR> getDSRDetail(String id)  {	
	String SQL="SELECT * FROM dsr_users WHERE PARENT_DEALER='"+id+"'";
	ArrayList<DSR> dsrList = new ArrayList<DSR>();
		Connection con = null;  
		Statement st = null;
		
		try {
			DatabaseController d = DatabaseController.getInstance();
			con=d.getConnection();
			ResultSet rs = null;
			st = (Statement)con.createStatement();			
			rs = st.executeQuery(SQL);
			System.out.println("SQL "+SQL);
			while( rs.next() ){
				DSR dsr = new DSR();
				dsr.setMobileNumber(""+rs.getString("MSISDN")) ;
				dsr.setName(""+rs.getString("NAME"));
				dsrList.add(dsr);
				
			} 
		} catch (Exception e) {
			System.out.println("Error On Get DSR List : "+e.toString());
		}
		finally{  
			if (con != null){  
				try { 
					con.close(); 
               } catch (SQLException e) {
       	    }
	            con = null;
	          }  
		}
		return dsrList;
	}*/

	
	public Agent getAgentDetail(String SQL)  {	
		
		Agent agt = new Agent();
		Connection con = null;  
		Statement st = null;
		
		try {
			DatabaseController d = DatabaseController.getInstance();
			con=d.getConnection();
			ResultSet rs = null;
			st = (Statement)con.createStatement();			
			rs = st.executeQuery(SQL);
			System.out.println("SQL "+SQL);
			while( rs.next() ){
				agt.setMobileNumber(rs.getString("AGENT_MOBILE"));
			} 
		} catch (Exception e) {
			System.out.println("Error On get agent details details : "+e.toString());
		}
		finally{  
			if (con != null){  
				try { 
					con.close(); 
	           } catch (SQLException e) {
	   	    }
	            con = null;
	          }  
		}
		return agt;
	}
	
public DSR getDSRDetail(String SQL)  {	
		
		DSR ds = new DSR();
		Connection con = null;  
		Statement st = null;
		
		try {
			DatabaseController d = DatabaseController.getInstance();
			con=d.getConnection();
			ResultSet rs = null;
			st = (Statement)con.createStatement();			
			rs = st.executeQuery(SQL);
			System.out.println("SQL "+SQL);
			while( rs.next() ){
				//agt.setMobileNumber(rs.getString("AGENT_MOBILE"));
				ds.setMobileNumber(rs.getString("AGENT_MOBILE"));
			} 
		} catch (Exception e) {
			System.out.println("Error On get agent details details : "+e.toString());
		}
		finally{  
			if (con != null){  
				try { 
					con.close(); 
	           } catch (SQLException e) {
	   	    }
	            con = null;
	          }  
		}
		return ds;
	}
	
	public boolean insertImage(String SQL,Agent agt,InputStream inputStream1,InputStream inputStream2,InputStream inputStream3,InputStream inputStream4,InputStream inputStream5,InputStream inputStream6,InputStream inputStream7,String user){
		  
		  System.out.println("SQL : "+SQL);
		  System.out.println("city : "+agt.getOutlet_city());
		  System.out.println("state : "+agt.getOutlet_state());
		  System.out.println("street : "+agt.getOutlet_street());
		  
		  System.out.println("inputStream1 : "+inputStream1);
		  System.out.println("inputStream2 : "+inputStream2);
		  System.out.println("inputStream3 : "+inputStream3);
		  System.out.println("inputStream4 : "+inputStream4);
		  System.out.println("inputStream5 : "+inputStream5);
		  System.out.println("inputStream6 : "+inputStream6);
		  System.out.println("inputStream7 : "+inputStream7);
		  System.out.println("user : "+user);
		  
		  Connection con = null;
			PreparedStatement statement;
			Date date = new Date();
			//Dealer deal = new Dealer();
			SimpleDateFormat format = new SimpleDateFormat();
			format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String DateToStr = format.format(date);
			
			System.out.println(DateToStr);
			boolean st=false;
			try {
				DatabaseController d = DatabaseController.getInstance();	
				con=(Connection)d.getConnection();	
				statement = con.prepareStatement(SQL);
				    statement.setString(1, user);
				    statement.setString(2, DateToStr);
				    statement.setString(3, agt.getDealer_org_name());
				    statement.setString(4, agt.getDealer_mobile());
				    statement.setString(5, agt.getDealer_name());
				    statement.setString(6, agt.getRegion());
				    statement.setString(7, agt.getArea());
				    statement.setString(8, agt.getTerritory());
				    statement.setString(9, agt.getOutlet_name());
				    statement.setString(10, agt.getOutlet_home());
				    statement.setString(11, agt.getOutlet_city());
				    statement.setString(12, agt.getOutlet_state());
				    statement.setString(13, agt.getOutlet_street());
				    statement.setString(14, agt.getOutlet_district());
				    statement.setString(15, agt.getOutlet_thana());
				    statement.setString(16, agt.getOutlet_union());
				    statement.setString(17, agt.getOutlet_postCode());
				    statement.setString(18, agt.getOutlet_division());
				    statement.setString(19, agt.getAgentName());
				    statement.setString(20, agt.getDateofbirth());
				    statement.setString(21, agt.getIdType());
				    statement.setString(22, agt.getIdNumber());
				    statement.setString(23, agt.getAgentHome());
				    statement.setString(24, agt.getAgentCity());
				    statement.setString(25, agt.getAgentStreet());
				    statement.setString(26, agt.getAgentstate());
				    statement.setString(27, agt.getAgentDistric());
				    statement.setString(28, agt.getAgentUnion());
				    statement.setString(29, agt.getAgentThana());
				    statement.setString(30, agt.getAgentDivision());
				    statement.setString(31, agt.getAgentPostCode());
				    statement.setString(32, agt.getMobileNumber());
				    statement.setString(33, agt.getOwnershipType());
				    statement.setString(34, agt.getEmergencyContactNo());
				    statement.setString(35, agt.getEmergencyName());
				    statement.setString(36, agt.getRelataion());
				    statement.setString(37, agt.getEmergencyHome());
				    statement.setString(38, agt.getEmergencyCity());
				    statement.setString(39, agt.getEmergencyStreet());
				    statement.setString(40, agt.getEmergencyState());
				    statement.setString(41, agt.getEmergencyDistrict());
				    statement.setString(42, agt.getEmergencyThana());
				    statement.setString(43, agt.getEmergencyUnion());
				    statement.setString(44, agt.getTradeLicences());
				    statement.setString(45, agt.getTinNo());
				    statement.setString(46, agt.getVatRegNo());
				    statement.setString(47, agt.getAccountNo());
				    statement.setString(48, agt.getBankName());
				    statement.setString(49, agt.getBranchName());
				    statement.setString(50, agt.getNomineeName());
				    statement.setString(51, agt.getNomineeRelation());
				    statement.setString(52, agt.getNomineeMobile());
				    statement.setString(53, agt.getNomineeFather());
				    statement.setString(54, agt.getNomineeMother());
				    statement.setString(55, agt.getNomineeIdType());
				    statement.setString(56, agt.getNomneeIdNo());
				    statement.setString(57, agt.getNomineeHome());
				    statement.setString(58, agt.getNomineeCity());
				    statement.setString(59, agt.getNomineeState());
				    statement.setString(60, agt.getNomineeStreet());
				    statement.setString(61, agt.getNomineeeDistrict());
				    statement.setString(62, agt.getNomineethana());
				    statement.setString(63, agt.getNomineeUnion());
				    statement.setString(64, agt.getTerminalId());
				    statement.setString(65, agt.getCardPan());
				    statement.setBlob(66, inputStream1);
				    statement.setBlob(67, inputStream2);
				    statement.setBlob(68, inputStream3);
				    statement.setBlob(69, inputStream4);
				    statement.setBlob(70, inputStream5);
				    statement.setBlob(71, inputStream6);
				    statement.setBlob(72, inputStream7);
				    
				    // sends the statement to the database server
				    int row = statement.executeUpdate();
				    st=true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally{  
				if (con != null){  
					try { 
						con.close(); 
	                } catch (SQLException e) {
	        	    }
		            con = null;
		          }  
			}
		   
			return st;
		}
	
	public boolean insertImageDSR(String SQL,Agent agt,InputStream inputStream1,InputStream inputStream2,InputStream inputStream3,InputStream inputStream4,InputStream inputStream5,InputStream inputStream6,InputStream inputStream7,String user){
		  
		  System.out.println("SQL : "+SQL);
		  System.out.println("city : "+agt.getOutlet_city());
		  System.out.println("state : "+agt.getOutlet_state());
		  System.out.println("street : "+agt.getOutlet_street());
		  System.out.println("inputStream1 : "+inputStream1);
		  System.out.println("inputStream2 : "+inputStream2);
		  System.out.println("inputStream3 : "+inputStream3);
		  System.out.println("inputStream4 : "+inputStream4);
		  System.out.println("inputStream5 : "+inputStream5);
		  System.out.println("inputStream6 : "+inputStream6);
		  System.out.println("inputStream7 : "+inputStream7);
		  System.out.println("user : "+user);
		  
		  Connection con = null;
			PreparedStatement statement;
			Date date = new Date();
			//Dealer deal = new Dealer();
			SimpleDateFormat format = new SimpleDateFormat();
			format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String DateToStr = format.format(date);
			
			System.out.println(DateToStr);
			boolean st=false;
			try {
				DatabaseController d = DatabaseController.getInstance();	
				con=(Connection)d.getConnection();	
				statement = con.prepareStatement(SQL);
				statement.setString(1, user);
			    statement.setString(2, DateToStr);
			    statement.setString(3, agt.getDealer_org_name());
			    statement.setString(4, agt.getDealer_mobile());
			    statement.setString(5, agt.getDealer_name());
			    statement.setString(6, agt.getRegion());
			    statement.setString(7, agt.getArea());
			    statement.setString(8, agt.getTerritory());
			    statement.setString(9, agt.getOutlet_name());
			    statement.setString(10, agt.getOutlet_home());
			    statement.setString(11, agt.getOutlet_city());
			    statement.setString(12, agt.getOutlet_state());
			    statement.setString(13, agt.getOutlet_street());
			    statement.setString(14, agt.getOutlet_district());
			    statement.setString(15, agt.getOutlet_thana());
			    statement.setString(16, agt.getOutlet_union());
			    statement.setString(17, agt.getOutlet_postCode());
			    statement.setString(18, agt.getOutlet_division());
			    statement.setString(19, agt.getAgentName());
			    statement.setString(20, agt.getDateofbirth());
			    statement.setString(21, agt.getIdType());
			    statement.setString(22, agt.getIdNumber());
			    statement.setString(23, agt.getAgentHome());
			    statement.setString(24, agt.getAgentCity());
			    statement.setString(25, agt.getAgentStreet());
			    statement.setString(26, agt.getAgentstate());
			    statement.setString(27, agt.getAgentDistric());
			    statement.setString(28, agt.getAgentUnion());
			    statement.setString(29, agt.getAgentThana());
			    statement.setString(30, agt.getAgentDivision());
			    statement.setString(31, agt.getAgentPostCode());
			    statement.setString(32, agt.getMobileNumber());
			    statement.setString(33, agt.getOwnershipType());
			    statement.setString(34, agt.getEmergencyContactNo());
			    statement.setString(35, agt.getEmergencyName());
			    statement.setString(36, agt.getRelataion());
			    statement.setString(37, agt.getEmergencyHome());
			    statement.setString(38, agt.getEmergencyCity());
			    statement.setString(39, agt.getEmergencyStreet());
			    statement.setString(40, agt.getEmergencyState());
			    statement.setString(41, agt.getEmergencyDistrict());
			    statement.setString(42, agt.getEmergencyThana());
			    statement.setString(43, agt.getEmergencyUnion());
			    statement.setString(44, agt.getTradeLicences());
			    statement.setString(45, agt.getTinNo());
			    statement.setString(46, agt.getVatRegNo());
			    statement.setString(47, agt.getAccountNo());
			    statement.setString(48, agt.getBankName());
			    statement.setString(49, agt.getBranchName());
			    statement.setString(50, agt.getNomineeName());
			    statement.setString(51, agt.getNomineeRelation());
			    statement.setString(52, agt.getNomineeMobile());
			    statement.setString(53, agt.getNomineeFather());
			    statement.setString(54, agt.getNomineeMother());
			    statement.setString(55, agt.getNomineeIdType());
			    statement.setString(56, agt.getNomneeIdNo());
			    statement.setString(57, agt.getNomineeHome());
			    statement.setString(58, agt.getNomineeCity());
			    statement.setString(59, agt.getNomineeState());
			    statement.setString(60, agt.getNomineeStreet());
			    statement.setString(61, agt.getNomineeeDistrict());
			    statement.setString(62, agt.getNomineethana());
			    statement.setString(63, agt.getNomineeUnion());
			    statement.setString(64, agt.getTerminalId());
			    statement.setString(65, agt.getCardPan());
			    statement.setBlob(66, inputStream1);
			    statement.setBlob(67, inputStream2);
			    statement.setBlob(68, inputStream3);
			    statement.setBlob(69, inputStream4);
			    statement.setBlob(70, inputStream5);
			    statement.setBlob(71, inputStream6);
			    statement.setBlob(72, inputStream7);
				    
				    // sends the statement to the database server
				    int row = statement.executeUpdate();
				    st=true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally{  
				if (con != null){  
					try { 
						con.close(); 
	                } catch (SQLException e) {
	        	    }
		            con = null;
		          }  
			}
		   
			return st;
		}

/*public ArrayList<Agent> getAgentDetail(String id)  {	
	String SQL="SELECT * FROM agent_users WHERE CREATED_BY='"+id+"'";
	ArrayList<Agent> agentList = new ArrayList<Agent>();
		Connection con = null;  
		Statement st = null;
		
		try {
			DatabaseController d = DatabaseController.getInstance();
			con=d.getConnection();
			ResultSet rs = null;
			st = (Statement)con.createStatement();			
			rs = st.executeQuery(SQL);
			System.out.println("SQL "+SQL);
			while( rs.next() ){
				Agent agent = new Agent();
				agent.setMobileNumber(""+rs.getString("MSISDN")) ;
				agent.setName(""+rs.getString("NAME"));
				agent.setCreatedBy(""+rs.getString("CREATED_BY"));
				agentList.add(agent);
				//System.out.println("agent:"+agent.getMobileNumber());
			} 
		} catch (Exception e) {
			System.out.println("Error On Get DSR List : "+e.toString());
		}
		finally{  
			if (con != null){  
				try { 
					con.close(); 
               } catch (SQLException e) {
       	    }
	            con = null;
	          }  
		}
		return agentList;
	}*/
}
