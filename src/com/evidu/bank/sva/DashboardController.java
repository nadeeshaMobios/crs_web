package com.evidu.bank.sva;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.evidu.bank.entitiy.*;
import com.mysql.jdbc.Connection;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DashboardController {
	
	public UserInfo getlogCurrentUserDetails(String SQL){
			
			UserInfo uInfo = new UserInfo();
			Connection con = null;
			Statement stmt = null;
			ResultSet rs = null;
			
			try {
				DatabaseController dc = DatabaseController.getInstance();
				con = (Connection) dc.getConnection();
	
				stmt = con.createStatement();
	
				rs = stmt.executeQuery(SQL);
				
				while(rs.next()) {
					
					uInfo.setUser_id(rs.getString("ID"));
					uInfo.setUser_name(rs.getString("USER_NAME"));
					uInfo.setUser_password(rs.getString("PASSWORD"));
					uInfo.setDisplay_name(rs.getString("DISPLAY_NAME"));
					uInfo.setUser_type(rs.getString("USER_TYPE"));
					uInfo.setBranch_code(rs.getString("BRANCH_CODE"));
					uInfo.setCreated_by(rs.getString("CREATED_BY"));
					uInfo.setCreate_date(rs.getString("CREATED_DATE"));
					uInfo.setStatus(rs.getString("STATUS"));
					/*uInfo.setLast_login_date(rs.getString("LAST_LOGIN_DATE"));*/
					}
				
			} catch (Exception e) {
				System.out.println("Error in get log Current User Details(String sql) method"+e);
				e.printStackTrace();
			}
	
			// to close DB connection
			finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						System.out.println("error in connection close "+e);
					}
					con = null;
				}
			}
			
			return uInfo;
		}
	
	public static List<Integer> getSummeryFromMysqlDB(List<String> querylist){

	    List<Integer> countlist = new ArrayList<Integer>();
	    Connection connection = null;
			DatabaseController d = DatabaseController.getInstance();
			try {
				connection = (Connection) d.getConnection();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

	   Statement stmt = null;
	   ResultSet rs=null;

	   try {
	        stmt = (Statement) connection.createStatement();

	        for(String SQL :querylist){
	        rs = stmt.executeQuery(SQL);
	        while (rs.next()){
	           int count=Integer.parseInt(rs.getString("count"));
	            countlist.add(count);

	        }
	    }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	finally{
	        try {
	            connection.close();
	            stmt.close();
	        }catch (SQLException e) {

	            e.printStackTrace();
	        }
	}

	return countlist;

	}
	
	public List<Account> getPendingCustomerListByTapNPay(){
		String SQL="SELECT id,ACCOUNT_NAME,NAME,MOBILE_NUMBER,STATUS FROM account WHERE  STATUS='PENDING' AND BRANCH_MAKER_STATUS IS NULL AND BRANCH_CHECKER_STATUS IS NULL";
		List<Account> customerPending = new ArrayList<Account>();
		customerPending=getPendingCustomerList(SQL);
		return customerPending;
	}
	
	public List<Account> getPendingCustomerList(String SQL){
		
	  	List<Account> customerPending = new ArrayList<Account>();
        Connection connection = null;
        DatabaseController d = DatabaseController.getInstance();
        try {
			connection = (Connection) d.getConnection();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
        Statement stmt = null;
        ResultSet rs=null;
        
        try {
        	System.out.println("getPendingCustomerList SQL : "+SQL);
            stmt = (Statement) connection.createStatement();
                rs = stmt.executeQuery(SQL);
                while (rs.next()){
                	//UrlTotal urlTot = new UrlTotal();
                	Account acc = new Account();
                	acc.setID(rs.getString("id"));
                	acc.setMOTHER_NAME(rs.getString("MOTHER_NAME"));
                	acc.setNAME(rs.getString("NAME"));
                	acc.setMOBILE_NUMBER(rs.getString("MOBILE_NUMBER"));
                	acc.setSTATUS(rs.getString("STATUS"));
                	acc.setGENDER(rs.getString("GENDER"));
                	customerPending.add(acc);
                	
                }
                }catch (Exception e) {
                	System.out.println("** getApporvalList **"+e.toString());
					e.printStackTrace();
                }
        finally{
        	if ( connection != null ){
				try{ 
					connection.close(); 
				}catch(Exception ce){
					ce.printStackTrace();
					}
			}
        }
		return customerPending;
	}
	
	 public List<Account> getAllCustomerList(String SQL){
			
		  	List<Account> allCustomerList = new ArrayList<Account>();
	        Connection connection = null;
	        DatabaseController d = DatabaseController.getInstance();
	        try {
				connection = (Connection) d.getConnection();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
	        Statement stmt = null;
	        ResultSet rs=null;
	        
	        try {
	            stmt = (Statement) connection.createStatement();
	                rs = stmt.executeQuery(SQL);
	                while (rs.next()){
	                	
	                	Account customerAccount = new Account();
	                	
	                	customerAccount.setID(rs.getString("id"));
	                	//customerAccount.setAccountName(rs.getString("ACCOUNT_NAME"));
	                	customerAccount.setMOBILE_NUMBER(rs.getString("MOBILE_NUMBER"));
	                	customerAccount.setNAME(rs.getString("NAME"));
	                	customerAccount.setFATHER_NAME(rs.getString("FATHER_NAME"));
	                	customerAccount.setMOTHER_NAME(rs.getString("MOTHER_NAME"));
	                	customerAccount.setSPOUSE_NAME(rs.getString("SPOUSE_NAME"));
	                	customerAccount.setDATE_OF_BIRTH(rs.getString("DATE_OF_BIRTH"));
	                	customerAccount.setPROFESSION(rs.getString("PROFESSION"));
	                	customerAccount.setOTHER_ACC_BANK_NAME(rs.getString("OTHER_ACC_BANK_NAME"));
	                	customerAccount.setOTHER_ACC_NUMBER(rs.getString("OTHER_ACC_NUMBER"));
	                	customerAccount.setOTHER_BRANCH_NAME(rs.getString("OTHER_BRANCH_NAME"));
	                	customerAccount.setID_TYPE(rs.getString("ID_TYPE"));
	                	customerAccount.setEMERGENCY_CONTACT_NO(rs.getString("EMERGENCY_CONTACT_NO"));
	                	customerAccount.setTIN_NO(rs.getString("TIN_NO"));
	                	customerAccount.setNATIONAL_ID(rs.getString("NATIONAL_ID"));
	                	customerAccount.setMAILING_CITY(rs.getString("MAILING_ADDRESS_HOUSE_NO"));
	                	customerAccount.setMAILING_STATE(rs.getString("MAILING_ADDRESS_STATE"));
	                	customerAccount.setMAILING_HOUSE_NO(rs.getString("MAILING_ADDRESS_CITY"));
	                	customerAccount.setMAILING_STREET(rs.getString("MAILING_ADDRESS_STREET"));
	                	customerAccount.setMAILING_DISTRICT(rs.getString("MAILING_ADDRESS_DISTRICT"));
	                	customerAccount.setMAILING_THANA(rs.getString("MAILING_ADDRESS_THANA"));
	                	customerAccount.setMAILING_UNION(rs.getString("MAILING_ADDRESS_UNION"));
	                	customerAccount.setMAILING_DIVISION(rs.getString("MAILING_ADDRESS_DIVISION"));
	                	customerAccount.setMAILING_POST_CODE(rs.getString("MAILING_ADDRESS_POSTCODE"));
	                	
	                	customerAccount.setPERMANT_CITY(rs.getString("PERMANENT_ADDRESS_CITY"));
	                	customerAccount.setPERMANT_STATE(rs.getString("PERMANENT_ADDRESS_STATE"));
	                	customerAccount.setPERMANT_HOUSE_NO(rs.getString("PERMANENT_ADDRESS_HOUSE_NO"));
	                	customerAccount.setPERMANT_STREET(rs.getString("PERMANENT_ADDRESS_STREET"));
	                	customerAccount.setPERMANT_DISTRICT(rs.getString("PERMANENT_ADDRESS_DISTRICT"));
	                	customerAccount.setPERMANT_THANA(rs.getString("PERMANENT_ADDRESS_THANA"));
	                	customerAccount.setPERMANT_UNION(rs.getString("PERMANENT_ADDRESS_UNION"));
	                	customerAccount.setPERMANT_DIVISION(rs.getString("PERMANENT_ADDRESS_DIVISION"));
	                	customerAccount.setPERMANT_POST_CODE(rs.getString("PERMANENT_ADDRESS_POSTCODE"));
	                	
	                	customerAccount.setNOMINEE_NAME(rs.getString("NOMINEE_NAME"));
	                	customerAccount.setNOMINEE_ID_TYPE(rs.getString("NOMINEE_ID_TYPE"));
	                	customerAccount.setNOMINEE_ID_NO(rs.getString("NOMINEE_NID"));
	                	customerAccount.setNOMINEE_FATHERS_NAME(rs.getString("NOMINEE_FATHERS_NAME"));
	                	customerAccount.setNOMINEE_MOTHERS_NAME(rs.getString("NOMINEE_MOTHERS_NAME"));
	                	customerAccount.setNOMINEE_CONTACT_NO(rs.getString("NOMINEE_CONTACT_NO"));
	                	
	                	customerAccount.setNOMINEE_CITY(rs.getString("NOMINEE_ADDRESS_CITY"));
	                	customerAccount.setNOMINEE_STATE(rs.getString("NOMINEE_ADDRESS_STATE"));
	                	customerAccount.setNOMINEE_HOUSE_NO(rs.getString("NOMINEE_ADDRESS_HOUSE_NO"));
	                	customerAccount.setNOMINEE_STREET(rs.getString("NOMINEE_ADDRESS_STEET"));
	                	customerAccount.setNOMINEE_DISTRICT(rs.getString("NOMINEE_DISTRICT"));
	                	customerAccount.setNOMINEE_THANA(rs.getString("NOMINEE_THANA"));
	                	customerAccount.setNOMINEE_UNION(rs.getString("NOMINEE_UNION"));
	                	
	                	
	                	
	                	allCustomerList.add(customerAccount);
	                	
	                }
	                }catch (Exception e) {
	                	System.out.println("**getAllCustomerList**"+e.toString());
						e.printStackTrace();
	                }
	        finally{
	        	if ( connection != null ){
					try{ 
						connection.close(); 
					}catch(Exception ce){
						ce.printStackTrace();
						}
				}
	        }
			return allCustomerList;
	 }
	 
	 
	 public byte[] getImageData(String query){
		 byte[] imgData = null;
		 Connection con = null;
		 Statement stmt = null;
		 ResultSet rs = null;
		 Blob image = null;
		 boolean q_update_status=false;

		 try {

		  DatabaseController d = DatabaseController.getInstance(); 
		  con=(Connection)d.getConnection(); 
		  
		  stmt = con.createStatement();

		  rs = stmt.executeQuery(query);

		  if (rs.next()) {
		  
		   image = rs.getBlob(1);//blob
		   
		   //inputStream=image.getBinaryStream();
		   imgData = image.getBytes(1, (int) image.length()); //array
		  }
		 }catch( Exception e){
		  e. printStackTrace();
		 }finally {
		   try {
		   rs.close();
		   stmt.close();
		   con.close();
		  } catch (SQLException e) {

		   e.printStackTrace();

		  }
		 }
		 return imgData;
		}
	 
	 public boolean checkboxUpdate(String sql) {
			
			Connection con=null;
			boolean b= false;
			
			try {
				DatabaseController dc = DatabaseController.getInstance();
				try {
					con = (Connection) dc.getConnection();
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				Statement stmt=con.createStatement();
				stmt.executeUpdate(sql);
				b=true;
			
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			finally {
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						System.out.println("error in connection close");
					}
					con = null;
				}
			}
			
			return b;
			
		}
	
	

}
