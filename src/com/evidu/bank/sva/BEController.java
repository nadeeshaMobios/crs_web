package com.evidu.bank.sva;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.evidu.bank.entitiy.Account;
import com.evidu.bank.entitiy.BranchExecutive;
import com.evidu.bank.entitiy.UserInfo;

public class BEController {
	
	public boolean insertBEUserData(String SQL,InputStream inputStream,InputStream inputStream2,BranchExecutive brExecutive,String userId){
		Connection con = null;
		PreparedStatement statement;
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat();
		format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String DateToStr = format.format(date);
		//INSERT INTO maker_checker_users 
		/*(CREATED_DATE,
		 *  CREATED_BY,
		 *  USER_TYPE,
		 *  USER_NAME,
		 *  PASSWORD,
		 *  MSISDN,
		 *  STATUS,
		 *  CREATE_BY_USER_NAME,
		 *  DISPLAY_NAME,
		 *  BRANCH_CODE,
		 *  PERMENANT_ADDRESS
		 *  ,MAILING_ADDRESS
		 *  ,NID_IMAGE,
		 *  USER_IMAGE) values (? ,? ,? ,? ,? ,?,? ,?,?,?,?,?,?,?)
		*/System.out.println(DateToStr);
		boolean st=false;
		try {
			DatabaseController d = DatabaseController.getInstance();	
			con=(Connection)d.getConnection();	
			statement = con.prepareStatement(SQL);
			statement.setString(1, DateToStr);
			statement.setString(2, userId);
			statement.setString(3, brExecutive.getBe_role());
			statement.setString(4, brExecutive.getBe_mobile_number());
			statement.setString(5, "1234");
			statement.setString(6, brExecutive.getBe_mobile_number());
			statement.setString(7, "active");
			statement.setString(8, userId);
			statement.setString(9, brExecutive.getBe_display_name());
			statement.setString(10, brExecutive.getBe_branch_name());
			statement.setString(11, brExecutive.getBe_permanent_address_city());
			statement.setString(12, brExecutive.getBe_mailling_address_city());
			statement.setBlob(13, inputStream);
			statement.setBlob(14, inputStream2);
			   
			  
			    // sends the statement to the database server
			    int row = statement.executeUpdate();
			    st=true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{  
			if (con != null){  
				try { 
					con.close(); 
                } catch (SQLException e) {
        	    }
	            con = null;
	          }  
		}
	   
		return st;
	}
	

	public UserInfo getUserDetail(String SQL)  {	
		
		UserInfo Acc = new UserInfo();
		Connection con = null;  
		Statement st = null;
		
		try {
			DatabaseController d = DatabaseController.getInstance();
			con=d.getConnection();
			ResultSet rs = null;
			st = (Statement)con.createStatement();			
			rs = st.executeQuery(SQL);
			System.out.println("SQL "+SQL);
			while( rs.next() ){
				Acc.setUser_name(""+rs.getString("USER_NAME"));
				Acc.setUser_password(""+rs.getString("PASSWORD"));
				
			} 
		} catch (Exception e) {
			System.out.println("Error On account details : "+e.toString());
		}
		finally{  
			if (con != null){  
				try { 
					con.close(); 
               } catch (SQLException e) {
       	    }
	            con = null;
	          }  
		}
		return Acc;
	}


}
