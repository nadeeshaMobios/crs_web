package com.evidu.bank.sva;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.evidu.bank.entitiy.Account;
import com.evidu.bank.entitiy.Dealer;

public class MainController {
	private static final MainController mc = new MainController();

	public static MainController getInstance() {
		return mc;
	}

	 public List<BOApprovedStatus> getApporvalList(String SQL){
		
		  	List<BOApprovedStatus> dbList = new ArrayList<BOApprovedStatus>();
	        Connection connection = null;
	        DatabaseController d = DatabaseController.getInstance();
	        try {
				connection = (Connection) d.getConnection();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
	        Statement stmt = null;
	        ResultSet rs=null;
	        
	        try {
	        	System.out.println("2222222222222222222 : "+SQL);
	            stmt = (Statement) connection.createStatement();
	                rs = stmt.executeQuery(SQL);
	                while (rs.next()){
	                	//UrlTotal urlTot = new UrlTotal();
	                	BOApprovedStatus bas = new BOApprovedStatus();
	                	bas.setId(rs.getString("id"));
	                	bas.setRefNo(rs.getString("REF_NO"));
	                	bas.setAccountName(rs.getString("ACCOUNT_NAME"));
	                	bas.setCutomer(rs.getString("NAME"));
	                	bas.setMobileNo(rs.getString("MOBILE_NUMBER"));
	                	bas.setStatus(rs.getString("STATUS"));
	                	dbList.add(bas);
	                	
	                }
	                }catch (Exception e) {
	                	System.out.println("** getApporvalList **"+e.toString());
						e.printStackTrace();
	                }
	        finally{
	        	if ( connection != null ){
					try{ 
						connection.close(); 
					}catch(Exception ce){
						ce.printStackTrace();
						}
				}
	        }
			return dbList;
	 }
	 
	 public BOApprovedStatus getPendingCount(String SQL){
			
			//Virtual_url virtualUrlData = new Virtual_url();
		 	BOApprovedStatus app = new BOApprovedStatus();
			Connection con = null;
			Statement stmt = null;
			ResultSet rs = null;
			
			try {
				DatabaseController dc = DatabaseController.getInstance();
				con = (Connection) dc.getConnection();

				stmt = con.createStatement();

				rs = stmt.executeQuery(SQL);
				
				while(rs.next()) {
						
						app.setTotalPending(rs.getString("total_pending"));
						
					}
				
			} catch (Exception e) {
				System.out.println("Error in getPendingCount(String sql) method"+e);
				e.printStackTrace();
			}

			// to close DB connection
			finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						System.out.println("error in getPendingCount connection close "+e);
					}
					con = null;
				}
			}
			
			return app;
		}
	 
	 
	 public List<BOApprovedStatus> getAllCustomerList(String SQL){
			
		  	List<BOApprovedStatus> allCustomerList = new ArrayList<BOApprovedStatus>();
	        Connection connection = null;
	        DatabaseController d = DatabaseController.getInstance();
	        try {
				connection = (Connection) d.getConnection();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
	        Statement stmt = null;
	        ResultSet rs=null;
	        
	        try {
	            stmt = (Statement) connection.createStatement();
	                rs = stmt.executeQuery(SQL);
	                while (rs.next()){
	                	
	                	BOApprovedStatus boapproval = new BOApprovedStatus();
	                	
	                	boapproval.setId(rs.getString("id"));
	                	boapproval.setAccountName(rs.getString("ACCOUNT_NAME"));
	                	boapproval.setMobileNo(rs.getString("MOBILE_NUMBER"));
	                	boapproval.setCutomer(rs.getString("NAME"));
	                	boapproval.setFatherName(rs.getString("FATHER_NAME"));
	                	boapproval.setMotherName(rs.getString("MOTHER_NAME"));
	                	boapproval.setSpouseName(rs.getString("SPOUSE_NAME"));
	                	boapproval.setBirthday(rs.getString("DATE_OF_BIRTH"));
	                	boapproval.setProfession(rs.getString("PROFESSION"));
	                	boapproval.setSourceIncome(rs.getString("SOURCE_OF_INCOME"));
	                	boapproval.setOtherBankName(rs.getString("OTHER_ACC_BANK_NAME"));
	                	boapproval.setOtherAccountNo(rs.getString("OTHER_ACC_NUMBER"));
	                	boapproval.setOtherBranchName(rs.getString("OTHER_BRANCH_NAME"));
	                	boapproval.setIdType(rs.getString("ID_TYPE"));
	                	boapproval.setIdNumber(rs.getString("NATIONAL_ID"));
	                	boapproval.setMailHome(rs.getString("MAILING_ADDRESS_HOUSE_NO"));
	                	boapproval.setMailStreet(rs.getString("MAILING_ADDRESS_STREET"));
	                	boapproval.setMailCity(rs.getString("MAILING_ADDRESS_CITY"));
	                	boapproval.setMailstate(rs.getString("MAILING_ADDRESS_STATE"));
	                	boapproval.setPermentHome(rs.getString("PERMANENT_ADDRESS_HOUSE_NO"));
	                	boapproval.setPermanatStreet(rs.getString("PERMANENT_ADDRESS_STREET"));
	                	boapproval.setPermentCity(rs.getString("PERMANENT_ADDRESS_CITY"));
	                	boapproval.setPermanatState(rs.getString("PERMANENT_ADDRESS_STATE"));
	                	boapproval.setAnticipatedAmount(rs.getString("ANTICIPATED_AMOUNT"));
	                	boapproval.setNomineeName(rs.getString("NOMINEE_NAME"));
	                	boapproval.setNomineeBranch(rs.getString("NOMINEE_BRANCH_NAME"));
	                	boapproval.setNomineeFather(rs.getString("NOMINEE_FATHERS_NAME"));
	                	boapproval.setNomineeMother(rs.getString("NOMINEE_MOTHERS_NAME"));
	                	boapproval.setNomineeHome(rs.getString("NOMINEE_ADDRESS_HOUSE_NO"));
	                	boapproval.setNomineeStreet(rs.getString("NOMINEE_ADDRESS_STEET"));
	                	boapproval.setNomineeCity(rs.getString("NOMINEE_ADDRESS_CITY"));
	                	boapproval.setNomineeState(rs.getString("NOMINEE_ADDRESS_STATE"));
	                	boapproval.setNomineeContactNumber(rs.getString("NOMINEE_CONTACT_NO"));
	                	boapproval.setClientIdVerfy(rs.getString("AUTHORIZED_ID_VERIFIED"));
	                	boapproval.setFaceInterview(rs.getString("AUTHORIZED_INTERVIW_TAKEN"));
	                	boapproval.setUserImg(rs.getBlob("USER_IMAGE"));
	                	boapproval.setNicImg(rs.getBlob("NIC_IMAGE"));
	                	allCustomerList.add(boapproval);
	                	
	                }
	                }catch (Exception e) {
	                	System.out.println("** getApporvalList/// **"+e.toString());
						e.printStackTrace();
	                }
	        finally{
	        	if ( connection != null ){
					try{ 
						connection.close(); 
					}catch(Exception ce){
						ce.printStackTrace();
						}
				}
	        }
			return allCustomerList;
	 }
	 
	 public boolean checkboxUpdate(String sql) {
			
			Connection con=null;
			boolean b= false;
			
			try {
				DatabaseController dc = DatabaseController.getInstance();
				try {
					con = (Connection) dc.getConnection();
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				Statement stmt=con.createStatement();
				stmt.executeUpdate(sql);
				b=true;
			
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			finally {
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						System.out.println("error in connection close");
					}
					con = null;
				}
			}
			
			return b;
			
		}
	 
	 public boolean buttonStatusUpdate(String sql) {
			
			Connection con=null;
			boolean b= false;
			
			try {
				DatabaseController dc = DatabaseController.getInstance();
				try {
					con = (Connection) dc.getConnection();
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				Statement stmt=con.createStatement();
				stmt.executeUpdate(sql);
				b=true;
			
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			finally {
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						System.out.println("error in connection close");
					}
					con = null;
				}
			}
			
			return b;
			
		}
	 
	 public boolean dropdownUpdate(String sql) {
			
			Connection con=null;
			boolean b= false;
			
			try {
				DatabaseController dc = DatabaseController.getInstance();
				try {
					con = (Connection) dc.getConnection();
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				Statement stmt=con.createStatement();
				stmt.executeUpdate(sql);
				b=true;
			
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			finally {
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						System.out.println("error in dropdownUpdate connection close");
					}
					con = null;
				}
			}
			
			return b;
			
		}
	 
	  public byte[] getImageData(String query){
		 byte[] imgData = null;
		 Connection con = null;
		 Statement stmt = null;
		 ResultSet rs = null;
		 Blob image = null;
		 boolean q_update_status=false;

		 try {

		  DatabaseController d = DatabaseController.getInstance(); 
		  con=(Connection)d.getConnection(); 
		  
		  stmt = con.createStatement();

		  rs = stmt.executeQuery(query);

		  if (rs.next()) {
		  
		   image = rs.getBlob(1);//blob
		   
		   //inputStream=image.getBinaryStream();
		   imgData = image.getBytes(1, (int) image.length()); //array
		  }
		 }catch( Exception e){
		  e. printStackTrace();
		 }finally {
		   try {
		   rs.close();
		   stmt.close();
		   con.close();
		  } catch (SQLException e) {

		   e.printStackTrace();

		  }
		 }
		 return imgData;
		}
	  
	  
	  public static List<String> getDistric(List<String> querylist){

		    List<String> distlist = new ArrayList<String>();
		    Connection connection = null;
				DatabaseController d = DatabaseController.getInstance();
				try {
					connection = (Connection) d.getConnection();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

		   Statement stmt = null;
		   ResultSet rs=null;

		   try {
		        stmt = (Statement) connection.createStatement();

		        for(String SQL :querylist){
		        	//System.out.print("**** SQL****"+SQL);
		        rs = stmt.executeQuery(SQL);
		        while (rs.next()){
		           String dist=rs.getString("dist_name");
		           distlist.add(dist);

		        }
		    }
		    } catch (SQLException e) {
		        e.printStackTrace();
		    }
		finally{
		        try {
		            connection.close();
		            stmt.close();
		        }catch (SQLException e) {

		            e.printStackTrace();
		        }
		}

		return distlist;

		}
	  
	  
	  public static List<String> getThana(List<String> querylist){

		    List<String> thanalist = new ArrayList<String>();
		    Connection connection = null;
				DatabaseController d = DatabaseController.getInstance();
				try {
					connection = (Connection) d.getConnection();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

		   Statement stmt = null;
		   ResultSet rs=null;

		   try {
		        stmt = (Statement) connection.createStatement();

		        for(String SQL :querylist){
		        	//System.out.print("**** THANA SQL****"+SQL);
		        rs = stmt.executeQuery(SQL);
		        while (rs.next()){
		           String thana=rs.getString("thana_name");
		           thanalist.add(thana);

		        }
		    }
		    } catch (SQLException e) {
		        e.printStackTrace();
		    }
		finally{
		        try {
		            connection.close();
		            stmt.close();
		        }catch (SQLException e) {

		            e.printStackTrace();
		        }
		}

		return thanalist;

		}
	  
	  
	  public static List<String> getUnion(List<String> querylist){

		    List<String> unilist = new ArrayList<String>();
		    Connection connection = null;
				DatabaseController d = DatabaseController.getInstance();
				try {
					connection = (Connection) d.getConnection();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

		   Statement stmt = null;
		   ResultSet rs=null;

		   try {
		        stmt = (Statement) connection.createStatement();

		        for(String SQL :querylist){
		        	System.out.print("**** UNION SQL****"+SQL);
		        rs = stmt.executeQuery(SQL);
		        while (rs.next()){
		           String union=rs.getString("union_name");
		           unilist.add(union);

		        }
		    }
		    } catch (SQLException e) {
		        e.printStackTrace();
		    }
		finally{
		        try {
		            connection.close();
		            stmt.close();
		        }catch (SQLException e) {

		            e.printStackTrace();
		        }
		}

		return unilist;

		}
	  
	  public boolean insertImage(String SQL,Dealer deal,InputStream inputStream1,InputStream inputStream2,InputStream inputStream3,InputStream inputStream4,InputStream inputStream5,InputStream inputStream6,InputStream inputStream7,String user){
		  
		  System.out.println("SQL : "+SQL);
		  System.out.println("deal : "+deal.getArea());
		  System.out.println("inputStream1 : "+inputStream1);
		  System.out.println("inputStream2 : "+inputStream2);
		  System.out.println("inputStream3 : "+inputStream3);
		  System.out.println("inputStream4 : "+inputStream4);
		  System.out.println("inputStream5 : "+inputStream5);
		  System.out.println("inputStream6 : "+inputStream6);
		  System.out.println("inputStream7 : "+inputStream7);
		  System.out.println("user : "+user);
		  
		  Connection con = null;
			PreparedStatement statement;
			Date date = new Date();
			//Dealer deal = new Dealer();
			SimpleDateFormat format = new SimpleDateFormat();
			format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String DateToStr = format.format(date);
			
			System.out.println(DateToStr);
			boolean st=false;
			try {
				DatabaseController d = DatabaseController.getInstance();	
				con=(Connection)d.getConnection();	
				statement = con.prepareStatement(SQL);
				    statement.setString(1, user);
				    statement.setString(2, DateToStr);
				    statement.setString(3, deal.getPd_org_name());
				    statement.setString(4, deal.getPd_mobile());
				    statement.setString(5, deal.getRegion());
				    statement.setString(6, deal.getArea());
				    statement.setString(7, deal.getTerritory());
				    statement.setString(8, deal.getDealer_org_name());
				    statement.setString(9, deal.getDealer_org_address());
				    statement.setString(10, deal.getOrg_union());
				    statement.setString(11, deal.getOrg_thana());
				    statement.setString(12, deal.getOrg_district());
				    statement.setString(13, deal.getOrg_division());
				    statement.setString(14, deal.getOrg_postCode());
				    statement.setString(15, deal.getDealerName());
				    statement.setString(16, deal.getDateofbirth());
				    statement.setString(17, deal.getIdType());
				    statement.setString(18, deal.getIdNumber());
				    statement.setString(19, deal.getDealerAddress());
				    statement.setString(20, deal.getDealerUnion());
				    statement.setString(21, deal.getDealerThana());
				    statement.setString(22, deal.getDealerDistric());
				    statement.setString(23, deal.getDealerDivision());
				    statement.setString(24, deal.getDealerPostCode());
				    statement.setString(25, deal.getMobileNumber());
				    statement.setString(26, deal.getOwnershipType());
				    statement.setString(27, deal.getEmergencyContactNo());
				    statement.setString(28, deal.getEmergencyName());
				    statement.setString(29, deal.getRelataion());
				    statement.setString(30, deal.getEmergencyAddress());
				    statement.setString(31, deal.getEmergencyDistrict());
				    statement.setString(32, deal.getEmergencyThana());
				    statement.setString(33, deal.getEmergencyUnion());
				    statement.setString(34, deal.getTradeLicences());
				    statement.setString(35, deal.getTinNo());
				    statement.setString(36, deal.getVatRegNo());
				    statement.setString(37, deal.getAccountNo());
				    statement.setString(38, deal.getBankName());
				    statement.setString(39, deal.getBranchName());
				    statement.setString(40, deal.getNomineeName());
				    statement.setString(41, deal.getNomineeRelation());
				    statement.setString(42, deal.getNomineeMobile());
				    statement.setString(43, deal.getNomineeFather());
				    statement.setString(43, deal.getNomineeMother());
				    statement.setString(44, deal.getNomineeIdType());
				    statement.setString(45, deal.getNomneeIdNo());
				    statement.setString(46, deal.getNomineeAddress());
				    statement.setString(47, deal.getNomineeUnion());
				    statement.setString(48, deal.getNomineethana());
				    statement.setString(49, deal.getNomineeeDistrict());
				    statement.setString(50, deal.getNomineePostCode());
				    statement.setBlob(51, inputStream1);
				    statement.setBlob(52, inputStream2);
				    statement.setBlob(53, inputStream3);
				    statement.setBlob(54, inputStream4);
				    statement.setBlob(55, inputStream5);
				    statement.setBlob(56, inputStream6);
				    statement.setBlob(57, inputStream7);
				    
				    // sends the statement to the database server
				    int row = statement.executeUpdate();
				    st=true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally{  
				if (con != null){  
					try { 
						con.close(); 
	                } catch (SQLException e) {
	        	    }
		            con = null;
		          }  
			}
		   
			return st;
		}
	  
	  
	  public Dealer getDealerDetail(String SQL)  {	
			
			Dealer deal = new Dealer();
			Connection con = null;  
			Statement st = null;
			
			try {
				DatabaseController d = DatabaseController.getInstance();
				con=d.getConnection();
				ResultSet rs = null;
				st = (Statement)con.createStatement();			
				rs = st.executeQuery(SQL);
				System.out.println("SQL "+SQL);
				while( rs.next() ){
					deal.setMobileNumber(""+rs.getString("dealer_mobileNumber"));
				} 
			} catch (Exception e) {
				System.out.println("Error On account details : "+e.toString());
			}
			finally{  
				if (con != null){  
					try { 
						con.close(); 
	               } catch (SQLException e) {
	       	    }
		            con = null;
		          }  
			}
			return deal;
		}
	  
	  
	  public static List<String> getRegion(List<String> querylist){

		    List<String> regionlist = new ArrayList<String>();
		    Connection connection = null;
				DatabaseController d = DatabaseController.getInstance();
				try {
					connection = (Connection) d.getConnection();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

		   Statement stmt = null;
		   ResultSet rs=null;

		   try {
		        stmt = (Statement) connection.createStatement();

		        for(String SQL :querylist){
		        	//System.out.print("**** SQL****"+SQL);
		        rs = stmt.executeQuery(SQL);
		        while (rs.next()){
		        	
		           String reg=rs.getString("reg_name");
		           regionlist.add(reg);

		        }
		    }
		    } catch (SQLException e) {
		        e.printStackTrace();
		    }
		finally{
		        try {
		            connection.close();
		            stmt.close();
		        }catch (SQLException e) {

		            e.printStackTrace();
		        }
		}

		return regionlist;

		}
	  
	  
	  public static List<String> getArea(List<String> querylist){

		    List<String> arealist = new ArrayList<String>();
		    Connection connection = null;
				DatabaseController d = DatabaseController.getInstance();
				try {
					connection = (Connection) d.getConnection();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

		   Statement stmt = null;
		   ResultSet rs=null;

		   try {
		        stmt = (Statement) connection.createStatement();

		        for(String SQL :querylist){
		        	//System.out.print("**** THANA SQL****"+SQL);
		        rs = stmt.executeQuery(SQL);
		        while (rs.next()){
		        	
		           String area=rs.getString("area_name");
		           arealist.add(area);

		        }
		    }
		    } catch (SQLException e) {
		        e.printStackTrace();
		    }
		finally{
		        try {
		            connection.close();
		            stmt.close();
		        }catch (SQLException e) {

		            e.printStackTrace();
		        }
		}

		return arealist;

		}
	  
	  public static List<String> getTerritory(List<String> querylist){

		    List<String> territory = new ArrayList<String>();
		    Connection connection = null;
				DatabaseController d = DatabaseController.getInstance();
				try {
					connection = (Connection) d.getConnection();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

		   Statement stmt = null;
		   ResultSet rs=null;

		   try {
		        stmt = (Statement) connection.createStatement();

		        for(String SQL :querylist){
		        	System.out.print("**** UNION SQL****"+SQL);
		        rs = stmt.executeQuery(SQL);
		        while (rs.next()){
		        	
		           String terri=rs.getString("terri_name");
		           territory.add(terri);

		        }
		    }
		    } catch (SQLException e) {
		        e.printStackTrace();
		    }
		finally{
		        try {
		            connection.close();
		            stmt.close();
		        }catch (SQLException e) {

		            e.printStackTrace();
		        }
		}

		return territory;

		}
	  
	  
	
}
