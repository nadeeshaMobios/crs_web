package com.evidu.bank.sva;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.evidu.bank.entitiy.Dealer;
import com.evidu.bank.entitiy.Account;
import com.evidu.bank.entitiy.TemporyAccount;

public class AccountController {
	
	public boolean InsertData(String sql){
		boolean status=false;
		Connection con = null;
		try {
			DatabaseController d = DatabaseController.getInstance();	
			con=(Connection)d.getConnection();	
			Statement stmt = (Statement) con.createStatement();
		    stmt.execute(sql);
		    status=true;
		    
		} catch (SQLException e) {
			System.out.println("Error On insert Account Data : "+e.toString());
		}
		finally{  
			if (con != null) { 
				try { 
					con.close(); 
				}catch (SQLException e) {
					
				}
				con = null;
			} 
		}
		System.out.println("status : "+status);
		return status;
	}
	
	public boolean insertImage(String SQL,InputStream inputStream,InputStream inputStream2,String accNo,String user,String mobileNo){
		Connection con = null;
		PreparedStatement statement;
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat();
		format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String DateToStr = format.format(date);
		
		System.out.println(DateToStr);
		boolean st=false;
		try {
			DatabaseController d = DatabaseController.getInstance();	
			con=(Connection)d.getConnection();	
			statement = con.prepareStatement(SQL);
			 statement.setString(1, accNo);
			    statement.setString(2, mobileNo);
			   
			   
			     
			   // if (inputStream != null) {
			        // fetches input stream of the upload file for the blob column
			        statement.setBlob(3, inputStream);
			        statement.setBlob(4, inputStream2);
			    //}
			    statement.setString(5,DateToStr );
			    statement.setString(6, user);
			    // sends the statement to the database server
			    int row = statement.executeUpdate();
			    st=true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{  
			if (con != null){  
				try { 
					con.close(); 
                } catch (SQLException e) {
        	    }
	            con = null;
	          }  
		}
	   
		return st;
	}
	
	public boolean insertImage1(String SQL,InputStream inputStream,InputStream inputStream2,InputStream inputStream3,InputStream inputStream4,InputStream inputStream5,String user,String mobileNo){
		
		Connection con = null;
		PreparedStatement statement;
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat();
		format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String DateToStr = format.format(date);
		
		System.out.println(DateToStr);
		boolean st=false;
		try {
			DatabaseController d = DatabaseController.getInstance();	
			con=(Connection)d.getConnection();	
			statement = con.prepareStatement(SQL);
			 statement.setString(1, mobileNo);
			   
			   // if (inputStream != null) {
			        // fetches input stream of the upload file for the blob column
			        statement.setBlob(2, inputStream);
			        statement.setBlob(3, inputStream2);
			        statement.setBlob(4, inputStream3);
			        statement.setBlob(5, inputStream4);
			        statement.setBlob(6, inputStream5);
			        
			    //}
			    statement.setString(7,DateToStr );
			    statement.setString(8, user);
			    // sends the statement to the database server
			    int row = statement.executeUpdate();
			    st=true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{  
			if (con != null){  
				try { 
					con.close(); 
                } catch (SQLException e) {
        	    }
	            con = null;
	          }  
		}
	   
		return st;
	}
	
	public TemporyAccount getTemporyAccDetails(String SQL)  {	
		TemporyAccount temporyAcc = new TemporyAccount();
		Connection con = null;  
		Statement st = null;
		
		try {
			DatabaseController d = DatabaseController.getInstance();
			con=d.getConnection();
			ResultSet rs = null;
			st = (Statement)con.createStatement();			
			rs = st.executeQuery(SQL);
			System.out.println("SQL "+SQL);
			while( rs.next() ){
				temporyAcc.setMSISDN(""+rs.getString("MSISDN"));
				temporyAcc.setNIC(""+rs.getString("NIC"));
				temporyAcc.setCREATED_DATE(""+rs.getString("CREATED_DATE"));
				temporyAcc.setSTATUS(""+rs.getString("STATUS"));
				temporyAcc.setMOTHERS_NAME(""+rs.getString("MOTHERS_NAME"));
				temporyAcc.setADDRESS_HOUSE_NO(""+rs.getString("ADDRESS_HOUSE_NO"));
				temporyAcc.setADDRESS_CITY(""+rs.getString("ADDRESS_CITY"));
				temporyAcc.setADDRESS_STATE(""+rs.getString("ADDRESS_STATE"));
				temporyAcc.setADDRESS_STREET(""+rs.getString("ADDRESS_STREET"));
				temporyAcc.setCUSTOMER_NAME(""+rs.getString("CUSTOMER_NAME"));
				
			} 
		} catch (Exception e) {
			System.out.println("Error On tempory account details : "+e.toString());
		}
		finally{  
			if (con != null){  
				try { 
					con.close(); 
                } catch (SQLException e) {
        	    }
	            con = null;
	          }  
		}
		return temporyAcc;
	}

	
	public Account getAccountDetail(String SQL)  {	
		
		Account Acc = new Account();
		Connection con = null;  
		Statement st = null;
		
		try {
			DatabaseController d = DatabaseController.getInstance();
			con=d.getConnection();
			ResultSet rs = null;
			st = (Statement)con.createStatement();			
			rs = st.executeQuery(SQL);
			System.out.println("SQL "+SQL);
			while( rs.next() ){
				Acc.setACCOUNT_NUMBER(""+rs.getString("ACCOUNT_NUMBER")) ;
				Acc.setACCOUNT_NAME(""+rs.getString("ACCOUNT_NAME"));
				Acc.setCREATED_BY(""+rs.getString("CREATED_BY"));
				Acc.setMOBILE_NUMBER(""+rs.getString("MOBILE_NUMBER"));
				/*Acc.setNATIONAL_ID(""+rs.getString("MOBILE_NUMBER"));*/
				
			} 
		} catch (Exception e) {
			System.out.println("Error On account details : "+e.toString());
		}
		finally{  
			if (con != null){  
				try { 
					con.close(); 
               } catch (SQLException e) {
       	    }
	            con = null;
	          }  
		}
		return Acc;
	}
	
	public Dealer getdealerInfo(String SQL)  {	
			
			Dealer deal = new Dealer();
			Connection con = null;  
			Statement st = null;
			
			try {
				DatabaseController d = DatabaseController.getInstance();
				con=d.getConnection();
				ResultSet rs = null;
				st = (Statement)con.createStatement();			
				rs = st.executeQuery(SQL);
				System.out.println("SQL "+SQL);
				while( rs.next() ){
					
					deal.setDealerName(rs.getString("dealer_name"));
					deal.setMobileNumber(rs.getString("dealer_mobileNumber"));
					deal.setDealerAddress(rs.getString("dealer_address"));
					deal.setDealerDistric(rs.getString("dealer_distric"));
					deal.setDealerThana(rs.getString("dealer_than"));
					deal.setDealerUnion(rs.getString("dealer_union"));
					
				} 
			} catch (Exception e) {
				System.out.println("Error On account details : "+e.toString());
			}
			finally{  
				if (con != null){  
					try { 
						con.close(); 
	               } catch (SQLException e) {
	       	    }
		            con = null;
		          }  
			}
			return deal;
		}

}

