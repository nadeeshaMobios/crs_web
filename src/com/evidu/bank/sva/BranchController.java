package com.evidu.bank.sva;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.evidu.bank.entitiy.Branches;


public class BranchController {

	public ArrayList<Branches> getBranchNameList(String SQL)  {	
		ArrayList<Branches> brancharr = new ArrayList<Branches>();
		Connection con = null;  
		Statement st = null;
		
		try {
			DatabaseController d = DatabaseController.getInstance();
			con=d.getConnection();
			ResultSet rs = null;
			st = (Statement)con.createStatement();			
			rs = st.executeQuery(SQL);
			System.out.println("SQL "+SQL);
			while( rs.next() ){
				Branches br= new Branches();
				br.setBRANCH_CODE(rs.getString("BRANCH_CODE"));
				br.setBRANCH_NAME(rs.getString("BRANCH_NAME"));
				br.setID(rs.getString("ID"));
				brancharr.add(br);
				
			} 
		} catch (Exception e) {
			System.out.println("Error On get Branch name list : "+e.toString());
		}
		finally{  
			if (con != null){  
				try { 
					con.close(); 
                } catch (SQLException e) {
        	    }
	            con = null;
	          }  
		}
		return brancharr;
	}

}
