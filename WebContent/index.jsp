
<%
response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
//response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
int timeout = session.getMaxInactiveInterval();
response.setHeader("Refresh", timeout + "; URL = index.jsp");
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility
session.invalidate();
%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
System.out.println("basepath>>>>>>>>>>>>>>>>>>>>>>>>"+basePath);
%>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->

<script src="http://wcetdesigns.com/assets/javascript/jquery.js"></script>
<script src="http://wcetdesigns.com/assets/javascript/jquery/cookie-plugin.js"></script>


<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Bootstrap Stylesheet -->
<link rel="stylesheet" href="<%=basePath%>bootstrap/css/bootstrap.min.css" media="screen">

<!-- Uniform Stylesheet -->
<link rel="stylesheet" href="<%=basePath%>plugins/uniform/css/uniform.default.css" media="screen">

<!-- Plugin Stylsheets first to ease overrides -->

<!-- End Plugin Stylesheets -->

<!-- Main Layout Stylesheet -->
<link rel="stylesheet" href="<%=basePath%>assets/css/fonts/icomoon/style.css" media="screen">
<link rel="stylesheet" href="<%=basePath%>assets/css/login.css" media="screen">
<link rel="stylesheet" href="<%=basePath%>plugins/zocial/zocial.css" media="screen">

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<title>Tap'nPay- Login</title>

</head>

<body >
<!-- onLoad="load_em()" -->
<%
	String errCode=""+request.getParameter("err");
%>
    <div id="login-wrap">

		<div id="login-buttons">
			
		<img border="0" src="assets/images/evidu-logo.png" alt="Evidu logo " width="120" height="40">	
		</div>

		<div id="login-inner" class="login-inset">

			<div id="login-circle">
				<section id="login-form" class="login-inner-form">
					<h1>Login</h1>
					<form class="form-vertical" action="loginController.jsp">
					<%if(errCode.equalsIgnoreCase("err")){ %>
					        <div id="errDisplay" style="color:red;">
					        	<h5 align="center">Username or Password is incorrect</h5>
					        </div>
					        <%} %>
					
						<div class="control-group-merged">
							<div class="control-group">
								<input type="text" placeholder="Username" name="input_usernameCP" id="input_usernameCP" class="big required">
							</div>
							<div class="control-group">
								<input type="password" placeholder="Password" name="input_passwordCP" id="input_passwordCP" class="big required" autocomplete="off">
							</div>
						</div>
						<div class="control-group">
							<!--
							 <label class="checkbox">
								<input type="checkbox" name="check" id="check" class="uniform" onchange="remember_me()"> Remember me
							</label> 
							 -->
						</div>
						<div class="form-actions">
							<input type="hidden" name="action" id="action" value="loginUser" >
							<button type="submit" class="btn btn-success btn-block btn-large">Login</button>
						</div>
					</form>
				</section>
				
				
			</div>

		</div>

	    <!-- <div id="login-social" class="login-inset">
	    	<button class="zocial facebook">Connect with Facebook</button>
	    	<button class="zocial twitter">Connect with Twitter</button>
	    </div> -->
    </div>

	<!-- Core Scripts -->
	<script src="assets/js/libs/jquery-1.8.3.min.js"></script>
	<script src="assets/js/libs/jquery.placeholder.min.js"></script>
    
    <!-- Login Script -->
    <script src="assets/js/login.js"></script>

    <!-- Validation -->
    <script src="plugins/validate/jquery.validate.min.js"></script>
    
    <!-- Uniform Script -->
    <script src="plugins/uniform/jquery.uniform.min.js"></script>

</body>

</html>
