<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.evidu.bank.sva.*"%>
<%@page import="com.evidu.bank.entitiy.*"%>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->

<head >

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Bootstrap Stylesheet -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" media="all">

<!-- jquery-ui Stylesheets -->
<link rel="stylesheet" href="assets/jui/css/jquery-ui.css" media="screen">
<link rel="stylesheet" href="assets/jui/jquery-ui.custom.css" media="screen">
<link rel="stylesheet" href="assets/jui/timepicker/jquery-ui-timepicker.css" media="screen">

<!-- Uniform Stylesheet -->
<link rel="stylesheet" href="plugins/uniform/css/uniform.default.css" media="screen">

<!-- Plugin Stylsheets first to ease overrides -->

<!-- iButton -->
<link rel="stylesheet" href="plugins/ibutton/jquery.ibutton.css" media="screen">

<!-- Circular Stat -->
<link rel="stylesheet" href="custom-plugins/circular-stat/circular-stat.css">

<!-- Fullcalendar -->
<link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.css" media="screen">
<link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.print.css" media="print">

<!-- End Plugin Stylesheets -->

<!-- Main Layout Stylesheet -->
<link rel="stylesheet" href="assets/css/fonts/icomoon/style.css" media="screen">
<link rel="stylesheet" href="assets/css/main-style.css" media="screen">

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<title>Tap'nPay</title> 
<script type="text/javascript">


</script>

</head>

<body data-show-sidebar-toggle-button="false" data-fixed-sidebar="false">
 <%
  UserInfo userData =new UserInfo();
		userData= (UserInfo)session.getAttribute("current_user");
		if(userData==null){
			response.sendRedirect("index.jsp");
			return;
		}
		String errCode=""+request.getParameter("st");
		 DashboardController dcon = new  DashboardController();
		 List<Account> pendingStatusList= new ArrayList<Account>();
 		//System.out.println("****"+pendingStatusList.size());
 		pendingStatusList = dcon.getPendingCustomerListByTapNPay();
		
  %>

    

    <div id="wrapper">
        <header id="header" class="navbar navbar-inverse">
            <div class="navbar-inner">
                <div class="container">
					<div class="brand-wrap pull-left">
						<div class="brand-img">
							<a class="brand" href="#">
								<img src="assets/images/Tap_n_PAy_original.png" alt="" style="width: 87px;position: relative;left: -17px;" >
							</a>
						</div>
					</div>
                    
                    <div id="header-right" class="clearfix">
						<div id="nav-toggle" data-toggle="collapse" data-target="#navigation" class="collapsed">
							<i class="icon-caret-down"></i>
						</div>
						<div id="header-search">
							<span id="search-toggle" data-toggle="dropdown">
								<i class="icon-search"></i>
							</span>
							<form class="navbar-search">
								<input type="text" class="search-query" placeholder="Search">
							</form>
						</div>
                        <div id="dropdown-lists">
                            <div class="item-wrap">
    							<a class="item" href="#" data-toggle="dropdown">
    								<span class="item-icon"><i class="icon-exclamation-sign"></i></span>
    								<span class="item-label">Notifications</span>
    								<span class="item-count">4</span>
    							</a>
                               
                            </div>
                            <div class="item-wrap">
    							<a class="item" href="#" data-toggle="dropdown">
    								<span class="item-icon"><i class="icon-envelope"></i></span>
    								<span class="item-label">Messages</span>
    								<span class="item-count">16</span>
    							</a>
                                
                            </div>
						</div>
                        <div id="header-functions" class="pull-right">
                        	<div id="user-info" class="clearfix">
                                	<span class="info">
                                	Welcome
                                    <span class="name"><%=userData.getDisplay_name() %></span>
                                </span>
                               
                            	<div class="avatar">
                                	<a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    	<img src="assets/images/defaltUser.png" alt="Avatar">
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                    	
                                        <li><a href="index.jsp"><i class="icol-key"></i> Logout</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="logout-ribbon">
                            	<a href="index.jsp"><i class="icon-off"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        
        <div id="content-wrap">
        	<div id="content" class="sidebar-minimized">
            	<div id="content-outer">
                	<div id="content-inner">
                    	<aside id="sidebar">
                        	<nav id="navigation" class="collapse">
                            	<ul>
                                	<li class="active"><a href="BranchMakerDashboard.jsp">
                                    	<span title="Dashbaord">
                                    		<i class="icon-gauge"></i>
											<span class="nav-title">Dashboard</span>
                                        </span>
                                        </a>
                                    </li>
                                    
                                
                                </ul>
                            </nav>
                        </aside>

                        <div id="sidebar-separator"></div>
                        
                        <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icol-application-home"></i>Tap'nPay
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<i class="icol-dashboard"></i>Dashboard
                                        <span class="divider">&raquo;</span>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Dashboard 
                                </h1>
                            </div>
                            
                            <div id="main-content" style="height: 640px;">
                            
                            	<h4 class='sub'><span>Status Results Summary</span></h4>
                                <div class="row-fluid" id="codeRefer" >
                                    <div class="span12 widget">
                                        <div class="widget-header">
                                            <span class="title">Basic DataTable</span>
                                        </div>
                                        <div class="widget-content table-container">
                                            <table id="demo-dtable-01" class="table table-striped" >
                                                <thead>
                                                   <tr>
                                                     	<th  style="display:none;">ID</th>
								                        <th>Account Name</th>
								                        <th>Customer</th>
								                        <th>Mobile</th>
								                        <th>Status</th>
								                        <th>Details</th>
								                        <th>Do Check</th>
						                            </tr>
                                                </thead>
                                                <tbody>
                                                <% 
                                               
                                        		
                                        			
                                        			for(int i=0;i<pendingStatusList.size();i++){ 
                                        		 %>
                                                 <tr >
                                                 <td style="display:none;"><%=pendingStatusList.get(i).getID() %> </td>
							                              <td><%=pendingStatusList.get(i).getACCOUNT_NAME() %></td>
							                              <td><%=pendingStatusList.get(i).getNAME() %></td>
							                              <td><%=pendingStatusList.get(i).getMOBILE_NUMBER() %></td>
							                              <td><%=pendingStatusList.get(i).getSTATUS() %></td>
					                                      <td style="padding-left: 27px;"><i class="icol-application-form" onclick="formalert('<%=pendingStatusList.get(i).getMOBILE_NUMBER() %>','<%=pendingStatusList.get(i).getSTATUS() %>','Tap n Pay','Cancel')" ></i> </td> 
							                              <td style="padding-left: 27px;"><input type="checkbox"  style=" width: 17px;height: 15px;" id="myCheck_<%=pendingStatusList.get(i).getID()%>" onchange="customerCheck('<%=pendingStatusList.get(i).getMOBILE_NUMBER() %>','<%=pendingStatusList.get(i).getID()%>','<%=pendingStatusList.get(i).getSTATUS()%>')"  >
							                              </td>
                                                 </tr>
                                                 <%} 
                                               
                                                 %>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div> 
                               
                           	</div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        
         <footer id="footer">
            <div class="footer-left">Mobile Banking Platform</div>
            <div class="footer-right"><p>Copyright 2016 - Evidu Private Limited All Rights Reserved.</p></div>
        </footer>
        
    </div>

    <!-- Core Scripts -->
    <script src="assets/js/libs/jquery-1.8.3.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/libs/jquery.placeholder.min.js"></script>
    <script src="assets/js/libs/jquery.mousewheel.min.js"></script>
    
    <!-- Template Script -->
    <script src="assets/js/template.js"></script>
    <script src="assets/js/setup.js"></script>

    <!-- Customizer, remove if not needed -->
    <script src="assets/js/customizer.js"></script>

    <!-- Uniform Script -->
    <script src="plugins/uniform/jquery.uniform.min.js"></script>
    
    <!-- jquery-ui Scripts -->
   <!--  <script src="assets/jui/js/jquery-ui-1.9.2.min.js"></script>  -->
     <script src="assets/jui/js/jquery-ui-1.9.2.min_edited.js"></script>
    <script src="assets/jui/jquery-ui.custom.min.js"></script>
    <script src="assets/jui/timepicker/jquery-ui-timepicker.min.js"></script>
    <script src="assets/jui/jquery.ui.touch-punch.min.js"></script>
    
    <!-- Plugin Scripts -->
    
    <!-- Flot -->
    <!--[if lt IE 9]>
    <script src="assets/js/libs/excanvas.min.js"></script>
    <![endif]-->
    <script src="plugins/flot/jquery.flot.min.js"></script>
    <script src="plugins/flot/plugins/jquery.flot.tooltip.min.js"></script>
    <script src="plugins/flot/plugins/jquery.flot.pie.min.js"></script>
    <script src="plugins/flot/plugins/jquery.flot.resize.min.js"></script>

    <!-- Circular Stat -->
    <script src="custom-plugins/circular-stat/circular-stat.min.js"></script>

    <!-- SparkLine -->
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
    
    <!-- iButton -->
    <script src="plugins/ibutton/jquery.ibutton.min.js"></script>

    <!-- Full Calendar -->
    <script src="plugins/fullcalendar/fullcalendar.min.js"></script>
    
    
    <script src="assets/js/tapnpayUser.js"></script>
    
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/TableTools/js/TableTools.min.js"></script>
    <script src="plugins/datatables/FixedColumns/FixedColumns.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.js"></script>
    <script src="plugins/datatables/jquery.dataTables.columnFilter.js"></script>
    
    <!-- Demo Scripts -->
    <script src="assets/js/demo/dashboard.js"></script>
    <script src="assets/js/demo/dataTables.js"></script>
    
    

</body>

</html>
