/* Japanese initialisation for the jQuery UI date picker plugin. */
/* Written by Kentaro SATO (kentaro@ranvis.com). */
jQuery(function($){
	$.datepicker.regional['ja'] = {
		closeText: 'é–‰ã�˜ã‚‹',
		prevText: '&#x3C;å‰�',
		nextText: 'æ¬¡&#x3E;',
		currentText: 'ä»Šæ—¥',
		monthNames: ['1æœˆ','2æœˆ','3æœˆ','4æœˆ','5æœˆ','6æœˆ',
		'7æœˆ','8æœˆ','9æœˆ','10æœˆ','11æœˆ','12æœˆ'],
		monthNamesShort: ['1æœˆ','2æœˆ','3æœˆ','4æœˆ','5æœˆ','6æœˆ',
		'7æœˆ','8æœˆ','9æœˆ','10æœˆ','11æœˆ','12æœˆ'],
		dayNames: ['æ—¥æ›œæ—¥','æœˆæ›œæ—¥','ç�«æ›œæ—¥','æ°´æ›œæ—¥','æœ¨æ›œæ—¥','é‡‘æ›œæ—¥','åœŸæ›œæ—¥'],
		dayNamesShort: ['æ—¥','æœˆ','ç�«','æ°´','æœ¨','é‡‘','åœŸ'],
		dayNamesMin: ['æ—¥','æœˆ','ç�«','æ°´','æœ¨','é‡‘','åœŸ'],
		weekHeader: 'é€±',
		dateFormat: 'yy-mm-dd',
		firstDay: 0,
		isRTL: false,
		showMonthAfterYear: true,
		yearSuffix: 'å¹´'};
	$.datepicker.setDefaults($.datepicker.regional['ja']);
});