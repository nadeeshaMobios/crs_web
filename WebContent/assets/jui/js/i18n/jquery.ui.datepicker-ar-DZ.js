/* Algerian Arabic Translation for jQuery UI date picker plugin. (can be used for Tunisia)*/
/* Mohamed Cherif BOUCHELAGHEM -- cherifbouchelaghem@yahoo.fr */

jQuery(function($){
	$.datepicker.regional['ar-DZ'] = {
		closeText: 'Ø¥ØºÙ„Ø§Ù‚',
		prevText: '&#x3C;Ø§Ù„Ø³Ø§Ø¨Ù‚',
		nextText: 'Ø§Ù„ØªØ§Ù„ÙŠ&#x3E;',
		currentText: 'Ø§Ù„ÙŠÙˆÙ…',
		monthNames: ['Ø¬Ø§Ù†Ù�ÙŠ', 'Ù�ÙŠÙ�Ø±ÙŠ', 'Ù…Ø§Ø±Ø³', 'Ø£Ù�Ø±ÙŠÙ„', 'Ù…Ø§ÙŠ', 'Ø¬ÙˆØ§Ù†',
		'Ø¬ÙˆÙŠÙ„ÙŠØ©', 'Ø£ÙˆØª', 'Ø³Ø¨ØªÙ…Ø¨Ø±','Ø£ÙƒØªÙˆØ¨Ø±', 'Ù†ÙˆÙ�Ù…Ø¨Ø±', 'Ø¯ÙŠØ³Ù…Ø¨Ø±'],
		monthNamesShort: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
		dayNames: ['Ø§Ù„Ø£Ø­Ø¯', 'Ø§Ù„Ø§Ø«Ù†ÙŠÙ†', 'Ø§Ù„Ø«Ù„Ø§Ø«Ø§Ø¡', 'Ø§Ù„Ø£Ø±Ø¨Ø¹Ø§Ø¡', 'Ø§Ù„Ø®Ù…ÙŠØ³', 'Ø§Ù„Ø¬Ù…Ø¹Ø©', 'Ø§Ù„Ø³Ø¨Øª'],
		dayNamesShort: ['Ø§Ù„Ø£Ø­Ø¯', 'Ø§Ù„Ø§Ø«Ù†ÙŠÙ†', 'Ø§Ù„Ø«Ù„Ø§Ø«Ø§Ø¡', 'Ø§Ù„Ø£Ø±Ø¨Ø¹Ø§Ø¡', 'Ø§Ù„Ø®Ù…ÙŠØ³', 'Ø§Ù„Ø¬Ù…Ø¹Ø©', 'Ø§Ù„Ø³Ø¨Øª'],
		dayNamesMin: ['Ø§Ù„Ø£Ø­Ø¯', 'Ø§Ù„Ø§Ø«Ù†ÙŠÙ†', 'Ø§Ù„Ø«Ù„Ø§Ø«Ø§Ø¡', 'Ø§Ù„Ø£Ø±Ø¨Ø¹Ø§Ø¡', 'Ø§Ù„Ø®Ù…ÙŠØ³', 'Ø§Ù„Ø¬Ù…Ø¹Ø©', 'Ø§Ù„Ø³Ø¨Øª'],
		weekHeader: 'Ø£Ø³Ø¨ÙˆØ¹',
		dateFormat: 'yy-mm-dd',
		firstDay: 6,
  		isRTL: true,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['ar-DZ']);
});
