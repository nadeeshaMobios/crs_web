/* Serbian i18n for the jQuery UI date picker plugin. */
/* Written by Dejan DimiÄ‡. */
jQuery(function($){
	$.datepicker.regional['sr'] = {
		closeText: 'Ð—Ð°Ñ‚Ð²Ð¾Ñ€Ð¸',
		prevText: '&#x3C;',
		nextText: '&#x3E;',
		currentText: 'Ð”Ð°Ð½Ð°Ñ�',
		monthNames: ['ÐˆÐ°Ð½ÑƒÐ°Ñ€','Ð¤ÐµÐ±Ñ€ÑƒÐ°Ñ€','ÐœÐ°Ñ€Ñ‚','Ð�Ð¿Ñ€Ð¸Ð»','ÐœÐ°Ñ˜','ÐˆÑƒÐ½',
		'ÐˆÑƒÐ»','Ð�Ð²Ð³ÑƒÑ�Ñ‚','Ð¡ÐµÐ¿Ñ‚ÐµÐ¼Ð±Ð°Ñ€','ÐžÐºÑ‚Ð¾Ð±Ð°Ñ€','Ð�Ð¾Ð²ÐµÐ¼Ð±Ð°Ñ€','Ð”ÐµÑ†ÐµÐ¼Ð±Ð°Ñ€'],
		monthNamesShort: ['ÐˆÐ°Ð½','Ð¤ÐµÐ±','ÐœÐ°Ñ€','Ð�Ð¿Ñ€','ÐœÐ°Ñ˜','ÐˆÑƒÐ½',
		'ÐˆÑƒÐ»','Ð�Ð²Ð³','Ð¡ÐµÐ¿','ÐžÐºÑ‚','Ð�Ð¾Ð²','Ð”ÐµÑ†'],
		dayNames: ['Ð�ÐµÐ´ÐµÑ™Ð°','ÐŸÐ¾Ð½ÐµÐ´ÐµÑ™Ð°Ðº','Ð£Ñ‚Ð¾Ñ€Ð°Ðº','Ð¡Ñ€ÐµÐ´Ð°','Ð§ÐµÑ‚Ð²Ñ€Ñ‚Ð°Ðº','ÐŸÐµÑ‚Ð°Ðº','Ð¡ÑƒÐ±Ð¾Ñ‚Ð°'],
		dayNamesShort: ['Ð�ÐµÐ´','ÐŸÐ¾Ð½','Ð£Ñ‚Ð¾','Ð¡Ñ€Ðµ','Ð§ÐµÑ‚','ÐŸÐµÑ‚','Ð¡ÑƒÐ±'],
		dayNamesMin: ['Ð�Ðµ','ÐŸÐ¾','Ð£Ñ‚','Ð¡Ñ€','Ð§Ðµ','ÐŸÐµ','Ð¡Ñƒ'],
		weekHeader: 'Ð¡ÐµÐ´',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['sr']);
});
