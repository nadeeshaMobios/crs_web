/* Arabic Translation for jQuery UI date picker plugin. */
/* Khaled Alhourani -- me@khaledalhourani.com */
/* NOTE: monthNames are the original months names and they are the Arabic names, not the new months name Ù�Ø¨Ø±Ø§ÙŠØ± - ÙŠÙ†Ø§ÙŠØ± and there isn't any Arabic roots for these months */
jQuery(function($){
	$.datepicker.regional['ar'] = {
		closeText: 'Ø¥ØºÙ„Ø§Ù‚',
		prevText: '&#x3C;Ø§Ù„Ø³Ø§Ø¨Ù‚',
		nextText: 'Ø§Ù„ØªØ§Ù„ÙŠ&#x3E;',
		currentText: 'Ø§Ù„ÙŠÙˆÙ…',
		monthNames: ['ÙƒØ§Ù†ÙˆÙ† Ø§Ù„Ø«Ø§Ù†ÙŠ', 'Ø´Ø¨Ø§Ø·', 'Ø¢Ø°Ø§Ø±', 'Ù†ÙŠØ³Ø§Ù†', 'Ù…Ø§ÙŠÙˆ', 'Ø­Ø²ÙŠØ±Ø§Ù†',
		'ØªÙ…ÙˆØ²', 'Ø¢Ø¨', 'Ø£ÙŠÙ„ÙˆÙ„',	'ØªØ´Ø±ÙŠÙ† Ø§Ù„Ø£ÙˆÙ„', 'ØªØ´Ø±ÙŠÙ† Ø§Ù„Ø«Ø§Ù†ÙŠ', 'ÙƒØ§Ù†ÙˆÙ† Ø§Ù„Ø£ÙˆÙ„'],
		monthNamesShort: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
		dayNames: ['Ø§Ù„Ø£Ø­Ø¯', 'Ø§Ù„Ø§Ø«Ù†ÙŠÙ†', 'Ø§Ù„Ø«Ù„Ø§Ø«Ø§Ø¡', 'Ø§Ù„Ø£Ø±Ø¨Ø¹Ø§Ø¡', 'Ø§Ù„Ø®Ù…ÙŠØ³', 'Ø§Ù„Ø¬Ù…Ø¹Ø©', 'Ø§Ù„Ø³Ø¨Øª'],
		dayNamesShort: ['Ø§Ù„Ø£Ø­Ø¯', 'Ø§Ù„Ø§Ø«Ù†ÙŠÙ†', 'Ø§Ù„Ø«Ù„Ø§Ø«Ø§Ø¡', 'Ø§Ù„Ø£Ø±Ø¨Ø¹Ø§Ø¡', 'Ø§Ù„Ø®Ù…ÙŠØ³', 'Ø§Ù„Ø¬Ù…Ø¹Ø©', 'Ø§Ù„Ø³Ø¨Øª'],
		dayNamesMin: ['Ø­', 'Ù†', 'Ø«', 'Ø±', 'Ø®', 'Ø¬', 'Ø³'],
		weekHeader: 'Ø£Ø³Ø¨ÙˆØ¹',
		dateFormat: 'yy-mm-dd',
		firstDay: 6,
  		isRTL: true,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['ar']);
});