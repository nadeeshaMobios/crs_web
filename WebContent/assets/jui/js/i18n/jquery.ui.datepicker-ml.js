/* Malayalam (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Saji Nediyanchath (saji89@gmail.com). */
jQuery(function($){
	$.datepicker.regional['ml'] = {
		closeText: 'à´¶à´°à´¿',
		prevText: 'à´®àµ�à´¨àµ�à´¨à´¤àµ�à´¤àµ†',  
		nextText: 'à´…à´Ÿàµ�à´¤àµ�à´¤à´¤àµ� ',
		currentText: 'à´‡à´¨àµ�à´¨àµ�',
		monthNames: ['à´œà´¨àµ�à´µà´°à´¿','à´«àµ†à´¬àµ�à´°àµ�à´µà´°à´¿','à´®à´¾à´°àµ�â€�à´šàµ�à´šàµ�','à´�à´ªàµ�à´°à´¿à´²àµ�â€�','à´®àµ‡à´¯àµ�','à´œàµ‚à´£àµ�â€�',
		'à´œàµ‚à´²àµˆ','à´†à´—à´¸àµ�à´±àµ�à´±àµ�','à´¸àµ†à´ªàµ�à´±àµ�à´±à´‚à´¬à´°àµ�â€�','à´’à´•àµ�à´Ÿàµ‹à´¬à´°àµ�â€�','à´¨à´µà´‚à´¬à´°àµ�â€�','à´¡à´¿à´¸à´‚à´¬à´°àµ�â€�'],
		monthNamesShort: ['à´œà´¨àµ�', 'à´«àµ†à´¬àµ�', 'à´®à´¾à´°àµ�â€�', 'à´�à´ªàµ�à´°à´¿', 'à´®àµ‡à´¯àµ�', 'à´œàµ‚à´£àµ�â€�',
		'à´œàµ‚à´²à´¾', 'à´†à´—', 'à´¸àµ†à´ªàµ�', 'à´’à´•àµ�à´Ÿàµ‹', 'à´¨à´µà´‚', 'à´¡à´¿à´¸'],
		dayNames: ['à´žà´¾à´¯à´°àµ�â€�', 'à´¤à´¿à´™àµ�à´•à´³àµ�â€�', 'à´šàµŠà´µàµ�à´µ', 'à´¬àµ�à´§à´¨àµ�â€�', 'à´µàµ�à´¯à´¾à´´à´‚', 'à´µàµ†à´³àµ�à´³à´¿', 'à´¶à´¨à´¿'],
		dayNamesShort: ['à´žà´¾à´¯', 'à´¤à´¿à´™àµ�à´•', 'à´šàµŠà´µàµ�à´µ', 'à´¬àµ�à´§', 'à´µàµ�à´¯à´¾à´´à´‚', 'à´µàµ†à´³àµ�à´³à´¿', 'à´¶à´¨à´¿'],
		dayNamesMin: ['à´žà´¾','à´¤à´¿','à´šàµŠ','à´¬àµ�','à´µàµ�à´¯à´¾','à´µàµ†','à´¶'],
		weekHeader: 'à´†',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['ml']);
});
