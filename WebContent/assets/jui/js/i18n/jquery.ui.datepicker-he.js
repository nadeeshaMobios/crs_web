/* Hebrew initialisation for the UI Datepicker extension. */
/* Written by Amir Hardon (ahardon at gmail dot com). */
jQuery(function($){
	$.datepicker.regional['he'] = {
		closeText: '×¡×’×•×¨',
		prevText: '&#x3C;×”×§×•×“×�',
		nextText: '×”×‘×�&#x3E;',
		currentText: '×”×™×•×�',
		monthNames: ['×™× ×•×�×¨','×¤×‘×¨×•×�×¨','×ž×¨×¥','×�×¤×¨×™×œ','×ž×�×™','×™×•× ×™',
		'×™×•×œ×™','×�×•×’×•×¡×˜','×¡×¤×˜×ž×‘×¨','×�×•×§×˜×•×‘×¨','× ×•×‘×ž×‘×¨','×“×¦×ž×‘×¨'],
		monthNamesShort: ['×™× ×•','×¤×‘×¨','×ž×¨×¥','×�×¤×¨','×ž×�×™','×™×•× ×™',
		'×™×•×œ×™','×�×•×’','×¡×¤×˜','×�×•×§','× ×•×‘','×“×¦×ž'],
		dayNames: ['×¨×�×©×•×Ÿ','×©× ×™','×©×œ×™×©×™','×¨×‘×™×¢×™','×—×ž×™×©×™','×©×™×©×™','×©×‘×ª'],
		dayNamesShort: ['×�\'','×‘\'','×’\'','×“\'','×”\'','×•\'','×©×‘×ª'],
		dayNamesMin: ['×�\'','×‘\'','×’\'','×“\'','×”\'','×•\'','×©×‘×ª'],
		weekHeader: 'Wk',
		dateFormat: 'yy-mm-dd',
		firstDay: 0,
		isRTL: true,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['he']);
});
