/* Persian (Farsi) Translation for the jQuery UI date picker plugin. */
/* Javad Mowlanezhad -- jmowla@gmail.com */
/* Jalali calendar should supported soon! (Its implemented but I have to test it) */
jQuery(function($) {
	$.datepicker.regional['fa'] = {
		closeText: 'Ø¨Ø³ØªÙ†',
		prevText: '&#x3C;Ù‚Ø¨Ù„ÛŒ',
		nextText: 'Ø¨Ø¹Ø¯ÛŒ&#x3E;',
		currentText: 'Ø§Ù…Ø±ÙˆØ²',
		monthNames: [
			'Ù�Ø±ÙˆØ±Ø¯ÙŠÙ†',
			'Ø§Ø±Ø¯ÙŠØ¨Ù‡Ø´Øª',
			'Ø®Ø±Ø¯Ø§Ø¯',
			'ØªÙŠØ±',
			'Ù…Ø±Ø¯Ø§Ø¯',
			'Ø´Ù‡Ø±ÙŠÙˆØ±',
			'Ù…Ù‡Ø±',
			'Ø¢Ø¨Ø§Ù†',
			'Ø¢Ø°Ø±',
			'Ø¯ÛŒ',
			'Ø¨Ù‡Ù…Ù†',
			'Ø§Ø³Ù�Ù†Ø¯'
		],
		monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],
		dayNames: [
			'ÙŠÚ©Ø´Ù†Ø¨Ù‡',
			'Ø¯ÙˆØ´Ù†Ø¨Ù‡',
			'Ø³Ù‡â€ŒØ´Ù†Ø¨Ù‡',
			'Ú†Ù‡Ø§Ø±Ø´Ù†Ø¨Ù‡',
			'Ù¾Ù†Ø¬Ø´Ù†Ø¨Ù‡',
			'Ø¬Ù…Ø¹Ù‡',
			'Ø´Ù†Ø¨Ù‡'
		],
		dayNamesShort: [
			'ÛŒ',
			'Ø¯',
			'Ø³',
			'Ú†',
			'Ù¾',
			'Ø¬', 
			'Ø´'
		],
		dayNamesMin: [
			'ÛŒ',
			'Ø¯',
			'Ø³',
			'Ú†',
			'Ù¾',
			'Ø¬', 
			'Ø´'
		],
		weekHeader: 'Ù‡Ù�',
		dateFormat: 'yy-mm-dd',
		firstDay: 6,
		isRTL: true,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['fa']);
});