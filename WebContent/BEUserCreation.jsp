<%@page import="com.evidu.bank.entitiy.*"%>
<%@page import="com.evidu.bank.sva.*"%>
<%@page import="java.util.ArrayList"%>

<!DOCTYPE html>

<%
response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
//response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
int timeout = session.getMaxInactiveInterval();
response.setHeader("Refresh", timeout + "; URL = index.jsp");
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility

%>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->

<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Bootstrap Stylesheet -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" media="all">

<!-- jquery-ui Stylesheets -->
<link rel="stylesheet" href="assets/jui/css/jquery-ui.css" media="screen">
<link rel="stylesheet" href="assets/jui/jquery-ui.custom.css" media="screen">
<link rel="stylesheet" href="assets/jui/timepicker/jquery-ui-timepicker.css" media="screen">

<!-- Uniform Stylesheet -->
<link rel="stylesheet" href="plugins/uniform/css/uniform.default.css" media="screen">

<!-- Plugin Stylsheets first to ease overrides -->

<!-- Wizard Stylesheet -->
<link rel="stylesheet" href="custom-plugins/wizard/wizard.css">

<!-- End Plugin Stylesheets -->

<!-- Main Layout Stylesheet -->
<link rel="stylesheet" href="assets/css/fonts/icomoon/style.css" media="screen">
<link rel="stylesheet" href="assets/css/main-style.css" media="screen">

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<link href="css/elements.css" rel="stylesheet">

<title>Tap'n Pay</title>
<meta http-equiv="imagetoolbar" content="no" />
</head>

<body data-show-sidebar-toggle-button="false" data-fixed-sidebar="false"  oncontextmenu="return false">
											
  <%
  UserInfo userData =new UserInfo();
		userData= (UserInfo)session.getAttribute("current_user");
		
		if(userData==null){
			response.sendRedirect("index.jsp");
			return;
		}
		String errCode=""+request.getParameter("st");
		//getBranchNameList
		String SQL="SELECT * FROM branches";
		BranchController brController = new BranchController();
		ArrayList<Branches> brancharr = new ArrayList<Branches>();
		brancharr=brController.getBranchNameList(SQL);
  %> 

    <div id="wrapper">
        <header id="header" class="navbar navbar-inverse">
            <div class="navbar-inner">
                <div class="container">
					<div class="brand-wrap pull-left">
						<div class="brand-img">
							<a class="brand" href="#">
								<img src="assets/images/evidu_logo.png" alt="" style="width: 144px;position: relative;left: -21px;" >
							</a>
						</div>
					</div>
                    
                    <div id="header-right" class="clearfix">
						<div id="nav-toggle" data-toggle="collapse" data-target="#navigation" class="collapsed">
							<i class="icon-caret-down"></i>
						</div>
						
                        
                        <div id="header-functions" class="pull-right">
                        	<div id="user-info" class="clearfix">
                                <span class="info">
                                	Welcome
                                    <span class="name"><%=userData.getDisplay_name() %></span>
                                </span>
                            	<div class="avatar">
                                	<a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    	<img src="assets/images/defaltUser.png" alt="Avatar">
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                    	<li><a href="profile.html"><i class="icol-user"></i> My Profile</a></li>
                                    	<li><a href="#"><i class="icol-layout"></i> My Invoices</a></li>                                        
                                        <li class="divider"></li>
                                        <li><a href="index.jsp"><i class="icol-key"></i> Logout</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="logout-ribbon">
                            	<a href="index.jsp"><i class="icon-off"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        
        <div id="content-wrap">
        	<div id="content" class="sidebar-minimized">
            	<div id="content-outer">
                	<div id="content-inner">
                    	<aside id="sidebar">
                        	<nav id="navigation" class="collapse">
                            	<ul>
                                	<li ><a href="AdminHome.jsp" >
                                    	<span title="Home">
                                    		<i class="icon-home"></i>
											<span class="nav-title">Dashboard</span>
                                        </span>
                                    	</a>
                                    </li>
                                	<li class="active" ><a href="BEUserCreation.jsp" >
                                    	<span title="New Entry">
                                    		<i class="icon-plus-sign"></i>
											<span class="nav-title">User Management</span>
                                        </span>
                                    	</a>
                                    </li>
                                </ul>
                            </nav>
                        </aside>

                        <div id="sidebar-separator"></div>
                        
                        <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-home"></i>Tap&#39;n Pay
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="BEUserCreation.jsp">Add User</a>
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    
                                </ul>
                                
                                
                            </div>
                            
                            <div id="main-content">
                                
                               
								
								 <div id="update_userdata_div"  class="row-fluid">
								
									<div class="span12 widget">
										<div class="widget-header">
											<span class="title"><i class="icol-wand"></i>Bank Executive Details</span>
											<div class="toolbar">
												<div class="btn-group">
													
												</div>
											</div>
										</div>
										<div class="widget-content form-container">
										  
											<form id="wizard-demo-2"  method="post" class="form-horizontal" action="BEController.jsp" data-forward-only="true"  enctype="multipart/form-data" style="
    											background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(242, 242, 242, 0.36));">
												
												<fieldset class="wizard-step">
													<legend class="wizard-label"><i class="icol-user-business-boss" style="position: relative; right: 4px;"></i>PERSONAL DETAILS</legend>
													<table>
														<tr>
															<td>
																<div class="control-group" style=" width:518px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">Full Name<span class="required">*</span></label>
																	<div class="controls">
																		 <input type="text" name="be_full_name" class="required span10"> 
																	</div>
																</div>
															</td>
															<td>
																<div class="control-group" style=" width:593px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">Mobile Number<span class="required">*</span></label>
																	<div class="controls">
																		<input type="text" name="be_mobile_number" class="required span8">
																	</div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="control-group" style=" width:518px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">Display Name<span class="required">*</span></label>
																	<div class="controls">
																		<input type="text" name="be_display_name" class="required span10">
																	</div>
																</div>
															</td>
															<td>
																<div class="control-group" style=" width:593px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">Date of Birth<span class="required">*</span></label>
																	<div class="controls">
																	<input id="summery-data-date-from" name="be_birthday_date_from" value="1996-01-01" class="required span8 ndshaatepicker-basic" type="text"
																	value="">
															
																	</div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="control-group" style=" width:518px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">Profession<span class="required">*</span></label>
																	<div class="controls">
																		<input type="text" name="be_profession" class="required span10">
																	</div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="control-group" style=" width:518px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">Photo ID document type<span class="required">*</span></label>
																	<div class="controls">
																	<select name="be_id_type" class="required span10">
																			<option value="NID">NID </option>
																			<option value="passport">Passport </option>
																			<option value="driving_licence">Driving License </option>
																			
																		</select>
																		</div>
																</div>
															</td>
															<td>
																<div class="control-group" style=" width:593px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">Number<span class="required">*</span></label>
																	<div class="controls">
																		<input type="text"  name="be_id_number" class="required span8">
																	</div>
																</div>
															</td>
														</tr>
													</table>
												</fieldset>	
										
												<fieldset class="wizard-step">
													<legend class="wizard-label"><i class="icol-book-addresses" style="position: relative; right: 4px;"></i>Contact Information</legend>
													<table>
														<tr>
															<td colspan="2">
															<div class="navbar-inner" style="background-image: linear-gradient(to bottom, rgba(0,0,0,.12), #f2f2f2);">
															<div class="control-group" style="padding-bottom: 8px;padding-top: 9px;">
																<i class="icol-house" ></i><b style=" position: relative;left: 10px;">Permanent Address</b>
															</div>
															</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="control-group" style=" width:518px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">House Number<span class="required">*</span></label>
																	<div class="controls">
																		<input type="text" name="be_permanent_address_h_no" id="be_permanent_address_h_no" class="required span10">
																	</div>
																</div>
															</td>
															<td>
																<div class="control-group" style=" width:593px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">Street<span class="required">*</span></label>
																	<div class="controls">
																		<input type="text"  name="be_permanent_address_street" id="be_permanent_address_street" class="required span8">
																	</div>
																</div>
															</td>
															
														</tr>
														<tr>
															<td>
																<div class="control-group" style=" width:518px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">City<span class="required">*</span></label>
																	<div class="controls">
																		<input type="text"  name="be_permanent_address_city" id="be_permanent_address_city" class="required span10">
																	</div>
																</div>
															</td>
															<td>
																<div class="control-group" style=" width:593px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">State<span class="required">*</span></label>
																	<div class="controls">
																		<input type="text"  name="be_permanent_address_state" id="be_permanent_address_state" class="required span8">
																	</div>
																</div>
															</td>
														</tr>
														<tr>
															<td colspan="2">
																<div class="navbar-inner" style="background-image: linear-gradient(to bottom, rgba(0,0,0,.12), #f2f2f2);">
																<div class="control-group" style="padding-bottom: 8px;padding-top: 9px;">
																	<i class="icol-envelope" ></i><b style=" position: relative;left: 10px;">Mailing Address</b>
																	<div class="controls" style="display: inline-block;position: relative;right: 91px;">
																		<input type="checkbox" name="address_check" id="address_check" onclick="SameAddress()"> Same as Permanent address.
																	</div>
																</div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="control-group" style=" width:518px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">House Number<span class="required">*</span></label>
																	<div class="controls">
																		<input type="text" name="be_mailling_address_h_no" id="be_mailling_address_h_no" class="required span10">
																	</div>
																</div>
															</td>
															<td>
																<div class="control-group" style=" width:593px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">Street<span class="required">*</span></label>
																	<div class="controls">
																		<input type="text"  name="be_mailling_address_street" id="be_mailling_address_street" class="required span8">
																	</div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">City<span class="required">*</span></label>
																	<div class="controls">
																		<input type="text"  name="be_mailling_address_city" id="be_mailling_address_city" class="required span10">
																	</div>
																</div>
															</td>
															<td>
																<div class="control-group" style="width:593px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">State<span class="required">*</span></label>
																	<div class="controls">
																		<input type="text"  name="be_mailling_address_state" id="be_mailling_address_state"  class="required span8">
																	</div>
																</div>
															</td>
														</tr>
													</table>
												</fieldset>
												
												<fieldset class="wizard-step">
													<legend class="wizard-label"><i class="icol-accept" style="position: relative; right: 4px;"></i>AUTHORIZATION</legend>
													<table>
														<tr>
															<td>
																<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">Executive Role <span class="required">*</span></label>
																	<div class="controls">
																		<label class="radio" style="display: inline-block"><input type="radio" name="be_role" class="required" value="BranchM">Branch Maker</label>
																		<label class="radio" style="display: inline-block; position: relative;left: 17px;"><input type="radio" name="be_role" class="required" value="BranchC">Branch Checker</label>
																	</div>
																</div>
															</td>
															<td>
																<div class="control-group" style="width:593px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">Branch Name <span class="required">*</span></label>
																	<div class="controls">
																		<select name="be_branch_name" class="required span8">
																		<option></option>
																		<%for(int i=0;i<brancharr.size();i++){%>
																		<option value="<%=brancharr.get(i).getBRANCH_CODE()%>"><%=brancharr.get(i).getBRANCH_NAME() %></option>	
																		<%} %>
																		</select>
																	</div>
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
				                                                     <label class="control-label">Upload NID Image<span class="required">*</span></label>
				                                                     <div class="controls">
				                                                        <input type="file" id="photo1"  name="photo1"  class="required span10" onchange="checkPhoto(this)">
				                                                      <div id="photoLabel"></div> 
				                                                      </div>
				                                                </div>
															</td>
														</tr>
														<tr>
															<td>
																 <div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
				                                                     <label class="control-label">Upload User Image<span class="required">*</span></label>
				                                                     <div class="controls">
				                                                        <input type="file" id="photo2"  name="photo2"  class="required span10" onchange="checkPhoto(this)">
				                                                     <div id="photoLabel"></div> 
				                                                     </div>
				                                                </div>
															</td>
														</tr>
													</table>
												</fieldset>
											</form>
										</div>
									
									</div>
								 <%if(errCode.equalsIgnoreCase("crr")){ 
								 //System.out.println("_____________"+errCode);
								 %>
									        
										        <script type="text/javascript">
										       
									       		 window.onload=function(){alertOnload("Branch executive user created successfully", "CRS", "OK");
									   			     };
									       
										        </script>
									      <%}if(errCode.equalsIgnoreCase("err")){
									    	  %>
										        <script type="text/javascript">
										        window.onload=function(){alertOnload("Error on User creation.", "CRS", "OK");
								   			     };	
								   			     </script>
									     <% } if(errCode.equalsIgnoreCase("err2")){
									    	  %>
										        <script type="text/javascript">
										        window.onload=function(){alertOnload("Branch Executive mobile number already has an account.", "CRS", "OK");
								   			     };	
								   			     </script>
									     <% } %>
                                                 <script type="text/javascript">
                                                function alertOnload(message, title, buttonText) {
													 buttonText = (buttonText == undefined) ? "Ok" : buttonText;
												    title = (title == undefined) ? "The page says:" : title;
												
												    var div = $('<div>');
												    div.html(message);
												    div.attr('title', "CRS");
												    div.dialog({
												        autoOpen: true,
												        modal: true,
												        draggable: false,
												        resizable: false,
												        buttons: [{
												            text: buttonText,
												            click: function () {
												                $(this).dialog("close");
												                div.remove();
												              
												            }
												        }]
												    });
												   
												}
                                                
                                                function SameAddress()
                                                {
                                                	//alert("checked");
                                                   if (document.getElementById('address_check').checked) 
                                                  { 
                                                	  
                                                	   var house_num=document.getElementById('be_permanent_address_h_no').value;
                                                	   var street=document.getElementById('be_permanent_address_street').value;
                                                	   var city=document.getElementById('be_permanent_address_city').value;
                                                	   var state=document.getElementById('be_permanent_address_state').value;
                                                	  // alert(house_num);
                                                	   document.getElementById('be_mailling_address_h_no').value=house_num;
                                                	   document.getElementById('be_mailling_address_street').value=street;
                                                	   document.getElementById('be_mailling_address_city').value=city ;
                                                	   document.getElementById('be_mailling_address_state').value=state ;
                                                	   
                                                  } else{
	                                                   document.getElementById('be_mailling_address_h_no').value="";
	                                               	   document.getElementById('be_mailling_address_street').value="";
	                                               	   document.getElementById('be_mailling_address_city').value="" ;
	                                               	   document.getElementById('be_mailling_address_state').value="" ;
                                                 }
                                                }
                                                
                                                
                                               
                                                </script>
								
								</div>
                                
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        
        <footer id="footer">
            <div class="footer-left">Mobile Banking Platform</div>
            <div class="footer-right"><p>Copyright 2016 - Evidu Private Limited All Rights Reserved.</p></div>
        </footer>
        
    </div>

	<!-- Core Scripts -->
	<script src="assets/js/libs/jquery-1.8.3.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/libs/jquery.placeholder.min.js"></script>
	<script src="assets/js/libs/jquery.mousewheel.min.js"></script>

    <!-- Template Script -->
    <script src="assets/js/template.js"></script>
    <script src="assets/js/setup.js"></script>

    <!-- Customizer, remove if not needed -->
    <script src="assets/js/customizer.js"></script>

    <!-- Uniform Script -->
    <script src="plugins/uniform/jquery.uniform.min.js"></script>
    
    <!-- jquery-ui Scripts -->
    <script src="assets/jui/js/jquery-ui-1.9.2.min.js"></script>
    <script src="assets/jui/jquery-ui.custom.min.js"></script>
    <script src="assets/jui/timepicker/jquery-ui-timepicker.min.js"></script>
	<script src="assets/jui/jquery.ui.touch-punch.min.js"></script>
    
    <!-- Plugin Scripts -->
    
	<!-- Validation -->
	<script src="plugins/validate/jquery.validate.min.js"></script>
	
	<!-- Wizard -->
	<script src="custom-plugins/wizard/wizard.min.js"></script>
    <script src="custom-plugins/wizard/jquery.form.min.js"></script>

   
    <!-- Demo Scripts -->
    <script src="assets/js/demo/form_wizard.js"></script>
	
	<!-- newly added js -->
	
	<script src="plugins/flot/jquery.flot.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.tooltip.min.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.pie.min.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.orderBars.min.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.resize.min.js"></script>

<script src="js/my_js.js"></script>

	 <!-- Demo Scripts -->
    <script src="assets/js/getRefNumberDetail.js"></script>


	
	<script src="assets/js/demo/ui_comps.js"></script>
	<!--  --------------------------------------------------------->



</body>

</html>
