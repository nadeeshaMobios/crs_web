<%@page import="com.evidu.bank.gson.*"%>
<%@page import="com.evidu.bank.sva.*"%>
<%@page import="com.google.gson.Gson"%>

<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStream"%>
<%@ page import="java.util.UUID"%>
 <%@ page import="com.mobios.util.*"%>

<%
String ID = ""+UUID.randomUUID().toString();
String platformID="ETI-IVR";
String serverIP=""+request.getLocalAddr();
String ip =""+request.getRemoteAddr();
String softVersion="V1.2";
String logData=""+serverIP+","+ip+","+softVersion+","+platformID+","+ID+",";
Gson gson = new Gson();

String reqMethod = request.getMethod();
String requestContent = null;
TemporaryAccount JsontempAcc= new TemporaryAccount();
ResponseTemporaryAccJson responseTempAcc= new ResponseTemporaryAccJson();
JsonController jController = new JsonController();
TemporaryAccount dbcelectedtempAcc= new TemporaryAccount();
boolean q_status=false;
String ErrorCode="2";
if(reqMethod.equals("POST")){
	 System.out.println("*************1");
	requestContent = readStringContent(request);
	try{
	JsontempAcc= gson.fromJson(requestContent, TemporaryAccount.class);

	
	responseTempAcc.setCustomerMobile(JsontempAcc.getCustomerMobile());
	responseTempAcc.setCustomerRefno(JsontempAcc.getCustomerRefno());
	}catch(Exception e){
		e.printStackTrace();
		ErrorCode="4";// incorrect_json
		LogUtil.getLog("Event").debug(logData+","+e.toString()+",incorrect_json,ErrorCode:"+ErrorCode+","+requestContent);
		LogUtil.getLog("Error").debug(logData+","+e.toString()+",incorrect_json,ErrorCode:"+ErrorCode+","+requestContent);
	}
	String selectSQL="SELECT * FROM tempory_account WHERE MSISDN='"+JsontempAcc.getCustomerMobile()+"'";
	
	if(!ErrorCode.equals("4")){
		dbcelectedtempAcc=jController.getAccountDetail(selectSQL);
		if(dbcelectedtempAcc.getCustomerMobile().equals(null)){
			ErrorCode="2";// no_record
			LogUtil.getLog("Event").debug(logData+",no_record,ErrorCode:"+ErrorCode+","+requestContent);
			LogUtil.getLog("Error").debug(logData+",no_record,ErrorCode:"+ErrorCode+","+requestContent);
		}
		if( dbcelectedtempAcc.getCustomerRefno().equals(JsontempAcc.getCustomerRefno()) ){
			if(!dbcelectedtempAcc.getCustomerReqStatus().equals("deleted")){
				
				 String SQL="UPDATE tempory_account SET STATUS='deleted',DELETED_REASON='"+JsontempAcc.getCustomerReqStatus()+"' WHERE MSISDN='"+JsontempAcc.getCustomerMobile()+"' AND REF_NO='"+JsontempAcc.getCustomerRefno()+"'";
				 System.out.println("*************2"+dbcelectedtempAcc.getCustomerReqStatus());
				 AccountController ac=new AccountController();
				 q_status=ac.InsertData(SQL);
				 ErrorCode="0"; //success
				 LogUtil.getLog("Event").debug(logData+",success,ErrorCode:"+ErrorCode+","+requestContent);
			}else{
				ErrorCode="1";//alredy deleted
				LogUtil.getLog("Event").debug(logData+",alredy_deleted,ErrorCode:"+ErrorCode+","+requestContent);
				LogUtil.getLog("Error").debug(logData+",alredy_deleted,ErrorCode:"+ErrorCode+","+requestContent);
			}
		}else{
			ErrorCode="3";//incorrect_ref_number
			LogUtil.getLog("Event").debug(logData+",incorrect_ref_number,ErrorCode:"+ErrorCode+","+requestContent);
			LogUtil.getLog("Error").debug(logData+",incorrect_ref_number,ErrorCode:"+ErrorCode+","+requestContent);
		}
	}
	responseTempAcc.setCustomerResponseStatus(ErrorCode);
	
	String jsonString = gson.toJson(responseTempAcc);

	System.out.println(jsonString);
	response.getWriter().write(jsonString);

}
%>
<%!
private String readStringContent(HttpServletRequest req) throws IOException {
    InputStream is = req.getInputStream();
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

    StringBuilder content = new StringBuilder();
    String line;
    
    while ((line = bufferedReader.readLine()) != null) {
      content.append(line);
    }
    System.out.println(content.toString());
    return content.toString();
}

%>