<%@page import="com.evidu.bank.entitiy.*"%>
<!DOCTYPE html>

<%
response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
//response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
int timeout = session.getMaxInactiveInterval();
response.setHeader("Refresh", timeout + "; URL = index.jsp");
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility

%>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->

<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Bootstrap Stylesheet -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" media="all">

<!-- jquery-ui Stylesheets -->
<link rel="stylesheet" href="assets/jui/css/jquery-ui.css" media="screen">
<link rel="stylesheet" href="assets/jui/jquery-ui.custom.css" media="screen">
<link rel="stylesheet" href="assets/jui/timepicker/jquery-ui-timepicker.css" media="screen">

<!-- Uniform Stylesheet -->
<link rel="stylesheet" href="plugins/uniform/css/uniform.default.css" media="screen">

<!-- Plugin Stylsheets first to ease overrides -->

<!-- Wizard Stylesheet -->
<link rel="stylesheet" href="custom-plugins/wizard/wizard.css">

<!-- End Plugin Stylesheets -->

<!-- Main Layout Stylesheet -->
<link rel="stylesheet" href="assets/css/fonts/icomoon/style.css" media="screen">
<link rel="stylesheet" href="assets/css/main-style.css" media="screen">

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<title>Tap'n Pay</title>
<script type="text/javascript">
function SameAddress()
{
	//alert("checked");
   if (document.getElementById('address_check').checked) 
  { 
	  
	   var house_num=document.getElementById('cs_permanent_address_h_no').value;
	   var street=document.getElementById('cs_permanent_address_street').value;
	   var city=document.getElementById('cs_permanent_address_city').value;
	   var state=document.getElementById('cs_permanent_address_state').value;
	   var distric=document.getElementById('district').value;
	   var thana =document.getElementById('dealer_org_thana').value;
	   var union=document.getElementById('union').value;
	   var division=document.getElementById('person_division').value;
	   var post_code=document.getElementById('person_post_code').value;
	   
		//alert("THANA : "+thana);
		//alert("UNION : "+union);
	  	  
	   document.getElementById('cs_mailling_address_h_no').value=house_num;
	   document.getElementById('cs_mailling_address_street').value=street;
	   document.getElementById('cs_mailling_address_city').value=city ;
	   document.getElementById('cs_mailling_address_state').value=state;
	   document.getElementById('personal_district').value=distric;
	  
	   alert("THANA** : "+thana);
	   changePersonalDistric(distric);
	   document.getElementById('person_thana').value=thana;
	   //document.getElementById('person_union').value=union;
	
	   document.getElementById('mail_division').value=division;
	   document.getElementById('mail_post_code').value=post_code;
	   
  }else{
	  document.getElementById('cs_mailling_address_h_no').value="";
	   document.getElementById('cs_mailling_address_street').value="";
	   document.getElementById('cs_mailling_address_city').value="";
	   document.getElementById('cs_mailling_address_state').value="";
	   document.getElementById('personal_district').value="";
	   document.getElementById('person_thana').value="";
	   document.getElementById('person_union').value="";
	   document.getElementById('mail_division').value="";
	   document.getElementById('mail_post_code').value="";
  }
   
}
</script>
</head>

<body data-show-sidebar-toggle-button="true" data-fixed-sidebar="false" onload="districList();personDistricList();nomineeDistricList();">

  <%
  UserInfo userData =new UserInfo();
		userData= (UserInfo)session.getAttribute("current_user");
		if(userData==null){
			response.sendRedirect("index.jsp");
			return;
		}
		String errCode=""+request.getParameter("st");
  %> 

    <div id="wrapper">
        <header id="header" class="navbar navbar-inverse">
            <div class="navbar-inner">
                <div class="container">
					<div class="brand-wrap pull-left">
						<div class="brand-img">
							<a class="brand" href="#">
								<img src="assets/images/Tap n PAy_original.png" alt="" style="width: 87px;position: relative;left: -17px;" >>
							</a>
						</div>
					</div>
                    
                    <div id="header-right" class="clearfix">
						<div id="nav-toggle" data-toggle="collapse" data-target="#navigation" class="collapsed">
							<i class="icon-caret-down"></i>
						</div>
						<div id="header-search">
							<span id="search-toggle" data-toggle="dropdown">
								<i class="icon-search"></i>
							</span>
							<form class="navbar-search">
								<input type="text" class="search-query" placeholder="Search">
							</form>
						</div>
						<div id="dropdown-lists">
                            <div class="item-wrap">
    							<a class="item" href="#" data-toggle="dropdown">
    								<span class="item-icon"><i class="icon-exclamation-sign"></i></span>
    								<span class="item-label">Notifications</span>
    								<span class="item-count">4</span>
    							</a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-item-wrap">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>John Doe</strong> commented on your photo
                                                        <span class="time">13 minutes ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>Jane Roe</strong> commented on your photo
                                                        <span class="time">27 minutes ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>Billy John</strong> commented on your photo
                                                        <span class="time">43 minutes ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">View all notifications</a></li>
                                </ul>
                            </div>
                            <div class="item-wrap">
    							<a class="item" href="#" data-toggle="dropdown">
    								<span class="item-icon"><i class="icon-envelope"></i></span>
    								<span class="item-label">Messages</span>
    								<span class="item-count">16</span>
    							</a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-item-wrap">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>John Doe</strong><br> Hello, do you have time to go out tomorrow?
                                                        <span class="time">13 minutes ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>Jane Roe</strong><br> Hey, the reports said that you were...
                                                        <span class="time">27 minutes ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>Billy John</strong><br> Can I borrow your new camera for taking...
                                                        <span class="time">About an hour ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="mail.html">View all messages</a></li>
                                </ul>
                            </div>
						</div>
                        <div id="header-functions" class="pull-right">
                        	<div id="user-info" class="clearfix">
                                <span class="info">
                                	Welcome
                                    <span class="name"><%=userData.getDisplay_name() %></span>
                                </span>
                            	<div class="avatar">
                                	<a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    	<img src="assets/images/defaltUser.png" alt="Avatar">
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                    	<li><a href="profile.html"><i class="icol-user"></i> My Profile</a></li>
                                    	<li><a href="#"><i class="icol-layout"></i> My Invoices</a></li>                                        
                                        <li class="divider"></li>
                                        <li><a href="index.jsp"><i class="icol-key"></i> Logout</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="logout-ribbon">
                            	<a href="index.jsp"><i class="icon-off"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        
        <div id="content-wrap">
        	<div id="content" class="sidebar-minimized">
            	<div id="content-outer">
                	<div id="content-inner">
                    	<aside id="sidebar">
                        	<nav id="navigation" class="collapse">
                            	<ul>
                            	<li ><a href="DealerDashboard.jsp" >
                                    	<span title="Home">
                                    		<i class="icon-home"></i>
											<span class="nav-title">Dashboard</span>
                                        </span>
                                    	</a>
                                    </li>
                                	<li ><a href="DEORefSearch.jsp" >
                                    	<span title="Home">
                                    		<i class="icon-business-card"></i>
											<span class="nav-title">Add Customer</span>
                                        </span>
                                    	</a>
                                    </li>
                                	<li  class="active"><a href="DEONewEntry.jsp" >
                                    	<span title="New Entry">
                                    		<i class="icon-plus-sign"></i>
											<span class="nav-title">New Entry</span>
                                        </span>
                                    	</a>
                                    </li>
                                	
                                	<li ><a href="AddAgent.jsp" >
                                    	<span title="Add  Agent">
                                    		<i class="icon-user"></i>
											<span class="nav-title">Agent/DSR Registration</span>
                                        </span>
                                    	</a>
                                    </li>
                                	
                                </ul>
                            </nav>
                        </aside>

                        <div id="sidebar-separator"></div>
                        
                        <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-home"></i>Tap&#39;n Pay
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="DEONewEntry.jsp">New Entry</a>
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    
                                </ul>
                                
                                <h1 id="main-heading">
                                	Add new Account
                                </h1>
                            </div>
                            
                            <div id="main-content">
                                
                                <div class="row-fluid">
								
									<div class="span12 widget">
										<div class="widget-header">
											<span class="title"><i class="icol-wand"></i>New Account Details</span>
											<div class="toolbar">
												<div class="btn-group">
													
												</div>
											</div>
										</div>
										<div class="widget-content form-container">
										  
											<form id="wizard-demo-2"  method="post" class="form-horizontal" action="DEOController_edit.jsp" data-forward-only="true"  enctype="multipart/form-data" style="
    											background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(242, 242, 242, 0.36));">
												
												<fieldset class="wizard-step">
													<legend class="wizard-label"><i class="icol-user-business-boss" style="position: relative; right: 4px;"></i>PERSONAL DETAILS</legend>
													<table>
													<tr>
													<td>
														<div class="control-group" style=" width: 518px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label">Mobile number<span class="required">*</span></label>
															<div class="controls">
																<input type="text" name="cs_mobiole_no" class="required span10" />
															</div>
														</div>
													</td>
													<td>
														<div class="control-group" style=" width: 593px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label">Customer Name<span class="required">*</span></label>
															<div class="controls">
																<input type="text" name="cs_name" class="required span8" style="text-transform:uppercase">
															</div>
														</div>
													</td>
													</tr>
													<tr>
													<td>
														<div class="control-group" style=" width: 518px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label">Father&#39;s Name<span class="required">*</span></label>
															<div class="controls">
																<input type="text" name="cs_fathers_name" class="required span10">
															</div>
														</div>
													</td>
													<td>
														<div class="control-group" style=" width: 593px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label">Mother&#39;s Name<span class="required">*</span></label>
															<div class="controls">
																<input type="text" name="cs_mothers_name" class="required span8">
															</div>
														</div>
													</td>
													</tr>
													<tr>
													<td>
														<div class="control-group" style=" width: 518px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label">Date of Birth<span class="required">*</span></label>
															<div class="controls">
															<input id="summery-data-date-from" name="summery-data-date-from" value="1996-01-01" class="required span10 ndshaatepicker-basic" type="text"
															value="">
																
															</div>
														</div>
													</td>
													<td>
														<div class="control-group" style=" width: 593px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">Gender<span class="required">*</span></label>
														<div class="controls">
															<label class="radio" style="display:inline-block;"><input type="radio" name="gender" value ="female" class="required"> Female</label>
															<label class="radio" style="display:inline-block; position: relative;left: 17px;"><input type="radio" name="gender" value ="male" class="required"> Male</label>
														</div>
														</div>
													</td>
													</tr>
													<tr>
													<td>
														<div class="control-group" style=" width: 518px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label">Spouse Name<span class="required">*</span></label>
															<div class="controls">
																<input type="text" name="cs_spouse_name" class="required span10">
															</div>
														</div>
													</td>
													
													<td>
														<div class="control-group" style=" width: 593px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">Nationality<span class="required">*</span></label>
														<div class="controls">
															<label class="radio" style="display: inline-block;"><input type="radio" name="national" value ="bangaladesh" class="required"> Bangladeshi</label>
															<label class="radio" style="display: inline-block; position: relative;left: 17px;"><input type="radio" name="national" value ="other" class="required"> Other</label>
														</div>
														</div>
													
													</td>
													</tr>
													<tr>
													<td>
													<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">ID type<span class="required">*</span></label>
														<div class="controls">
														<select name="personal_id_type" class="required span10">
																<option value="" disabled selected> SELECT ID TYPE </option>
																<option value="NID">NID </option>
																<option value="passport">Passport </option>
																<option value="driving_licence">Driving Licence </option>
																<option value="student">Student </option>
															</select>
														</div>
													</div>
													</td>
													<td>
													<div class="control-group" style="width:593px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">ID Number<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="personal_id_number" class="required span8">
														</div>
													</div>
													</td>
													</tr>
													<tr>
														<td>
															<div class="control-group" style=" width: 518px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label">Occupation<span class="required">*</span></label>
															<div class="controls">
																<input type="text" name="cs_profession" class="required span10">
															</div>
															</div>
														</td>
													</tr>
													</table>
												</fieldset>
												
												<fieldset class="wizard-step">
													<legend class="wizard-label"><i class="icol-book" style="position: relative; right: 4px;"></i>CONTACT INFO</legend>
													<table>
													<tr>
													<td>
														<div class="control-group" style=" width: 518px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label">Emergency Contact No<span class="required">*</span></label>
															<div class="controls">
																<input type="text"  name="cs_emerg_contact_number" class="required span10">
															</div>
														</div>
													</td>
													<td>
														<div class="control-group" style=" width: 593px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label">Tin Number<span class="required">*</span></label>
															<div class="controls">
																<input type="text"  name="cs_tin_number" class="required span8">
															</div>
														</div>
													</td>
													</tr>
													<tr>
														<td colspan="2">
															<div class="navbar-inner" style="background-image: linear-gradient(to bottom, rgba(0,0,0,.12), #f2f2f2);">
															 <div class="control-group" style="padding-bottom: 8px;padding-top: 9px;">
																<i class="icol-house" ></i><b style=" position: relative;left: 10px;">Permanent Address</b>
															</div>
															</div>
														</td>
													</tr>
													
													<tr>
														<td>
															<div class="control-group" style=" width: 518px;padding-bottom: 8px;padding-top: 9px;">
																<label class="control-label">House Number<span class="required">*</span></label>
																<div class="controls">
																	<input type="text" name="cs_permanent_address_h_no" id="cs_permanent_address_h_no" class="required span10">
																</div>
															</div>
														</td>
														<td>
															<div class="control-group" style=" width: 593px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label">Street<span class="required">*</span></label>
																<div class="controls">
																	<input type="text"  name="cs_permanent_address_street" id="cs_permanent_address_street" class="required span8">
																</div>
															</div>
														</td>
													</tr>
													<tr>
														<td>
															<div class="control-group" style=" width: 518px;padding-bottom: 8px;padding-top: 9px;">
																<label class="control-label">City<span class="required">*</span></label>
																<div class="controls">
																	<input type="text"  name="cs_permanent_address_city" id="cs_permanent_address_city" class="required span10">
																</div>
															</div>
														
														</td>
														<td>
															<div class="control-group" style=" width: 593px;padding-bottom: 8px;padding-top: 9px;">
																<label class="control-label">State<span class="required">*</span></label>
																<div class="controls">
																	<input type="text"  name="cs_permanent_address_state" id="cs_permanent_address_state" class="required span8">
																</div>
															</div>
														</td>
													</tr>
													<tr>
														<td>
														<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label"> District<span class="required">*</span></label>
															<div class="controls">
																<select name="person_district" class="required span10" id="district" onchange="changeDist()">
																	<option value="" disabled selected>SELECT DISTRICT</option>
																	
																</select>
															</div>
														</div>
														</td>
													</tr>
													<tr>
														<td>
														<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label"> Than/Upazilla <span class="required">*</span></label>
															<div class="controls">
																<select name="peranat_thana" class="required span10" onchange="getUnion()" id="dealer_org_thana">
																	<option value="" disabled selected>SELECT THANA / UPAZILLA</option>
																	
																</select>
															</div>
														</div>
														</td>
													</tr>
													<tr>
														<td>
														<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label"> Union/Ward<span class="required">*</span></label>
															<div class="controls">
																<select name="perment_union" class="required span10" id="union" >
																	<option value="" disabled selected>SELECT UNION</option>
																</select>
															</div>
														</div>
														</td>
													</tr>
													<tr>
														<td>
															<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
																<label class="control-label">Division<span class="required">*</span></label>
																<div class="controls">
																	<input type="text" name="person_division" class="required span10" id="person_division">
																</div>
															</div> 
														</td>
														<td>
															<div class="control-group" style="width:593px;padding-bottom: 8px;padding-top: 9px;">
																<label class="control-label">Post Code<span class="required">*</span></label>
																<div class="controls">
																	<input type="text" name="person_post_code" class="required span8" id="person_post_code">
																</div>
															</div> 
														</td>
													</tr>
													<tr>
														<td colspan="2">
															<div class="navbar-inner" style="background-image: linear-gradient(to bottom, rgba(0,0,0,.12), #f2f2f2);">
															 <div class="control-group" style="padding-bottom: 8px;padding-top: 9px;">
																<i class="icol-envelope" ></i><b style=" position: relative;left: 10px;">Mailing Address</b>
																<div class="controls" style="display: inline-block;position: relative;right: 91px;">
																	<input type="checkbox" name="address_check" id="address_check" onclick="SameAddress()"> Same as Permanent address.
																</div>
															</div>
															</div>
														</td>
													</tr>
													<tr>
														<td>
															<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
																<label class="control-label">House Number<span class="required">*</span></label>
																<div class="controls">
																	<input type="text" name="cs_mailling_address_h_no" id="cs_mailling_address_h_no" class="required span10">
																</div>
															</div>
														</td>
														<td>
															<div class="control-group" style="width:593px;padding-bottom: 8px;padding-top: 9px;">
																<label class="control-label">Street<span class="required">*</span></label>
																<div class="controls">
																	<input type="text"  name="cs_mailling_address_street" id="cs_mailling_address_street" class="required span8">
																</div>
															</div>
														</td>
													</tr>
													<tr>
														<td>
															<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
																<label class="control-label">City<span class="required">*</span></label>
																<div class="controls">
																	<input type="text"  name="cs_mailling_address_city" id="cs_mailling_address_city" class="required span10">
																</div>
															</div>
														
														</td>
														<td>
															<div class="control-group" style="width:593px;padding-bottom: 8px;padding-top: 9px;">
																<label class="control-label">State<span class="required">*</span></label>
																<div class="controls">
																	<input type="text"  name="cs_mailling_address_state" id="cs_mailling_address_state"  class="required span8">
																</div>
															</div>
														</td>
													</tr>
													<tr>
														<td>
														<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label">District<span class="required">*</span></label>
															<div class="controls">
																<select name="mailling_district" class="required span10" id="personal_district" onchange="changePersonalDistric()">
																	<option value="" disabled selected>SELECT DISTRICT</option>
																</select>
															</div>
														</div>
														</td>
													</tr>
													<tr>
														<td>
														<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label">Than/Upazilla <span class="required">*</span></label>
															<div class="controls">
																<select name="mailling_thana" class="required span10" id="person_thana" onchange="changePersonThana()">
																	<!-- <option value="">SELECT THAN / UPAZILLA</option> -->
																</select>
															</div>
														</div>
														</td>
													</tr>
													<tr>
														<td>
														<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label"> Union/Ward<span class="required">*</span></label>
															<div class="controls">
																<select name="mailliing_union" class="required span10" id="person_union" >
																	<option value="" disabled selected>SELECT UNION / WARD</option>
																</select>
															</div>
														</div>
														</td>
													</tr>
													<tr>
														<td>
														<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
															<label class="control-label">Division<span class="required">*</span></label>
															<div class="controls">
																<input type="text" name="mail_division" class="required span10" id="mail_division">
															</div>
														</div>
														</td>
															<td>
															<div class="control-group" style="width:593px;padding-bottom: 8px;padding-top: 9px;">
																<label class="control-label">Post Code<span class="required">*</span></label>
																<div class="controls">
																	<input type="text" name="mail_post_code" class="required span8" id="mail_post_code">
																</div>
															</div> 
															</td>
													</tr>
													<tr>
														<td>
															<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
																<label class="control-label">Purpose of Transaction<span class="required">*</span></label>
																<div class="controls">
																	<input type="text" name="purpose_transaction" class="required span10">
																</div>
															</div> 
														</td>
													</tr>
													</table>
												</fieldset>
												<fieldset class="wizard-step">
													<legend class="wizard-label"><i class="icol-zone-money" style="position: relative; right: 4px;"></i>OTHER BANK ACCOUNT</legend>
													<table>
														<tr>
															<td>
																<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">Bank name</label>
																	<div class="controls">
																		<input type="text" name="oth_bank_name" class=" span10">
																	</div>
																</div>
															</td>
														</tr>
														
														<tr>
															<td>
																<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">Account no</label>
																	<div class="controls">
																		<input type="text"  name="oth_account_no" class=" span10">
																	</div>
																</div>
															</td>
														</tr>
														
														<tr>
															<td>
																<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
																	<label class="control-label">Branch name</label>
																	<div class="controls">
																		<input type="text"  name="oth_branch_name" class=" span10">
																	</div>
																</div>
															</td>
														</tr>
													</table>
												</fieldset>
												
												<fieldset class="wizard-step">
													<legend class="wizard-label"><i class="icol-user" style="position: relative; right: 4px;"></i>NOMINEE</legend>
													<table>
													<tr>
													<td>
													<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">Nominee Name<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="nominee_name" class="required span10">
														</div>
													</div>
													</td>
													<td>
													<div class="control-group" style="width:593px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">Relation<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="nominee_relation" class="required span8">
														</div>
													</div>
													</td>
													</tr>
													
													<tr>
													<td>
													<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">Contact Number<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="nominee_mobile" class="required span10">
														</div>
													</div>
													</td>
													</tr>
													
													<tr>
													<td>
													<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">Father&#39;s Name<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="Nominee_fathers_name" class="required span10">
														</div>
													</div>
													</td>
													<td>
													<div class="control-group" style="width:593px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">Mother&#39;s Name<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="nominee_mothers_name" class="required span8">
														</div>
													</div>
													</td>
													</tr>
													<tr>
													<td>
													<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">ID type<span class="required">*</span></label>
														<div class="controls">
														<select name="nominee_id_type" class="required span10">
																<option value="NID">NID </option>
																<option value="passport">Passport </option>
																<option value="driving_licence">Driving Licence </option>
															</select>
														</div>
													</div>
													</td>
													<td>
													<div class="control-group" style="width:593px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">ID Number<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="Nominee_id_number" class="required span8">
														</div>
													</div>
													</td>
													</tr>
													
													<tr>
													<td colspan="2">
													<div class="navbar-inner" style="background-image: linear-gradient(to bottom, rgba(0,0,0,.12), #f2f2f2);" >
													<div class="control-group" style="padding-bottom: 8px;padding-top: 9px;">
														<i class="icol-house" ></i><b style=" position: relative;left: 10px;">Permanent Address</b>
													</div>
													</div>
													</td>
													</tr>
													<tr>
													<td>
													<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">House Number<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="nominee_h_no" id="cs_permanent_address_h_no" class="required span10">
														</div>
													</div>
													</td>
													<td>
													<div class="control-group" style="width:593px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">Street<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="nominee_street" id="cs_permanent_address_street" class="required span8">
														</div>
													</div>
													</td>
													</tr>
													<tr>
													<td>
													<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">City<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="nominee_city" id="cs_permanent_address_city" class="required span10">
														</div>
													</div>
													</td>
													<td>
													<div class="control-group" style="width:593px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">State<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="nominee_state" id="cs_permanent_address_state" class="required span8">
														</div>
													</div>
													</td>
													</tr>
													<tr>
													<td>
													<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">District<span class="required">*</span></label>
														<div class="controls">
															<select name="nominee_district" class="required span10" id="nominee_district" onchange="changeNomiDistric()" >
																<option value="" disabled selected>SELECT DISTRICT</option>
															</select>
														</div>
													</div>
													</td>
													</tr>
													<tr>
													<td>
													<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">Than/Upazilla <span class="required">*</span></label>
														<div class="controls">
															<select name="nominee_thana" class="required span10" id="nominee_thana" onchange="changeNomineeThana()">
																<option value="" disabled selected>SELECT THAN / UPAZILLA</option>
															</select>
														</div>
													</div>
													</td>
													</tr>
													<tr>
													<td>
													<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
														<label class="control-label">Union/Ward<span class="required">*</span></label>
														<div class="controls">
															<select name="nominee_union" class="required span10" id="nominee_union" >
																<option value="" disabled selected>SELECT UNION / WARD</option>
															</select>
														</div>
													</div>
													</td>
													</tr>
													</table>
												</fieldset>
												
												<fieldset class="wizard-step">
													<legend class="wizard-label"><i class="icol-accept" style="position: relative; right: 4px;"></i>AUTHORIZATION</legend>
													
													<table>
													<tr>
														<td>
														<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
	                                                        <label class="control-label">NID Image<span class="required">*</span></label>
	                                                        <div class="controls">
	                                                        	<input type="file" id="photo1"  name="photo1"  class="required span10" >
	                                                        <div id="photoLabel"></div> 
	                                                        </div>
	                                                	</div>
														</td>
													</tr>
													
													<tr>
														<td>
														<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
	                                                        <label class="control-label">User Image<span class="required">*</span></label>
	                                                        <div class="controls">
	                                                        	<input type="file" id="photo2"  name="photo2"  class="required span10" >
	                                                        <div id="photoLabel"></div> 
	                                                        </div>
	                                               		 </div>
														</td>
													</tr>
													
													<tr>
														<td>
														 <div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
	                                                        <label class="control-label">KYC<span class="required">*</span></label>
	                                                        <div class="controls">
	                                                        	<input type="file" id="photo3"  name="photo3"  class="required span10" >
	                                                        <div id="photoLabel"></div> 
	                                                        </div>
	                                               		 </div>
														</td>
													</tr>
													
													<tr>
														<td>
														<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
	                                                        <label class="control-label">Info Updated KYC <span class="required">*</span></label>
	                                                        <div class="controls">
	                                                        	<input type="file" id="photo4"  name="photo4"  class="required span10">
	                                                        <div id="photoLabel"></div> 
	                                                        </div>
	                                               		 </div>
														
														</td>
													</tr>
													
													<tr>
														<td>
														<div class="control-group" style="width:518px;padding-bottom: 8px;padding-top: 9px;">
	                                                        <label class="control-label">Other<span class="required"></span></label>
	                                                        <div class="controls">
	                                                        	<input type="file" id="photo5"  name="photo5"  class="span10" >
	                                                        <div id="photoLabel"></div> 
	                                                        </div>
	                                                	</div>
														</td>
													</tr>
													</table>
												</fieldset>
											</form>
										</div>
									
									</div>
								
								</div>
								
								
					<%if(errCode.equalsIgnoreCase("crr")){ %>
									        
						<script type="text/javascript">
								window.onload=function(){
										alertOnload("User account  created successfully", "CRS", "OK");
									};
						</script>
					<%}if(errCode.equalsIgnoreCase("err")){%>
						<script type="text/javascript">
							window.onload=function(){alertOnload("Error on User creation.", "CRS", "OK");
							};	
						</script>
					<% } if(errCode.equalsIgnoreCase("err1")){%>
						<script type="text/javascript">
							window.onload=function(){alertOnload("User account  already created", "CRS", "OK");
							};	
						</script>
					<% } %>
                                              
                                    <script type="text/javascript">
                                            function alertOnload(message, title, buttonText) {
									 buttonText = (buttonText == undefined) ? "Ok" : buttonText;
								    title = (title == undefined) ? "The page says:" : title;
								
								    var div = $('<div>');
								    div.html(message);
								    div.attr('title', "CRS");
								    div.dialog({
								        autoOpen: true,
								        modal: true,
								        draggable: false,
								        resizable: false,
								        buttons: [{
								            text: buttonText,
								            click: function () {
								                $(this).dialog("close");
								                div.remove();
								              
								            }
								        }]
								    });
								   
								}
                                            </script>
								

                                
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        
        <footer id="footer">
            <div class="footer-left">Mobile Banking Platform</div>
            <div class="footer-right"><p>Copyright 2016 - Evidu Private Limited All Rights Reserved.</p></div>
        </footer>
        
    </div>

	<!-- Core Scripts -->
	<script src="assets/js/libs/jquery-1.8.3.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/libs/jquery.placeholder.min.js"></script>
	<script src="assets/js/libs/jquery.mousewheel.min.js"></script>

    <!-- Template Script -->
    <script src="assets/js/template.js"></script>
    <script src="assets/js/setup.js"></script>

    <!-- Customizer, remove if not needed -->
    <script src="assets/js/customizer.js"></script>

    <!-- Uniform Script -->
    <script src="plugins/uniform/jquery.uniform.min.js"></script>
    
    <!-- jquery-ui Scripts -->
    <script src="assets/jui/js/jquery-ui-1.9.2.min.js"></script>
    <script src="assets/jui/jquery-ui.custom.min.js"></script>
    <script src="assets/jui/timepicker/jquery-ui-timepicker.min.js"></script>
	<script src="assets/jui/jquery.ui.touch-punch.min.js"></script>
    
    <!-- Plugin Scripts -->
    
	<!-- Validation -->
	<script src="plugins/validate/jquery.validate.min.js"></script>
	
	<!-- newly added js -->
	<script src="js/selection_js.js"></script>
	
	<!-- Wizard -->
	<script src="custom-plugins/wizard/wizard.min.js"></script>
    <script src="custom-plugins/wizard/jquery.form.min.js"></script>

    <!-- Demo Scripts -->
    <script src="assets/js/demo/form_wizard.js"></script>
	
	<!-- newly added js -->
	<script src="js/selection_js.js"></script>
	
	
	<script src="plugins/flot/jquery.flot.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.tooltip.min.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.pie.min.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.orderBars.min.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.resize.min.js"></script>

	
	<script src="assets/js/demo/ui_comps.js"></script>
	<!--  --------------------------------------------------------->



</body>

</html>
