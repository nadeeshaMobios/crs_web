<%@page import="com.evidu.bank.sva.MainController"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.evidu.bank.sva.*"%>
<%@ page import="com.evidu.bank.entitiy.*"%>
<%@page import="java.util.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.apache.commons.codec.binary.Base64"%>

<%
	MainController mc = new MainController();
	String action = "" + request.getParameter("action");
	System.out.println("action"+action);
	String query;
	
	if(action.equals("getDistrictDetail")){
		
		List<String> distric = new ArrayList<String>();
		List<String> querylist = new ArrayList<String>();
		
		querylist.add("SELECT dist_name FROM district");
		
		distric = mc.getDistric(querylist);
		JSONObject obj = new JSONObject();	
		JSONArray list = new JSONArray();
		if(distric.size()>0){
			for(String dis :distric){
			list.add(dis);
			}
		}
	
		obj.put("distric", list);
		//System.out.print(">>>>>>>>>>>>>>>getDistrictDetail>>>>>>>>>>>>>>>>"+obj);
		out.print(obj);    
		
	}
	
	if(action.equals("getThanaDetail")){
		
		String thanaList = ""+request.getParameter("thanaVal");
		String districVal = ""+request.getParameter("districVal");
		
		List<String> thana = new ArrayList<String>();
		List<String> querylist = new ArrayList<String>();
		
		querylist.add("SELECT `thana`.`thana_name`FROM `thana`INNER JOIN `district`ON thana.`dist_id`=district.`dist_id`WHERE `district`.`dist_name` LIKE '%"+districVal+"%'");
		
		thana = mc.getThana(querylist);
		JSONObject obj = new JSONObject();	
		JSONArray list = new JSONArray();
		if(thana.size()>0){
			for(String dis :thana){
			list.add(dis);
			}
		}
	
		obj.put("thana", list);
		//System.out.print(">>>>>>>>>>>>>>>getThanaDetail>>>>>>>>>>>>>>>>"+obj);
		out.print(obj); 
		
	}
	
	if(action.equals("getUnion")){
		
		String thanaVal = ""+request.getParameter("thanaVal");
		System.out.println("thanaVal : "+thanaVal);
		
		List<String> union = new ArrayList<String>();
		List<String> querylist = new ArrayList<String>();
		
		querylist.add("SELECT `bangala_union`.`union_name` FROM `bangala_union` INNER JOIN `thana` ON `bangala_union`.`thana_id`= `thana`.`thana_id` WHERE `thana`.`thana_name` LIKE '%"+thanaVal+"%'");
		
		union = mc.getUnion(querylist);
		JSONObject obj = new JSONObject();	
		JSONArray list = new JSONArray();
		if(union.size()>0){
			for(String dis :union){
			list.add(dis);
			}
		}
	
		obj.put("union", list);
		System.out.println(">>>>>>>>>>>>>>>getUnion>>>>>>>>>>>>>>>>"+obj);
		out.print(obj);
		
	}
	
	if(action.equals("getPersonDistrictDetail")){
			
			List<String> personaldistric = new ArrayList<String>();
			List<String> querylist = new ArrayList<String>();
			
			querylist.add("SELECT dist_name FROM district");
			
			personaldistric = mc.getDistric(querylist);
			JSONObject obj = new JSONObject();	
			JSONArray list = new JSONArray();
			if(personaldistric.size()>0){
				for(String dis :personaldistric){
				list.add(dis);
				}
			}
		
			obj.put("personaldistric", list);
			//System.out.print(">>>>>>>>>>>>>>>getPersonDistrictDetail>>>>>>>>>>>>>>>>"+obj);
			out.print(obj);    
			
		}
	
	if(action.equals("getPersonalThanaDetail")){
		
		String personDistricVal = ""+request.getParameter("personDistricVal");
		
		List<String> person_thana = new ArrayList<String>();
		List<String> querylist = new ArrayList<String>();
		
		querylist.add("SELECT `thana`.`thana_name`FROM `thana`INNER JOIN `district`ON thana.`dist_id`=district.`dist_id`WHERE `district`.`dist_name` LIKE '%"+personDistricVal+"%'");
		
		person_thana = mc.getThana(querylist);
		JSONObject obj = new JSONObject();	
		JSONArray list = new JSONArray();
		if(person_thana.size()>0){
			for(String dis :person_thana){
			list.add(dis);
			}
		}
	
		obj.put("person_thana", list);
		//System.out.print(">>>>>>>>>>>>>>>getPersonalThanaDetail>>>>>>>>>>>>>>>>"+obj);
		out.print(obj); 
	}
	
	if(action.equals("getPersonUnion")){
		
		String personalThanaVal = ""+request.getParameter("personalThanaVal");
		
		List<String> personUnion = new ArrayList<String>();
		List<String> querylist = new ArrayList<String>();
		
		querylist.add("SELECT `bangala_union`.`union_name` FROM `bangala_union` INNER JOIN `thana` ON `bangala_union`.`thana_id`= `thana`.`thana_id` WHERE `thana`.`thana_name` LIKE '%"+personalThanaVal+"%'");
		
		personUnion = mc.getUnion(querylist);
		JSONObject obj = new JSONObject();	
		JSONArray list = new JSONArray();
		if(personUnion.size()>0){
			for(String dis :personUnion){
			list.add(dis);
			}
		}
	
		obj.put("person_union", list);
		System.out.println(">>>>>>>>>>>>>>>getPersonUnion>>>>>>>>>>>>>>>>"+obj);
		out.print(obj);
		
	}
	
	
	if(action.equals("getEmergyDistrictDetail")){
		
		List<String> emergyDistric = new ArrayList<String>();
		List<String> querylist = new ArrayList<String>();
		
		querylist.add("SELECT dist_name FROM district");
		
		emergyDistric = mc.getDistric(querylist);
		JSONObject obj = new JSONObject();	
		JSONArray list = new JSONArray();
		if(emergyDistric.size()>0){
			for(String dis :emergyDistric){
			list.add(dis);
			}
		}
	
		obj.put("emergyDistric", list);
		//System.out.print(">>>>>>>>>>>>>>>getEmergyDistrictDetail>>>>>>>>>>>>>>>>"+obj);
		out.print(obj);    
		
	}
		
	if(action.equals("getEmergylThanaDetail")){
		
		String emergyDistricVal = ""+request.getParameter("emergyDistricVal");
		
		List<String> emergy_thana = new ArrayList<String>();
		List<String> querylist = new ArrayList<String>();
		
		querylist.add("SELECT `thana`.`thana_name`FROM `thana`INNER JOIN `district`ON thana.`dist_id`=district.`dist_id`WHERE `district`.`dist_name` LIKE '%"+emergyDistricVal+"%'");
		
		emergy_thana = mc.getThana(querylist);
		JSONObject obj = new JSONObject();	
		JSONArray list = new JSONArray();
		if(emergy_thana.size()>0){
			for(String dis :emergy_thana){
			list.add(dis);
			}
		}
	
		obj.put("emergy_thana", list);
		//System.out.print(">>>>>>>>>>>>>>>getEmergylThanaDetail>>>>>>>>>>>>>>>>"+obj);
		out.print(obj); 
		
	}
	
	if(action.equals("getEmergyUnion")){
		
		String changeEmergyThana = ""+request.getParameter("emergyThanaVal");
		
		List<String> emergyUnion = new ArrayList<String>();
		List<String> querylist = new ArrayList<String>();
		
		querylist.add("SELECT `bangala_union`.`union_name` FROM `bangala_union` INNER JOIN `thana` ON `bangala_union`.`thana_id`= `thana`.`thana_id` WHERE `thana`.`thana_name` LIKE '%"+changeEmergyThana+"%'");
		
		emergyUnion = mc.getUnion(querylist);
		JSONObject obj = new JSONObject();	
		JSONArray list = new JSONArray();
		if(emergyUnion.size()>0){
			for(String dis :emergyUnion){
			list.add(dis);
			}
		}
	
		obj.put("emergyUnion", list);
		System.out.println(">>>>>>>>>>>>>>>getEmergyUnion>>>>>>>>>>>>>>>>"+obj);
		out.print(obj);
	}
	
	if(action.equals("getNomineeDistrictDetail")){
		
		List<String> nomineeDistric = new ArrayList<String>();
		List<String> querylist = new ArrayList<String>();
		
		querylist.add("SELECT dist_name FROM district");
		
		nomineeDistric = mc.getDistric(querylist);
		JSONObject obj = new JSONObject();	
		JSONArray list = new JSONArray();
		if(nomineeDistric.size()>0){
			for(String dis :nomineeDistric){
			list.add(dis);
			}
		}
	
		obj.put("nomineeDistric", list);
		//System.out.print(">>>>>>>>>>>>>>>getEmergyDistrictDetail>>>>>>>>>>>>>>>>"+obj);
		out.print(obj);  
		
	}
	
	if(action.equals("getNomineeThanaDetail")){
		
		String changeDistricVal = ""+request.getParameter("nomineeDistricVal");

		List<String> nominee_thana = new ArrayList<String>();
		List<String> querylist = new ArrayList<String>();
		
		querylist.add("SELECT `thana`.`thana_name`FROM `thana`INNER JOIN `district`ON thana.`dist_id`=district.`dist_id`WHERE `district`.`dist_name` LIKE '%"+changeDistricVal+"%'");
		
		nominee_thana = mc.getThana(querylist);
		JSONObject obj = new JSONObject();	
		JSONArray list = new JSONArray();
		if(nominee_thana.size()>0){
			for(String dis :nominee_thana){
			list.add(dis);
			}
		}
	
		obj.put("nominee_thana", list);
		//System.out.print(">>>>>>>>>>>>>>>getNomineeThanaDetail>>>>>>>>>>>>>>>>"+obj);
		out.print(obj); 
	}
	
	if(action.equals("getNomineeUnion")){
		
		String nomiThanVal = ""+request.getParameter("nomineeThanaVal");
		
		List<String> nomineeUnion = new ArrayList<String>();
		List<String> querylist = new ArrayList<String>();
		
		querylist.add("SELECT `bangala_union`.`union_name` FROM `bangala_union` INNER JOIN `thana` ON `bangala_union`.`thana_id`= `thana`.`thana_id` WHERE `thana`.`thana_name` LIKE '%"+nomiThanVal+"%'");
		
		nomineeUnion = mc.getUnion(querylist);
		JSONObject obj = new JSONObject();	
		JSONArray list = new JSONArray();
		if(nomineeUnion.size()>0){
			for(String dis :nomineeUnion){
			list.add(dis);
			}
		}
	
		obj.put("nomineeUnion", list);
		System.out.println(">>>>>>>>>>>>>>>getNomineeUnion>>>>>>>>>>>>>>>>"+obj);
		out.print(obj);
		
	}
	
	if(action.equals("geRegionalDetail")){
		
		List<String> region = new ArrayList<String>();
		List<String> querylist = new ArrayList<String>();
		
		querylist.add("SELECT reg_name FROM regional_bang");
		
		region = mc.getRegion(querylist);
		JSONObject obj = new JSONObject();	
		JSONArray list = new JSONArray();
		if(region.size()>0){
			for(String reg :region){
			list.add(reg);
			}
		}
	
		obj.put("region", list);
		System.out.print(">>>>>>>>>>>>>>>geRegionalDetail>>>>>>>>>>>>>>>>"+obj);
		out.print(obj);    
		
	}
	
	if(action.equals("getAreaDetail")){
		
		String regVal = ""+request.getParameter("regVal");
		
		List<String> area = new ArrayList<String>();
		List<String> querylist = new ArrayList<String>();
		
		querylist.add("SELECT `area_bang`.`area_name` FROM `area_bang` INNER JOIN `regional_bang` ON `regional_bang`.`reg_id`=`area_bang`.`reg_id` WHERE `regional_bang`.`reg_name` LIKE '%"+regVal+"%'");
		
		area = mc.getArea(querylist);
		JSONObject obj = new JSONObject();	
		JSONArray list = new JSONArray();
		if(area.size()>0){
			for(String area1 :area){
			list.add(area1);
			}
		}
	
		obj.put("area", list);
		//System.out.print(">>>>>>>>>>>>>>>getAreaDetail>>>>>>>>>>>>>>>>"+obj);
		out.print(obj); 
		
	}
	
if(action.equals("getTerritory")){
		
		String areaVal = ""+request.getParameter("areaVal");
		
		List<String> territory = new ArrayList<String>();
		List<String> querylist = new ArrayList<String>();
		
		querylist.add("SELECT `territory_bang`.`terri_name` FROM `territory_bang` INNER JOIN `area_bang` ON `area_bang`.`area_id`=`territory_bang`.`area_id` WHERE `area_bang`.`area_name` LIKE '%"+areaVal+"%'");
		
		territory = mc.getTerritory(querylist);
		JSONObject obj = new JSONObject();	
		JSONArray list = new JSONArray();
		if(territory.size()>0){
			for(String territory_bang :territory){
			list.add(territory_bang);
			}
		}
	
		obj.put("territory", list);
		System.out.println(">>>>>>>>>>>>>>>getTerritory>>>>>>>>>>>>>>>>"+obj);
		out.print(obj);
		
	}
	
	

%>