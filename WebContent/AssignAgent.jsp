<%@page import="com.evidu.bank.entitiy.*"%>
<%@page import="com.evidu.bank.sva.*"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>

<%
response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
//response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
int timeout = session.getMaxInactiveInterval();
response.setHeader("Refresh", timeout + "; URL = index.jsp");
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility

%>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->

<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Bootstrap Stylesheet -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" media="all">

<!-- jquery-ui Stylesheets -->
<link rel="stylesheet" href="assets/jui/css/jquery-ui.css" media="screen">
<link rel="stylesheet" href="assets/jui/jquery-ui.custom.css" media="screen">
<link rel="stylesheet" href="assets/jui/timepicker/jquery-ui-timepicker.css" media="screen">

<!-- Uniform Stylesheet -->
<link rel="stylesheet" href="plugins/uniform/css/uniform.default.css" media="screen">

<!-- Plugin Stylsheets first to ease overrides -->

<!-- Wizard Stylesheet -->
<link rel="stylesheet" href="custom-plugins/wizard/wizard.css">

<!-- End Plugin Stylesheets -->

<!-- Main Layout Stylesheet -->
<link rel="stylesheet" href="assets/css/fonts/icomoon/style.css" media="screen">
<link rel="stylesheet" href="assets/css/main-style.css" media="screen">


<!-- Rating Plugin -->
<link rel="stylesheet" href="plugins/rating/jquery.rating.css" media="screen">

<!-- Plugin Stylsheets first to ease overrides -->

<!-- iButton -->
<link rel="stylesheet" href="plugins/ibutton/jquery.ibutton.css" media="screen">

<!-- PickList -->
<link rel="stylesheet" href="custom-plugins/picklist/picklist.css" media="screen">

<!-- Select2 -->
<link rel="stylesheet" href="plugins/select2/select2.css" media="screen">


<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<link href="css/elements.css" rel="stylesheet">

<title>Tap'n Pay</title>
<meta http-equiv="imagetoolbar" content="no" />
</head>

<body data-show-sidebar-toggle-button="false" data-fixed-sidebar="false"  oncontextmenu="return false">
											
  <%
 //
  UserInfo userData =new UserInfo();
		userData= (UserInfo)session.getAttribute("current_user");
		
		if(userData==null){
			response.sendRedirect("index.jsp");
			return;
		}
		String errCode=""+request.getParameter("st");
 
    %> 

    <div id="wrapper">
        <header id="header" class="navbar navbar-inverse">
            <div class="navbar-inner">
                <div class="container">
					<div class="brand-wrap pull-left">
						<div class="brand-img">
							<a class="brand" href="#">
								<img src="assets/images/evidu_logo.png" alt="" style="width: 144px;position: relative;left: -21px;" >
							</a>
						</div>
					</div>
                    
                    <div id="header-right" class="clearfix">
						<div id="nav-toggle" data-toggle="collapse" data-target="#navigation" class="collapsed">
							<i class="icon-caret-down"></i>
						</div>
						
                        
                        <div id="header-functions" class="pull-right">
                        	<div id="user-info" class="clearfix">
                                <span class="info">
                                	Welcome
                                    <span class="name"><%=userData.getDisplay_name() %></span>
                                </span>
                            	<div class="avatar">
                                	<a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    	<img src="assets/images/defaltUser.png" alt="Avatar">
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                    	<li><a href="profile.html"><i class="icol-user"></i> My Profile</a></li>
                                    	<li><a href="#"><i class="icol-layout"></i> My Invoices</a></li>                                        
                                        <li class="divider"></li>
                                        <li><a href="index.jsp"><i class="icol-key"></i> Logout</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="logout-ribbon">
                            	<a href="index.jsp"><i class="icon-off"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        
        <div id="content-wrap">
        	<div id="content" class="sidebar-minimized">
            	<div id="content-outer">
                	<div id="content-inner">
                    	<aside id="sidebar">
                        	<nav id="navigation" class="collapse">
                            	<ul>
                                	<li><a href="DEORefSearch.jsp" >
                                    	<span title="Home">
                                    		<i class="icon-home"></i>
											<span class="nav-title">General</span>
                                        </span>
                                    	</a>
                                    </li>
                                	<li ><a href="DEONewEntry.jsp" >
                                    	<span title="New Entry">
                                    		<i class="icon-plus-sign"></i>
											<span class="nav-title">New Entry</span>
                                        </span>
                                    	</a>
                                    </li>
                                	
                                	<li ><a href="AddDSR.jsp" >
                                    	<span title="Add DSR">
                                    		<i class="icon-table"></i>
											<span class="nav-title">Add DSR</span>
                                        </span>
                                    	</a>
                                    </li>
                                	<li><a href="AddAgent.jsp" >
                                    	<span title="Add  Agent">
                                    		<i class="icon-table"></i>
											<span class="nav-title">Add Agent</span>
                                        </span>
                                    	</a>
                                    </li>
                                    <li  class="active"><a href="AssignAgent.jsp" >
                                    	<span title="Assign Agent">
                                    		<i class="icon-table"></i>
											<span class="nav-title">Assign Agent</span>
                                        </span>
                                    	</a>
                                    </li>
                                    <li><a href="DEOReports.jsp" >
                                    	<span title="Reports">
                                    		<i class="icon-table"></i>
											<span class="nav-title">Reports</span>
                                        </span>
                                    	</a>
                                    </li>
                                </ul>
                            </nav>
                        </aside>

                        <div id="sidebar-separator"></div>
                        
                        <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-home"></i>Tap&#39;n Pay
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="DEORefSearch.jsp">Home</a>
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    
                                </ul>
                                
                                <h1 id="main-heading">
                                	Assign Agent
                                </h1>
                            </div>
                            
                            <div id="main-content">
                                	
										<div class="row-fluid">
                                	<div class="span12">
                                	<div class="widget">
                                            <div class="widget-header">
                                                <span class="title">General Form Elements</span>
                                            </div>
                                            <div class="widget-content form-container">
                                                <form class="form-horizontal" action="AgentController.jsp" method="post">
                                                    
                                                    <div class="control-group">
                                                        <label class="control-label" for="input01">Select DSR</label>
                                                        <div class="controls">
                                                            <select id="dsrList" onchange="getAgentList(this.value);" name="dsrList" class="span12">
                                                            	<option></option>
                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                  <div id="secondDropDiv_nad">
                                                 
                                                  	
                                                    <div class="control-group">
                                                        <label class="control-label" for="picklist-ex">Agent List</label>
                                                        <div class="controls">
                                                            <select id="picklist-ex" name="agentList">
                                                           <option value="988888"> 988888 Nadee </option> 
    															 <%

    															ArrayList<Agent> agentList = new ArrayList<Agent>(); 
    														    agentList= (ArrayList<Agent>)session.getAttribute("agentList");
    														    	
    															if(agentList!=null){
    																System.out.println("234567890---090-90");
    															for(int i=0;i<agentList.size();i++){ %>
<<<<<<< HEAD
    															<%-- <option value="<%=agentList.get(i).getMobileNumber() %>"><%=agentList.get(i).getMobileNumber() %> <%=agentList.get(i).getName() %></option> --%>
    															<%
=======
    															<%-- <option value="<%=agentList.get(i).getMobileNumber() %>"><%=agentList.get(i).getMobileNumber() %> <%=agentList.get(i).getName() %></option>
    															 --%><%
>>>>>>> 73f2c9e3b59fc3cc5d4dff1bcb0a739911160ea2
    															}
    															}
    															 session.removeAttribute("agentList");%>
    															
    														</select>
    														<p class="help-block">Use shift and control keys to select multiple items</p>
                                                        </div>
                                                    </div>
                                                   
                                                 </div>
                                                 <input type="hidden" value="assignAgent" name="action" id="action">
                                                    <div class="form-actions">
                                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                                        <button type="reset" class="btn" type="reset">Cancel</button>
                                                    </div>
                                                </form>
                                            </div>
        	                            </div>
                                	</div>
                                	</div>
								
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        
        <footer id="footer">
            <div class="footer-left">Mobile Banking Platform</div>
            <div class="footer-right"><p>Copyright 2016 - Evidu Private Limited All Rights Reserved.</p></div>
        </footer>
        
    </div>

	<!-- Core Scripts -->
	<script src="assets/js/libs/jquery-1.8.3.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/libs/jquery.placeholder.min.js"></script>
	<script src="assets/js/libs/jquery.mousewheel.min.js"></script>

    <!-- Template Script -->
    <script src="assets/js/template.js"></script>
    <script src="assets/js/setup.js"></script>

    <!-- Customizer, remove if not needed -->
    <script src="assets/js/customizer.js"></script>

    <!-- Uniform Script -->
    <script src="plugins/uniform/jquery.uniform.min.js"></script>
    
    <!-- jquery-ui Scripts -->
    <script src="assets/jui/js/jquery-ui-1.9.2.min.js"></script>
    <script src="assets/jui/jquery-ui.custom.min.js"></script>
    <script src="assets/jui/timepicker/jquery-ui-timepicker.min.js"></script>
	<script src="assets/jui/jquery.ui.touch-punch.min.js"></script>
    
    <!-- Plugin Scripts -->
    
	<!-- Validation -->
	<script src="plugins/validate/jquery.validate.min.js"></script>
	
	<!-- Wizard -->
	<script src="custom-plugins/wizard/wizard.min.js"></script>
    <script src="custom-plugins/wizard/jquery.form.min.js"></script>

   
    <!-- Demo Scripts -->
    <script src="assets/js/demo/form_wizard.js"></script>
	
	<!-- newly added js -->
	
	<script src="plugins/flot/jquery.flot.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.tooltip.min.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.pie.min.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.orderBars.min.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.resize.min.js"></script>

	 <!-- Bootstrap FileInput -->
    <script src="custom-plugins/bootstrap-fileinput.min.js"></script>

    <!-- Bootstrap InputMask -->
    <script src="custom-plugins/bootstrap-inputmask.min.js"></script>

    <!-- iButton -->
    <script src="plugins/ibutton/jquery.ibutton.min.js"></script>

    <!-- AutoSize -->
    <script src="plugins/autosize/jquery.autosize-min.js"></script>
	
	<!-- PickList -->
	<script src="custom-plugins/picklist/picklist.min.js"></script>
    
	<!-- Select2 -->
	<script src="plugins/select2/select2.min.js"></script>

    <!-- Rating  -->
    <script src="plugins/rating/jquery.rating.min.js"></script>
    
    <!-- Demo Scripts -->
    <script src="assets/js/demo/form.js"></script>
     <script src="js/dealer_portal.js"></script>



</body>

</html>
