<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.evidu.bank.sva.*"%>
<%@page import="com.evidu.bank.entitiy.*"%>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->

<head >

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Bootstrap Stylesheet -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" media="all">

<!-- jquery-ui Stylesheets -->
<link rel="stylesheet" href="assets/jui/css/jquery-ui.css" media="screen">
<link rel="stylesheet" href="assets/jui/jquery-ui.custom.css" media="screen">
<link rel="stylesheet" href="assets/jui/timepicker/jquery-ui-timepicker.css" media="screen">

<!-- Uniform Stylesheet -->
<link rel="stylesheet" href="plugins/uniform/css/uniform.default.css" media="screen">

<!-- Plugin Stylsheets first to ease overrides -->

<!-- iButton -->
<link rel="stylesheet" href="plugins/ibutton/jquery.ibutton.css" media="screen">

<!-- Circular Stat -->
<link rel="stylesheet" href="custom-plugins/circular-stat/circular-stat.css">

<!-- Fullcalendar -->
<link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.css" media="screen">
<link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.print.css" media="print">

<!-- End Plugin Stylesheets -->

<!-- Main Layout Stylesheet -->
<link rel="stylesheet" href="assets/css/fonts/icomoon/style.css" media="screen">
<link rel="stylesheet" href="assets/css/main-style.css" media="screen">

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<title>Tap'nPay</title> 
<script type="text/javascript">


</script>

</head>

<body data-show-sidebar-toggle-button="false" data-fixed-sidebar="false">
 <%
  UserInfo userData =new UserInfo();
		userData= (UserInfo)session.getAttribute("current_user");
		if(userData==null){
			response.sendRedirect("index.jsp");
			return;
		}
		String errCode=""+request.getParameter("st");
		
		
		//System.out.println("**///**"+pendingStatusList.size());
		 
		//System.out.println("** pendingStatusList **"+pendingStatusList.size());
  %>

    

    <div id="wrapper">
        <header id="header" class="navbar navbar-inverse">
            <div class="navbar-inner">
                <div class="container">
					<div class="brand-wrap pull-left">
						<div class="brand-img">
							<a class="brand" href="#">
								<img border="0" src="assets/images/mobios_logo.png"
								alt="mobios logo " width="150" height="50">  <img
								border="0" 
								 height="50"> 
							</a>
						</div>
					</div>
                    
                    <div id="header-right" class="clearfix">
						<div id="nav-toggle" data-toggle="collapse" data-target="#navigation" class="collapsed">
							<i class="icon-caret-down"></i>
						</div>
                        <div id="header-search">
							<span id="search-toggle" data-toggle="dropdown">
								<i class="icon-search"></i>
							</span>
							<form class="navbar-search">
								<input type="text" class="search-query" placeholder="Search">
							</form>
						</div>
						<div id="dropdown-lists">
                            <div class="item-wrap">
                                <a class="item" href="#" data-toggle="dropdown">
                                    <span class="item-icon"><i class="icon-exclamation-sign"></i></span>
                                    <span class="item-label">Notifications</span>
                                    <span class="item-count">4</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-item-wrap">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>John Doe</strong> commented on your photo
                                                        <span class="time">13 minutes ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>Jane Roe</strong> commented on your photo
                                                        <span class="time">27 minutes ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>Billy John</strong> commented on your photo
                                                        <span class="time">43 minutes ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">View all notifications</a></li>
                                </ul>
                            </div>
                            <div class="item-wrap">
                                <a class="item" href="#" data-toggle="dropdown">
                                    <span class="item-icon"><i class="icon-envelope"></i></span>
                                    <span class="item-label">Messages</span>
                                    <span class="item-count">16</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-item-wrap">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>John Doe</strong><br> Hello, do you have time to go out tomorrow?
                                                        <span class="time">13 minutes ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>Jane Roe</strong><br> Hey, the reports said that you were...
                                                        <span class="time">27 minutes ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>Billy John</strong><br> Can I borrow your new camera for taking...
                                                        <span class="time">About an hour ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="mail.html">View all messages</a></li>
                                </ul>
                            </div>
                        </div>
                        <div id="header-functions" class="pull-right">
                        	<div id="user-info" class="clearfix">
                                	<span class="info">
                                	Welcome
                                    <span class="name"><%=userData.getDisplay_name() %></span>
                                </span>
                               
                            	<div class="avatar">
                                	<a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    	<img src="assets/images/defaltUser.png" alt="Avatar">
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                    	
                                        <li><a href="index.jsp"><i class="icol-key"></i> Logout</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="logout-ribbon">
                            	<a href="index.jsp"><i class="icon-off"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        
        <div id="content-wrap">
        	<div id="content" class="sidebar-minimized">
            	<div id="content-outer">
                	<div id="content-inner">
                    	<aside id="sidebar">
                        	<nav id="navigation" class="collapse">
                            	<ul>
                            	<li class="active"><a href="DealerDashboard.jsp" >
                                    	<span title="Home">
                                    		<i class="icon-home"></i>
											<span class="nav-title">Dashboard</span>
                                        </span>
                                    	</a>
                                    </li>
                                	<li class=><a href="DEORefSearch.jsp" >
                                    	<span title="Home">
                                    		<i class="icon-business-card"></i>
											<span class="nav-title">Temporary Accounts</span>
                                        </span>
                                    	</a>
                                    </li>
                                	<li><a href="DEORefSearch.jsp" >
                                    	<span title="Home">
                                    		<i class="icon-plus-sign"></i>
											<span class="nav-title">Customer Registration</span>
                                        </span>
                                    	</a>
                                    </li>
                                	
                                   <li><a href="AddAgent.jsp" >
                                    	<span title="Add  Agent">
                                    		<i class="icon-user"></i>
											<span class="nav-title">Agent Registration</span>
                                        </span>
                                    	</a>
                                    </li>
                                </ul>
                            </nav>
                        </aside>

                        <div id="sidebar-separator"></div>
                        
                        <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icol-application-home"></i>Tap'nPay
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<i class="icol-dashboard"></i>Dashboard
                                        <span class="divider">&raquo;</span>
                                    </li>
                                </ul>
                                
                                <h1 id="main-heading">
                                	Dashboard 
                                </h1>
                            </div>
                            
                            <div id="main-content" >
                            	
                            	
                                
                                <div class="row-fluid">
                                	<div class="span12 widget">
                                        <div class="widget-header">
                                            <span class="title"><i class="icon-resize-horizontal"></i> User Details - Summary</span>
                                        </div>
                                        <div class="widget-content form-container">
                                           
                                                <div class="navbar-inner" >
                                               
                                                <div align="center" class="control-group">
                                                <table ><tr><td><label class="control-label" for="input02">Select User Type</label></td>
                                                <td><div class="controls">
                                                        <select id="input02" class="span5" style="width: 150%" onchange="getCountsFromDB(this.value);">
                                                            <option>Customer</option>
                                                            <option>Agent</option>
                                                            <option>DSR</option>
                                                            
                                                        </select>
                                                    </div></td><tr></table>
                                                    
                                                    
                                                </div>
                                               
                                                </div>
                                      <ul class="stats-container" style="padding-top: 40px">
                                   
                                    <li>
                                        <a  class="stat summary">
                                            <span class="icon icon-circle bg-red">
                                                <i class="icon-fire"></i>
                                            </span>
                                            <span  class="digit"  id='rejected'>
                                                <span class="text">Rejected </span>
                                                5
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a  class="stat summary">
                                            <span class="icon icon-circle bg-orange">
                                                <i class="icon-database"></i>
                                            </span>
                                            <span class="digit" id='pending'>
                                                <span class="text">Pending </span>
                                                23
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a  class="stat summary">
                                            <span class="icon icon-circle bg-blue">
                                                <i class="icon-hdd"></i>
                                            </span>
                                            <span class="digit" id='approved' >
                                                <span class="text">Approved</span>
                                                140
                                            </span>
                                        </a>
                                    </li>
                                </ul> 
                                           
                                        </div>
    	                            </div>
                                </div>
                           	</div>
                           	
                           	 <!-- <h4 class='sub'><span>Campaign Results Search By Date</span>
                            </h4> -->
                                
                                 <div class="row-fluid" style="width: 91%;left: 67px; position: relative;">
                                    <div class="span6 widget">
                                        <div class="widget-header">
                                            <span class="title"><i class="icon-graph"></i>Bar Chart</span>
                                        </div>
                                        <div class="widget-content">
                                            <div id="demo-charts-01" style="height:300px; "></div>
                                        </div>
                                    </div>
                                	<div class="span6 widget">
                                            <div class="widget-header">
                                                <span class="title"><i class="icon-pie-chart"></i>Pie Chart</span>
                                            </div>
                                            <div class="widget-content">
                                                <div id="demo-charts-05" style="height:300px; "></div>
                                            </div>
                                        </div>
                                </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        
         <footer id="footer">
            <div class="footer-left">Mobile Banking Platform</div>
            <div class="footer-right"><p>Copyright 2016 - Evidu Private Limited All Rights Reserved.</p></div>
        </footer>
        
    </div>

    <!-- Core Scripts -->
    <script src="assets/js/libs/jquery-1.8.3.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/libs/jquery.placeholder.min.js"></script>
    <script src="assets/js/libs/jquery.mousewheel.min.js"></script>
    
    <!-- Template Script -->
    <script src="assets/js/template.js"></script>
    <script src="assets/js/setup.js"></script>

    <!-- Customizer, remove if not needed -->
    <script src="assets/js/customizer.js"></script>

    <!-- Uniform Script -->
    <script src="plugins/uniform/jquery.uniform.min.js"></script>
    
    <!-- jquery-ui Scripts -->
   <!--  <script src="assets/jui/js/jquery-ui-1.9.2.min.js"></script>  -->
     <script src="assets/jui/js/jquery-ui-1.9.2.min_edited.js"></script>
    <script src="assets/jui/jquery-ui.custom.min.js"></script>
    <script src="assets/jui/timepicker/jquery-ui-timepicker.min.js"></script>
    <script src="assets/jui/jquery.ui.touch-punch.min.js"></script>
    
    <!-- Plugin Scripts -->
    
    <!-- Flot -->
    <!--[if lt IE 9]>
    <script src="assets/js/libs/excanvas.min.js"></script>
    <![endif]-->
    <script src="plugins/flot/jquery.flot.min.js"></script>
    <script src="plugins/flot/plugins/jquery.flot.tooltip.min.js"></script>
    <script src="plugins/flot/plugins/jquery.flot.pie.min.js"></script>
    <script src="plugins/flot/plugins/jquery.flot.resize.min.js"></script>

    <!-- Circular Stat -->
    <script src="custom-plugins/circular-stat/circular-stat.min.js"></script>

    <!-- SparkLine -->
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
    
    <!-- iButton -->
    <script src="plugins/ibutton/jquery.ibutton.min.js"></script>

    <!-- Full Calendar -->
    <script src="plugins/fullcalendar/fullcalendar.min.js"></script>
    
    <!-- dashboard custom js -->
    <script src="js/dealer_portal.js"></script>
    <script src="js/dashboardReports.js"></script>
    <script src="assets/js/demo
    /charts_dealer.js"></script>
   
    
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/TableTools/js/TableTools.min.js"></script>
    <script src="plugins/datatables/FixedColumns/FixedColumns.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.js"></script>
    <script src="plugins/datatables/jquery.dataTables.columnFilter.js"></script>
    
    <!-- Demo Scripts -->
    <script src="assets/js/demo/dashboard.js"></script>
    <script src="assets/js/demo/dataTables.js"></script>
    
    

</body>

</html>
