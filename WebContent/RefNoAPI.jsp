
<%@page import="com.evidu.bank.gson.*"%>
<%@page import="com.evidu.bank.sva.*"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.google.gson.GsonBuilder"%>
<%@page import="com.google.gson.JsonSyntaxException"%>

<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStream"%>
 <%@ page import="java.util.UUID"%>
 <%@ page import="com.mobios.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%
String ID = ""+UUID.randomUUID().toString();
String platformID="ETI-IVR";
String serverIP=""+request.getLocalAddr();
String ip =""+request.getRemoteAddr();
String softVersion="V1.2";
String logData=""+serverIP+","+ip+","+softVersion+","+platformID+","+ID+",";
Gson gson = new Gson();

String reqMethod = request.getMethod();
String requestContent = null;
if(reqMethod.equals("POST")){
	
	requestContent = readStringContent(request);
}
//System.out.println("request  : "+request);
//System.out.println("request Content : "+requestContent);
String responseCode="2";
TemporaryAccount JsontempAcc= new TemporaryAccount();
TemporaryAccount dbcelectedtempAcc= new TemporaryAccount();

ResponseTemporaryAccJson responseTempAcc= new ResponseTemporaryAccJson();
String genaratedReferanceNo="";
String created_by="SAMILA";
String created_via="AGENT_APP";
boolean q_status=false;
boolean q_status2=false;
try{
	
	JsontempAcc= gson.fromJson(requestContent, TemporaryAccount.class);
	
	responseTempAcc.setCustomerMobile(JsontempAcc.getCustomerMobile());
	responseTempAcc.setCustomerNRIC(JsontempAcc.getCustomerNRIC());
	LogUtil.getLog("Event").debug(logData+",,"+requestContent);
	
}catch(Exception e){
	e.printStackTrace();
	responseCode="3";
	//responseTempAcc.setCustomerResponseStatus(responseCode);
	LogUtil.getLog("Event").debug(logData+","+e.toString()+",responseCode:"+responseCode+","+requestContent);
	LogUtil.getLog("Error").debug(logData+","+e.toString()+",responseCode:"+responseCode+","+requestContent);
	
}
if(!responseCode.equals("3")){
	try{
		
		JsonController jController = new JsonController();
		String SQL="SELECT * FROM tempory_account WHERE MSISDN='"+JsontempAcc.getCustomerMobile()+"'";
		
		dbcelectedtempAcc=jController.getAccountDetail(SQL);
		
		
		//System.out.println("*****1"+dbcelectedtempAcc.getCustomerRefno());
		
		if(dbcelectedtempAcc.getCustomerRefno()==null){
			responseCode="0";
			//responseTempAcc.setCustomerResponseStatus("0");//success
			String query="";
			genaratedReferanceNo=jController.getRandomRefNo();
			try{//  
				 query="INSERT INTO tempory_account(MSISDN,NIC,CREATED_DATE,CREATED_BY,REF_NO,CREATED_VIA,STATUS,CUSTOMER_NAME,CUSTOMER_BANK,MOTHERS_NAME,ADDRESS_HOUSE_NO,ADDRESS_STREET,ADDRESS_CITY,ADDRESS_STATE,GENDER) "+
						"VALUES ('"+JsontempAcc.getCustomerMobile()+"','"+JsontempAcc.getCustomerNRIC()+"',NOW(),'"+created_by+"','"+genaratedReferanceNo+"','"+created_via+"','PENDING','"+JsontempAcc.getCustomerName()+"','"+JsontempAcc.getCustomerBank()+"','"+JsontempAcc.getCustomerMotherName()+"','"+JsontempAcc.getCustomerHouseNo()+"','"+JsontempAcc.getCustomerStreet()+"','"+JsontempAcc.getCustomerCity()+"','"+JsontempAcc.getCustomerState()+"','"+JsontempAcc.getCustomerGender()+"')";
				AccountController ac=new AccountController();
				 q_status=ac.InsertData(query);
				
			}catch(Exception e ){
				e.printStackTrace();
				LogUtil.getLog("Event").debug(logData+","+e.toString()+",insertion_error:"+query+",responseCode:"+responseCode+","+requestContent);
				LogUtil.getLog("Error").debug(logData+","+e.toString()+",insertion_error:"+query+",responseCode:"+responseCode+","+requestContent);
			}
			if(q_status== true){
				responseTempAcc.setCustomerRefno(genaratedReferanceNo);
				LogUtil.getLog("Event").debug(logData+",genaratedReferanceNo:"+genaratedReferanceNo+",insertion_success:"+query+",responseCode:"+responseCode+","+requestContent);
				
			}
			
		}else{
			//Newly Added and not in server 
			if(dbcelectedtempAcc.getCustomerReqStatus().equalsIgnoreCase("DELETED") ){
			responseCode="0";
			//responseTempAcc.setCustomerResponseStatus("0");//success
			String query="";
			genaratedReferanceNo=jController.getRandomRefNo();
			try{//  
				 /* query="INSERT INTO tempory_account(MSISDN,NIC,CREATED_DATE,CREATED_BY,REF_NO,CREATED_VIA,STATUS,CUSTOMER_NAME,CUSTOMER_BANK,MOTHERS_NAME,ADDRESS_HOUSE_NO,ADDRESS_STREET,ADDRESS_CITY,ADDRESS_STATE) "+
						"VALUES ('"+JsontempAcc.getCustomerMobile()+"','"+JsontempAcc.getCustomerNRIC()+"',NOW(),'"+created_by+"','"+genaratedReferanceNo+"','"+created_via+"','PENDING','"+JsontempAcc.getCustomerName()+"','"+JsontempAcc.getCustomerBank()+"','"+JsontempAcc.getCustomerMotherName()+"','"+JsontempAcc.getCustomerHouseNo()+"','"+JsontempAcc.getCustomerStreet()+"','"+JsontempAcc.getCustomerCity()+"','"+JsontempAcc.getCustomerState()+"')";
				 */
				 query="UPDATE tempory_account SET NIC='"+JsontempAcc.getCustomerNRIC()+"',CREATED_DATE=NOW(),CREATED_BY='"+created_by+"',REF_NO='"+genaratedReferanceNo+"',CREATED_VIA='"+created_via+"',STATUS='PENDING',CUSTOMER_NAME='"+JsontempAcc.getCustomerName()+"',CUSTOMER_BANK='"+JsontempAcc.getCustomerBank()+"',MOTHERS_NAME='"+JsontempAcc.getCustomerMotherName()+"',ADDRESS_HOUSE_NO='"+JsontempAcc.getCustomerHouseNo()+"',ADDRESS_STREET='"+JsontempAcc.getCustomerStreet()+"',ADDRESS_CITY='"+JsontempAcc.getCustomerCity()+"',ADDRESS_STATE='"+JsontempAcc.getCustomerState()+"',NID_IMAGE=null,USER_IMAGE=null,KYC_IMAGE=null WHERE MSISDN='"+JsontempAcc.getCustomerMobile()+"'";	
					
			AccountController ac=new AccountController();
				 q_status2=ac.InsertData(query);
				
			}catch(Exception e ){
				e.printStackTrace();
				LogUtil.getLog("Event").debug(logData+","+e.toString()+",insertion_error:"+query+",responseCode:"+responseCode+","+requestContent);
				LogUtil.getLog("Error").debug(logData+","+e.toString()+",insertion_error:"+query+",responseCode:"+responseCode+","+requestContent);
			}
			if(q_status2== true){
				responseTempAcc.setCustomerRefno(genaratedReferanceNo);
				LogUtil.getLog("Event").debug(logData+",genaratedReferanceNo:"+genaratedReferanceNo+",insertion_success:"+query+",responseCode:"+responseCode+","+requestContent);
				
			}
			}else{
			responseCode="1";
			//alredy have
			responseTempAcc.setCustomerRefno("XXXXX");
			LogUtil.getLog("Event").debug(logData+",,,responseCode:"+responseCode+","+requestContent);
			LogUtil.getLog("Error").debug(logData+",,,responseCode:"+responseCode+","+requestContent);
			}
		
		}
		
		
	}catch(Exception e){
		e.printStackTrace();
		responseCode="2";
		LogUtil.getLog("Event").debug(logData+",,,responseCode:"+responseCode+","+requestContent);
		LogUtil.getLog("Error").debug(logData+",,,responseCode:"+responseCode+","+requestContent);
	
	}
}
responseTempAcc.setCustomerResponseStatus(responseCode);

String jsonString = gson.toJson(responseTempAcc);

System.out.println(jsonString);
response.getWriter().write(jsonString);

%>


<%!
private String readStringContent(HttpServletRequest req) throws IOException {
    InputStream is = req.getInputStream();
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

    StringBuilder content = new StringBuilder();
    String line;
    
    while ((line = bufferedReader.readLine()) != null) {
      content.append(line);
    }
    System.out.println(content.toString());
    return content.toString();
}

%>