	
	function districList() {//org distric list
		
		var distric = document.getElementById("district");
		
		var jqxhr =
	 	    $.ajax({
	 	        url: "MainController.jsp?action=getDistrictDetail",
	 	        data: {
	 	            name : "The name",
	 	            desc : "The description"
	 	        }
	 	    })
	 	    .done  (function(response, textStatus, jqXHR)  
	 	    		{ 
	 	    			obj = JSON.parse(response.trim());	
	 	    			alert("/////" +obj);
	 	    			
	 	    			 for(var i = 0; i <= 63; i++) {
	 	    				 //alert(i);
	 	    		         var option = document.createElement('option');
	 	    		        option.text = option.value = obj.distric[i];
	 	    		        distric.add(option, 0); 
	 	    		    }
	 	    });
	}
	
	function changeDist() {//org thana list
		
		var thana = document.getElementById("dealer_org_thana");
		var district = document.getElementById("district").value;
		
		var jqxhr =
	 	    $.ajax({
	 	        url: "MainController.jsp?action=getThanaDetail&thanaVal="+thana+"&districVal="+district,
	 	        data: {
	 	            name : "The name",
	 	            desc : "The description"
	 	        }
	 	    })
	 	    .done  (function(response, textStatus, jqXHR)  
	 	    		{ 
	 	    			obj = JSON.parse(response.trim());	
	 	    			alert("****///***" +obj.thana.length);
	 	    			var optionempty = document.createElement('option');
	 	    			optionempty.text = optionempty.value = "";
	 	    			 thana.add(optionempty, 0);
	 	    			 for(var i = 0; i < obj.thana.length; i++) {
	 	    				 //alert(i);
	 	    		        var option = document.createElement('option');
	 	    		        option.text = option.value = obj.thana[i];
	 	    		        thana.add(option, 0); 
	 	    		    }   
	 	    });
		
	}
	
	function getUnion(){//org union list
		
		var thana = document.getElementById("dealer_org_thana").value;
		var union =  document.getElementById("union");
		alert("thana"+thana);
		
		
		var jqxhr =
	 	    $.ajax({
	 	        url: "MainController.jsp?action=getUnion&thanaVal="+thana,
	 	        data: {
	 	            name : "The name",
	 	            desc : "The description"
	 	        }
	 	    })
	 	    .done  (function(response, textStatus, jqXHR)  
	 	    		{ 
	 	    			obj = JSON.parse(response.trim());
	 	    			alert("****/uuuu//***" +obj);
	 	    			
		    			  for(var i = 0; i <= obj.union.length; i++) {
		    				 //alert(i);
		    		        var option = document.createElement('option');
		    		        option.text = option.value = obj.union[i];
		    		        union.add(option, 0); 
		    		    }    
	 	    			
	 	    			
	 	    });
	}
	
	
	function personDistricList(){//peronal distric
		
	var personal_distric = document.getElementById("personal_district");
		
		var jqxhr =
	 	    $.ajax({
	 	        url: "MainController.jsp?action=getPersonDistrictDetail",
	 	        data: {
	 	            name : "The name",
	 	            desc : "The description"
	 	        }
	 	    })
	 	    .done  (function(response, textStatus, jqXHR)  
	 	    		{ 
	 	    			obj = JSON.parse(response.trim());	
	 	    			alert("/////" +obj);
	 	    			
	 	    			 for(var i = 0; i <= 63; i++) {
	 	    				 //alert(i);
	 	    		        var option = document.createElement('option');
	 	    		        option.text = option.value = obj.personaldistric[i];
	 	    		        personal_distric.add(option, 0); 
	 	    		    }
	 	    });
		
	}
	
	function changePersonalDistric(){//peron thana
		
		var personal_distric = document.getElementById("personal_district").value;
		var person_thana = document.getElementById("person_thana");
		
		var jqxhr =
	 	    $.ajax({
	 	        url: "MainController.jsp?action=getPersonalThanaDetail&personDistricVal="+personal_distric,
	 	        data: {
	 	            name : "The name",
	 	            desc : "The description"
	 	        }
	 	    })
	 	    .done  (function(response, textStatus, jqXHR)  
	 	    		{ 
	 	    			obj = JSON.parse(response.trim());	
	 	    			alert("****///***" +obj.person_thana.length);
	 	    			
	 	    			 for(var i = 0; i <= obj.person_thana.length; i++) {
	 	    				 //alert(i);
	 	    		        var option = document.createElement('option');
	 	    		        option.text = option.value = obj.person_thana[i];
	 	    		       person_thana.add(option, 0); 
	 	    		    }   
	 	    });
	}
	
	function changePersonThana(){//person union
		
		var personThanaVal = document.getElementById("person_thana").value;
		var person_union = document.getElementById("person_union");
		
		var jqxhr =
	 	    $.ajax({
	 	        url: "MainController.jsp?action=getPersonUnion&personalThanaVal="+personThanaVal,
	 	        data: {
	 	            name : "The name",
	 	            desc : "The description"
	 	        }
	 	    })
	 	    .done  (function(response, textStatus, jqXHR)  
	 	    		{ 
	 	    			obj = JSON.parse(response.trim());
	 	    			
		    			  for(var i = 0; i <= obj.person_union.length; i++) {
		    		        var option = document.createElement('option');
		    		        option.text = option.value = obj.person_union[i];
		    		        person_union.add(option, 0); 
		    		    }    
	 	    });
		
	}
	
	
	function emergencyDistricList(){//emergy distric list
		
		var emerg_distric = document.getElementById("emerg_district");
		
		var jqxhr =
	 	    $.ajax({
	 	        url: "MainController.jsp?action=getEmergyDistrictDetail",
	 	        data: {
	 	            name : "The name",
	 	            desc : "The description"
	 	        }
	 	    })
	 	    .done  (function(response, textStatus, jqXHR)  
	 	    		{ 
	 	    			obj = JSON.parse(response.trim());	
	 	    			alert("/////" +obj);
	 	    			
	 	    			 for(var i = 0; i <= 63; i++) {
	 	    				 //alert(i);
	 	    		        var option = document.createElement('option');
	 	    		        option.text = option.value = obj.emergyDistric[i];
	 	    		        emerg_distric.add(option, 0); 
	 	    		    }
	 	    });
	}
	
	function emergyChangeDistric(){//emergy thana List
		
		var emergyDistricVal = document.getElementById("emerg_district").value;
		var emergyThana = document.getElementById("emergy_thana");
		
		var jqxhr =
	 	    $.ajax({
	 	        url: "MainController.jsp?action=getEmergylThanaDetail&emergyDistricVal="+emergyDistricVal,
	 	        data: {
	 	            name : "The name",
	 	            desc : "The description"
	 	        }
	 	    })
	 	    .done  (function(response, textStatus, jqXHR)  
	 	    		{ 
	 	    			obj = JSON.parse(response.trim());	
	 	    			alert("****///***" +obj.emergy_thana.length);
	 	    			
	 	    			 for(var i = 0; i <= obj.emergy_thana.length; i++) {
	 	    				 //alert(i);
	 	    		        var option = document.createElement('option');
	 	    		        option.text = option.value = obj.emergy_thana[i];
	 	    		        emergyThana.add(option, 0); 
	 	    		    }   
	 	    });
	}
	
	function emergyChangeThana(){// emergy union list
		
		var emergyThnaVal = document.getElementById("emergy_thana").value;
		var emergy_union = document.getElementById("emergy_union");
		
		var jqxhr =
	 	    $.ajax({
	 	        url: "MainController.jsp?action=getEmergyUnion&emergyThanaVal="+emergyThnaVal,
	 	        data: {
	 	            name : "The name",
	 	            desc : "The description"
	 	        }
	 	    })
	 	    .done  (function(response, textStatus, jqXHR)  
	 	    		{ 
	 	    			obj = JSON.parse(response.trim());
	 	    			
		    			  for(var i = 0; i <= obj.emergyUnion.length; i++) {
		    		        var option = document.createElement('option');
		    		        option.text = option.value = obj.emergyUnion[i];
		    		        emergy_union.add(option, 0); 
		    		    }    
	 	    });
	}
	
	function nomineeDistricList(){//nominee distric
		
		var nominneDistric = document.getElementById("nominee_district");
		
		var jqxhr =
	 	    $.ajax({
	 	        url: "MainController.jsp?action=getNomineeDistrictDetail",
	 	        data: {
	 	            name : "The name",
	 	            desc : "The description"
	 	        }
	 	    })
	 	    .done  (function(response, textStatus, jqXHR)  
	 	    		{ 
	 	    			obj = JSON.parse(response.trim());	
	 	    			alert("/////" +obj);
	 	    			
	 	    			 for(var i = 0; i <= 63; i++) {
	 	    				 //alert(i);
	 	    		        var option = document.createElement('option');
	 	    		        option.text = option.value = obj.nomineeDistric[i];
	 	    		        nominneDistric.add(option, 0); 
	 	    		    }
	 	    });
	}
	
	function changeNomiDistric(){// nominee thana
		
		var nomineeDistricVal = document.getElementById("nominee_district").value;
		var nomineeThana = document.getElementById("nominee_thana");
		
		var jqxhr =
	 	    $.ajax({
	 	        url: "MainController.jsp?action=getNomineeThanaDetail&nomineeDistricVal="+nomineeDistricVal,
	 	        data: {
	 	            name : "The name",
	 	            desc : "The description"
	 	        }
	 	    })
	 	    .done  (function(response, textStatus, jqXHR)  
	 	    		{ 
	 	    			obj = JSON.parse(response.trim());	
	 	    			alert("****///***" +obj.nominee_thana.length);
	 	    			
	 	    			 for(var i = 0; i <= obj.nominee_thana.length; i++) {
	 	    				 //alert(i);
	 	    		        var option = document.createElement('option');
	 	    		        option.text = option.value = obj.nominee_thana[i];
	 	    		       nomineeThana.add(option, 0); 
	 	    		    }   
	 	    });
	}
	
	function changeNomineeThana(){//nominee union
		
		var changeNomineeThana = document.getElementById("nominee_thana").value;
		var nominee_union = document.getElementById("nominee_union");
		
		var jqxhr =
	 	    $.ajax({
	 	        url: "MainController.jsp?action=getNomineeUnion&nomineeThanaVal="+changeNomineeThana,
	 	        data: {
	 	            name : "The name",
	 	            desc : "The description"
	 	        }
	 	    })
	 	    .done  (function(response, textStatus, jqXHR)  
	 	    		{ 
	 	    			obj = JSON.parse(response.trim());
	 	    			
		    			  for(var i = 0; i <= obj.nomineeUnion.length; i++) {
		    		        var option = document.createElement('option');
		    		        option.text = option.value = obj.nomineeUnion[i];
		    		        nominee_union.add(option, 0); 
		    		    }    
	 	    });
		
	}
	
