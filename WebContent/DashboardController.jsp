<%@page import="com.evidu.bank.entitiy.Account"%>
<%@page import="com.evidu.bank.entitiy.UserInfo"%>
<%@page import="com.evidu.bank.sva.MainController"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.io.*"%>
<%@ page import="com.evidu.bank.sva.*"%>
<%@page import="java.util.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.apache.commons.codec.binary.Base64"%>

<%
	String action = "" + request.getParameter("action");
	//System.out.println("action"+action);
	DashboardController dc = new DashboardController();
	UserInfo userData = new UserInfo();
	String query;
	
	userData= (UserInfo)session.getAttribute("current_user");
	int user_id=Integer.parseInt(userData.getUser_id());
	
	query="SELECT * FROM `user_login` WHERE id='"+user_id+"'";//get current user branch code
	userData = dc.getlogCurrentUserDetails(query);
	
	String currentUserBranchCode = userData.getBranch_code();//current branch
	
	if(action.equals("getTotalPendingCounts")){
		
		List<Integer> countList= new ArrayList<Integer>();
		List<String> querylist = new ArrayList<String>();
		
		querylist.add("SELECT COUNT(*) AS COUNT FROM `account` WHERE `BRANCH_CODE` = '"+currentUserBranchCode.trim()+"' AND STATUS='PENDING' AND BRANCH_MAKER_STATUS IS NULL");
		
		for(int i=0;i<querylist.size();i++){
		   //	 System.out.println("querylist ********: "+querylist.get(i));  
		      }
		
		countList = dc.getSummeryFromMysqlDB(querylist);
		JSONObject obj = new JSONObject();	
		JSONArray list = new JSONArray();
		if(countList.size()>0){
			for(Integer i :countList){
			list.add(i);
			}
		}
	
		obj.put("countList", list);
		//System.out.print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+obj);
		out.print(obj);    
		
	}
	
	
	if(action.equals("getPendingCustomers")){
		
		String appStatus = ""+request.getParameter("status");
		List<Account> approvedList = new ArrayList<Account>();
		String sendresponse = "false";
		
		query="SELECT `id`,`ACCOUNT_NAME`,`NAME`,`MOBILE_NUMBER`,`STATUS` FROM `account` WHERE `BRANCH_CODE`='"+currentUserBranchCode.trim()+"' AND STATUS='"+appStatus+"' AND BRANCH_MAKER_STATUS IS NULL";
		approvedList = dc.getPendingCustomerList(query);
		System.out.print(">>>>>>>>>>>>>******>>>>>>**>>>>>>>>>>>>"+approvedList.size());
		session.setAttribute("approList", approvedList);
		
		if(approvedList.size()>0){
			sendresponse="true";
		}
		out.print(sendresponse);
		
	}
	
	if(action.equals("getCustomerDetail")){
		
		String mobile = ""+request.getParameter("msisdn");
		String status = ""+request.getParameter("status");
		System.out.println("mobile : "+mobile);
		System.out.println("status : "+status);
		JSONObject obj = new JSONObject();	
	    JSONArray list = new JSONArray();
	    
	    try{
			query = "SELECT `account`.`id`,`account`.`ACCOUNT_NAME`, `account`.`MOBILE_NUMBER`,`account`.`NAME`,`account`.`FATHER_NAME`,`account`.`MOTHER_NAME`,`account`.`SPOUSE_NAME`,"+ 
					"`account`.`DATE_OF_BIRTH`,`account`.`PROFESSION`,`account`.`SOURCE_OF_INCOME`,`account`.`OTHER_ACC_BANK_NAME`,`account`.`OTHER_ACC_NUMBER`,"+
					"`account`.`OTHER_BRANCH_NAME`,`account`.`ID_TYPE`,`account`.`NATIONAL_ID`,"+
					"`account`.`MAILING_ADDRESS_HOUSE_NO`, `account`.`MAILING_ADDRESS_STREET`,`account`.`MAILING_ADDRESS_CITY`,`account`.`MAILING_ADDRESS_STATE`,"+
					"`account`.`PERMANENT_ADDRESS_HOUSE_NO`,`account`.`PERMANENT_ADDRESS_STREET`,`account`.`PERMANENT_ADDRESS_CITY`,`account`.`PERMANENT_ADDRESS_STATE`,"+
					"`account`.`ANTICIPATED_AMOUNT`, `account`.`NOMINEE_NAME`,`account`.`NOMINEE_BRANCH_NAME`,`account`.`NOMINEE_FATHERS_NAME`,`account`.`NOMINEE_MOTHERS_NAME`,"+
					"`account`.`NOMINEE_ADDRESS_HOUSE_NO`,`account`.`NOMINEE_ADDRESS_STEET`,`account`.`NOMINEE_ADDRESS_CITY`,`account`.`NOMINEE_ADDRESS_STATE`,"+
					"`account`.`NOMINEE_CONTACT_NO`, `account`.`AUTHORIZED_ID_VERIFIED`,`account`.`AUTHORIZED_INTERVIW_TAKEN` ,"+ 
					"`account_user_image`.`NIC_IMAGE`,`account_user_image`.`USER_IMAGE` FROM `account` INNER JOIN `account_user_image`"+ 
					"ON `account`.`MOBILE_NUMBER`= `account_user_image`.`MOBILE_NUMBER` WHERE `account`.`MOBILE_NUMBER`='"+mobile+"' AND `BRANCH_CODE`='"+currentUserBranchCode.trim()+"'";
			
			 List<BOApprovedStatus> detailslist = new ArrayList<BOApprovedStatus>();
			 BOApprovedStatus bO = new BOApprovedStatus();
			 
			 detailslist = dc.getAllCustomerList(query);
			 bO=detailslist.get(0);
			 System.out.println("status : "+bO.getAccountName());
		     if(detailslist.size()>0){
		    	 System.out.println("detailslist : "+detailslist.size());
		    	list.add(bO.getId());
		    	list.add(bO.getAccountName());
		    	list.add(bO.getMobileNo());
		    	list.add(bO.getCutomer());
		    	list.add(bO.getFatherName());
		    	list.add(bO.getMotherName());
		    	list.add(bO.getSpouseName());
		    	list.add(bO.getBirthday());
		    	list.add(bO.getProfession());
		    	list.add(bO.getSourceIncome());
		    	list.add(bO.getOtherBankName());
		    	list.add(bO.getOtherAccountNo());
		    	list.add(bO.getOtherBranchName());
		    	list.add(bO.getIdType());
		    	list.add(bO.getIdNumber());
		    	list.add(bO.getMailHome());
		    	list.add(bO.getMailStreet());
		    	
		    	list.add(bO.getMailCity());
		    	list.add(bO.getMailstate());
		    	list.add(bO.getPermentHome());
		    	list.add(bO.getPermanatStreet());
		    	list.add(bO.getPermentCity());
		    	list.add(bO.getPermanatState());
		    	list.add(bO.getAnticipatedAmount());
		    	list.add(bO.getNomineeName());
		    	list.add(bO.getNomineeBranch());
		    	list.add(bO.getNomineeFather());
		    	
		    	list.add(bO.getNomineeMother());
		    	list.add(bO.getNomineeHome());
		    	list.add(bO.getNomineeFather());
		    	list.add(bO.getNomineeStreet());
		    	list.add(bO.getNomineeCity());
		    	list.add(bO.getNomineeState());
		    	list.add(bO.getNomineeContactNumber());
		    	list.add(bO.getClientIdVerfy());
		    	list.add(bO.getFaceInterview());
		    	
		    	}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		 obj.put("CusomerDetail", list);
	  	 out.println(obj);
		
	}
	
	
	if(action.equals("getUserImage")){
		String msisdn=""+request.getParameter("image_id");
		 
		  query="SELECT USER_IMAGE FROM account_user_image WHERE MOBILE_NUMBER='"+msisdn+"'";
		 //System.out.print(">>>>"+msisdn);
		 byte[] imgData = null;
		 AccountController acc= new AccountController();
		 imgData=dc.getImageData(query);
		 
		 
		 JSONObject obj = new JSONObject(); 
		 JSONArray list = new JSONArray();
		 String base64 = Base64.encodeBase64String(imgData);
		 //String base64 = Base64.getEncoder().encodeToString(imgData);
		 
		 obj.put("imgData", base64);
		// System.out.print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+base64);
		out.print(obj);    
		}
	
	
	if(action.equals("getNicImage")){
		String msisdn=""+request.getParameter("image_id");
		 
		  query="SELECT NIC_IMAGE FROM account_user_image WHERE MOBILE_NUMBER='"+msisdn+"'";
		 //System.out.print(">>>>"+msisdn);
		 byte[] imgData = null;
		 AccountController acc= new AccountController();
		 imgData=dc.getImageData(query);
		 
		 
		 JSONObject obj = new JSONObject(); 
		 JSONArray list = new JSONArray();
		 String base64 = Base64.encodeBase64String(imgData);
		 //String base64 = Base64.getEncoder().encodeToString(imgData);
		 
		 obj.put("imgData", base64);
		// System.out.print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+base64);
		out.print(obj);    
		}
	
	
	if(action.equals("getcheckboxStatus")){
			
			String msisdn = ""+request.getParameter("mobile");
			String CheckStatus = ""+request.getParameter("checkStatus");
			String uniqueId = ""+request.getParameter("id");
			String status = ""+request.getParameter("showStatus");
			
			 if(CheckStatus.equals("true")){
				
				query="UPDATE account SET BRANCH_MAKER_STATUS='CHECKED', MODIFIED_DATE=NOW() ,BRANCH_MAKER_ID='"+currentUserBranchCode.trim()+"' WHERE MOBILE_NUMBER='"+msisdn+"' AND id='"+uniqueId+"'";
				boolean b = dc.checkboxUpdate(query);
				query="SELECT `id`,`ACCOUNT_NAME`,`NAME`,`MOBILE_NUMBER`,`STATUS` FROM `account` WHERE `BRANCH_CODE`='"+currentUserBranchCode.trim()+"' AND STATUS='PENDING' AND BRANCH_MAKER_STATUS IS NULL";
				List<Account> approvedList = new ArrayList<Account>();
				String sendresponse = "false";
				approvedList = dc.getPendingCustomerList(query);
				//System.out.print(">>>>>>>>>>>>>******>>>>>>**>>>>>>>>>>>>"+approvedList.size());
				session.setAttribute("approList", approvedList);
				
				if(b){
					out.print(true);
				} 
			} 
			
		}

%>