<%@page import="com.evidu.bank.entitiy.*"%>
<!DOCTYPE html>

<%
response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
//response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
int timeout = session.getMaxInactiveInterval();
response.setHeader("Refresh", timeout + "; URL = index.jsp");
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility

%>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->

<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Bootstrap Stylesheet -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" media="all">

<!-- jquery-ui Stylesheets -->
<link rel="stylesheet" href="assets/jui/css/jquery-ui.css" media="screen">
<link rel="stylesheet" href="assets/jui/jquery-ui.custom.css" media="screen">
<link rel="stylesheet" href="assets/jui/timepicker/jquery-ui-timepicker.css" media="screen">

<!-- Uniform Stylesheet -->
<link rel="stylesheet" href="plugins/uniform/css/uniform.default.css" media="screen">

<!-- Plugin Stylsheets first to ease overrides -->

<!-- Wizard Stylesheet -->
<link rel="stylesheet" href="custom-plugins/wizard/wizard.css">

<!-- End Plugin Stylesheets -->

<!-- Main Layout Stylesheet -->
<link rel="stylesheet" href="assets/css/fonts/icomoon/style.css" media="screen">
<link rel="stylesheet" href="assets/css/main-style.css" media="screen">

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<link href="css/elements.css" rel="stylesheet">

<title>Tap'n Pay</title>
<meta http-equiv="imagetoolbar" content="no" />
</head>

<body data-show-sidebar-toggle-button="false" data-fixed-sidebar="false"  oncontextmenu="return false">
											
  <%
  UserInfo userData =new UserInfo();
		userData= (UserInfo)session.getAttribute("current_user");
		
		if(userData==null){
			response.sendRedirect("index.jsp");
			return;
		}
		String errCode=""+request.getParameter("st");
  %> 

    <div id="wrapper">
        <header id="header" class="navbar navbar-inverse">
            <div class="navbar-inner">
                <div class="container">
					<div class="brand-wrap pull-left">
						<div class="brand-img">
							<a class="brand" href="#">
								<img src="assets/images/evidu_logo.png" alt="" style="width: 144px;position: relative;left: -21px;" >
							</a>
						</div>
					</div>
                    
                    <div id="header-right" class="clearfix">
						<div id="nav-toggle" data-toggle="collapse" data-target="#navigation" class="collapsed">
							<i class="icon-caret-down"></i>
						</div>
						
                        <div id="header-search">
							<span id="search-toggle" data-toggle="dropdown">
								<i class="icon-search"></i>
							</span>
							<form class="navbar-search">
								<input type="text" class="search-query" placeholder="Search">
							</form>
						</div>
						<div id="dropdown-lists">
                            <div class="item-wrap">
                                <a class="item" href="#" data-toggle="dropdown">
                                    <span class="item-icon"><i class="icon-exclamation-sign"></i></span>
                                    <span class="item-label">Notifications</span>
                                    <span class="item-count">4</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-item-wrap">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>John Doe</strong> commented on your photo
                                                        <span class="time">13 minutes ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>Jane Roe</strong> commented on your photo
                                                        <span class="time">27 minutes ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>Billy John</strong> commented on your photo
                                                        <span class="time">43 minutes ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">View all notifications</a></li>
                                </ul>
                            </div>
                            <div class="item-wrap">
                                <a class="item" href="#" data-toggle="dropdown">
                                    <span class="item-icon"><i class="icon-envelope"></i></span>
                                    <span class="item-label">Messages</span>
                                    <span class="item-count">16</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-item-wrap">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>John Doe</strong><br> Hello, do you have time to go out tomorrow?
                                                        <span class="time">13 minutes ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>Jane Roe</strong><br> Hey, the reports said that you were...
                                                        <span class="time">27 minutes ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="thumbnail"><img src="assets/images/pp.jpg" alt=""></span>
                                                    <span class="details">
                                                        <strong>Billy John</strong><br> Can I borrow your new camera for taking...
                                                        <span class="time">About an hour ago</span>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="mail.html">View all messages</a></li>
                                </ul>
                            </div>
                        </div>
                        <div id="header-functions" class="pull-right">
                        	<div id="user-info" class="clearfix">
                                <span class="info">
                                	Welcome
                                    <span class="name"><%=userData.getDisplay_name() %></span>
                                </span>
                            	<div class="avatar">
                                	<a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    	<img src="assets/images/defaltUser.png" alt="Avatar">
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                    	<li><a href="profile.html"><i class="icol-user"></i> My Profile</a></li>
                                    	<li><a href="#"><i class="icol-layout"></i> My Invoices</a></li>                                        
                                        <li class="divider"></li>
                                        <li><a href="index.jsp"><i class="icol-key"></i> Logout</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="logout-ribbon">
                            	<a href="index.jsp"><i class="icon-off"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        
        <div id="content-wrap">
        	<div id="content" class="sidebar-minimized">
            	<div id="content-outer">
                	<div id="content-inner">
                    	<aside id="sidebar">
                        	<nav id="navigation" class="collapse">
                            	<ul>
                            	<li ><a href="DealerDashboard.jsp" >
                                    	<span title="Home">
                                    		<i class="icon-home"></i>
											<span class="nav-title">Dashboard</span>
                                        </span>
                                    	</a>
                                    </li>
                                	<li><a href="DEORefSearch.jsp" >
                                    	<span title="Home">
                                    		<i class="icon-business-card"></i>
											<span class="nav-title">Add Customer</span>
                                        </span>
                                    	</a>
                                    </li>
                                	<li ><a href="DEONewEntry.jsp" >
                                    	<span title="New Entry">
                                    		<i class="icon-plus-sign"></i>
											<span class="nav-title">New Entry</span>
                                        </span>
                                    	</a>
                                    </li>
                                	
                                	<li  class="active"><a href="AddDSR.jsp" >
                                    	<span title="Add DSR">
                                    		<i class="icon-add-contact"></i>
											<span class="nav-title">DSR Registration</span>
                                        </span>
                                    	</a>
                                    </li>
                                	<li><a href="AddAgent.jsp" >
                                    	<span title="Add  Agent">
                                    		<i class="icon-user"></i>
											<span class="nav-title">Agent Registration</span>
                                        </span>
                                    	</a>
                                    </li>
                                    <!-- <li><a href="AssignAgent.jsp" >
                                    	<span title="Assign Agent">
                                    		<i class="icon-table"></i>
											<span class="nav-title">Assign Agent</span>
                                        </span>
                                    	</a>
                                    </li>
                                    <li><a href="DEOReports.jsp" >
                                    	<span title="Reports">
                                    		<i class="icon-table"></i>
											<span class="nav-title">Reports</span>
                                        </span>
                                    	</a>
                                    </li> -->
                                </ul>
                            </nav>
                        </aside>

                        <div id="sidebar-separator"></div>
                        
                        <section id="main" class="clearfix">
                        	<div id="main-header" class="page-header">
                            	<ul class="breadcrumb">
                                	<li>
                                    	<i class="icon-home"></i>Tap&#39;n Pay
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    <li>
                                    	<a href="DEORefSearch.jsp">DSR</a>
                                        <span class="divider">&raquo;</span>
                                    </li>
                                    
                                </ul>
                                
                                <h1 id="main-heading">
                                	New DSR Registration
                                </h1>
                            </div>
                            
                            <div id="main-content">
                                
                               
								 <div  class="row-fluid">
								
									<div class="span12 widget">
										<div class="widget-header">
											<span class="title"><i class="icol-wand"></i>New DSR User Details</span>
											<div class="toolbar">
												<div class="btn-group">
													
												</div>
											</div>
										</div>
										<div class="widget-content form-container">
										  
											<form id="wizard-demo-2"  method="post" class="form-horizontal" action="DEORefSearchController.jsp" data-forward-only="true"  >
												
												<fieldset class="wizard-step">
													<legend class="wizard-label"><i class="icon-book"></i>PERSONAL DETAILS</legend>
													
													<div class="control-group">
														<label class="control-label">DSR Name<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="dsr_name" class="required span12">
														</div>
													</div>
													
													<div class="control-group">
														<label class="control-label">Father&#39;s Name<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="dsr_fathers_name" class="required span12">
														</div>
													</div>
													
													<div class="control-group">
														<label class="control-label">Spouse Name<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="cs_spouse_name" class="required span12">
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">Date of Birth<span class="required">*</span></label>
														<div class="controls">
														<input id="summery-data-date-from" name="summery-data-date-from" value="1996-01-01" class="required span12 ndshaatepicker-basic" type="text"
														value="">
															
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">Profession<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="cs_profession" class="required span12">
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">Source of income<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="cs_source_income" class="required span12">
														</div>
													</div>
												
													
													<!-- <div class="control-group">
														<label class="control-label">Photo ID document type<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="cs_id_type" class="required span12">
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">Number<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="cs_id_number" class="required span12">
														</div>
													</div> -->
													<div class="control-group">
													<label class="control-label"><b>Mailing Address</b></label>
													<div class="controls">
													<input type="checkbox" name="address_check" id="address_check" onclick="SameAddress()"> Same as permenant address.
													</div>
													</div>
													<div class="control-group">
														<label class="control-label">House Number<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="cs_mailling_address_h_no" id="cs_mailling_address_h_no" value="" class="required span12">
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">Street<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="cs_mailling_address_street" id="cs_mailling_address_street" value="" class="required span12">
														</div>
													</div><div class="control-group">
														<label class="control-label">City<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="cs_mailling_address_city" id="cs_mailling_address_city" value="" class="required span12">
														</div>
													</div><div class="control-group">
														<label class="control-label">State<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="cs_mailling_address_state" id="cs_mailling_address_state" value="" class="required span12">
														</div>
													</div>
													
													<div class="control-group">
														<label class="control-label">Client&#39;s anticipated amount of transaction (monthly)<span class="required">*</span></label>
														<div class="controls">
                                                            <label class="radio"><input type="radio" name="cs_monthly_amt" value="0-0.1 lak" class="required">0 - 0.1 lac </label>
                                                            <label class="radio"><input type="radio" name="cs_monthly_amt" value="0.2 lac-0.5 lac" />0.2 lac> - < 0.5 lac</label>
                                                            <label class="radio"><input type="radio" name="cs_monthly_amt" value="0.5 lac-1.00lac" />0.5 lac> - <1.00 lac</label>
                                                      	 
                                                            <label for="wizard[package]" class="error" generated="true" style="display:none"></label>
                                                        </div>
													</div>
													
												</fieldset>	
										
												<fieldset class="wizard-step">
													<legend class="wizard-label"><i class="icon-user"></i>OTHER BANK ACCOUNT</legend>
													<div class="control-group">
														<label class="control-label">Bank name</label>
														<div class="controls">
															<input type="text" name="oth_bank_name" class=" span12">
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">Account no</label>
														<div class="controls">
															<input type="text"  name="oth_account_no" class=" span12">
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">Branch name</label>
														<div class="controls">
															<input type="text"  name="oth_branch_name" class=" span12">
														</div>
													</div>
													
													
												</fieldset>
												
												
												
												<fieldset class="wizard-step">
													<legend class="wizard-label"><i class="icon-pencil"></i>NOMINEE</legend>
													<div class="control-group">
														<label class="control-label">Name of the nominee<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="cs_nominee_name" class="required span12">
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">Branch name<span class="required">*</span></label>
														<div class="controls">
														
														<select name="cs_nominee_branch_name" class="required span12">
																<option value="Dhaka">Dhaka Branch </option>
																<option value="Chittagong">Chittagong Branch </option>
																
																
															</select>
															<!-- <input type="text"  name="cs_nominee_branch_name" class="required span12"> -->
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">Father&#39;s Name<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="cs_nominee_fathers_name" class="required span12">
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">Mother&#39;s Name<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="cs_nominee_mothers_name" class="required span12">
														</div>
													</div>
													<div class="control-group">
													<label class="control-label"><b> Address</b></label>
													</div>
													<div class="control-group">
														<label class="control-label">House Number<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="cs_nominee_address_h_no" class="required span12">
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">Street<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="cs_nominee_address_street" class="required span12">
														</div>
													</div><div class="control-group">
														<label class="control-label">City<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="cs_nominee_address_city" class="required span12">
														</div>
													</div><div class="control-group">
														<label class="control-label">State<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="cs_nominee_address_state" class="required span12">
														</div>
													</div>
													
													<div class="control-group">
														<label class="control-label">Contact Number<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="cs_nominee_contact_no" class="required span12">
														</div>
													</div>
													
												</fieldset>
												
												
												
												<fieldset class="wizard-step">
													<legend class="wizard-label"><i class="icon-ok"></i>AUTHORIZATION</legend>
													
													<!-- <div class="control-group">
														<label class="control-label">Introducer Account Number/Wallet Mobile Number<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="introducer_mobile_no" class="required span12">
														</div>
													</div> -->
													
													<!-- <div class="control-group">
														<label class="control-label">Outlet/Branch Name<span class="required">*</span></label>
														<div class="controls">
															<input type="text"  name="introducer_branch_name" class="required span12">
														</div>
													</div> -->
													<div class="control-group">
														<label class="control-label">Is client&#39;s ID verified<span class="required">*</span></label>
														<div class="controls">
                                                            <label class="radio"><input type="radio" name="cs_id_verified" value="yes" class="required"> Yes</label>
                                                            <label class="radio"><input type="radio" name="cs_id_verified" value="no"  /> No</label>
                                                           
                                                            <label for="cs_id_verified" class="error" generated="true" style="display:none"></label>
                                                        </div>
													</div>
													<div class="control-group">
														<label class="control-label">Has client&#39;s face-to-face interview taken<span class="required">*</span></label>
														<div class="controls">
                                                            <label class="radio"><input type="radio" name="cs_interview" value="yes"  class="required"> Yes</label>
                                                            <label class="radio"><input type="radio" name="cs_interview" value="no" /> No</label>
                                                           
                                                            <label for="cs_interview" class="error" generated="true" style="display:none"></label>
                                                        </div>
													</div>
													
													<div class="control-group">
														<label class="control-label">Initial deposited amount<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="cs_initial_amount" class="required span12">
														</div>
													</div>
													
													
												</fieldset>
											</form>
										</div>
									
									</div>
								 <%if(errCode.equalsIgnoreCase("crr")){ 
								 //System.out.println("_____________"+errCode);
								 %>
									        
										        <script type="text/javascript">
										       
									       		 window.onload=function(){alertOnload("User account  updated successfully", "SVA", "OK");
									   			     };
									       
										        </script>
									      <%}if(errCode.equalsIgnoreCase("err")){
									    	  %>
										        <script type="text/javascript">
										        window.onload=function(){alertOnload("Error on User creation.", "SVA", "OK");
								   			     };	
								   			     </script>
									     <% } %>
                                                 <script type="text/javascript">
                                                function alertOnload(message, title, buttonText) {
													 buttonText = (buttonText == undefined) ? "Ok" : buttonText;
												    title = (title == undefined) ? "The page says:" : title;
												
												    var div = $('<div>');
												    div.html(message);
												    div.attr('title', "SVA");
												    div.dialog({
												        autoOpen: true,
												        modal: true,
												        draggable: false,
												        resizable: false,
												        buttons: [{
												            text: buttonText,
												            click: function () {
												                $(this).dialog("close");
												                div.remove();
												              
												            }
												        }]
												    });
												   
												}
                                                /* function openNewWindow1()
                                                {
                                                	var msisdn=document.getElementById("cs_mobiole_no").value;
                                                	var leftPosition, topPosition;
                                                    //Allow for borders.
                                                    leftPosition = (window.screen.width / 2) - ((400 / 2) + 10);
                                                    //Allow for title and status bars.
                                                    topPosition = (window.screen.height / 2) - ((200 / 2) + 50);
                                                   // alert(leftPosition+"###"+topPosition);
                                                	//window.open("http://103.17.181.228:8080/CRS/NIDIMG.jsp?msisdn="+msisdn,null,"screenX="+leftPosition+",screenY="+topPosition+",height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");
                                                    window.open("http://localhost:8080/SVABangladesh/NIDIMG.jsp?msisdn="+msisdn,null,"screenX="+leftPosition+",screenY="+topPosition+",height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");
                                                    
                                                } */
                                               /*  function openNewWindow2()
                                                {
                                                	var msisdn=document.getElementById("cs_mobiole_no").value;
                                                	var leftPosition, topPosition;
                                                    //Allow for borders.
                                                    leftPosition = (window.screen.width / 2) - ((400 / 2) + 10);
                                                    //Allow for title and status bars.
                                                    topPosition = (window.screen.height / 2) - ((200 / 2) + 50);
                                                   // alert(leftPosition+"###"+topPosition);
                                                	//window.open("http://103.17.181.228:8080/CRS/UserIMG.jsp?msisdn="+msisdn,null,"screenX="+leftPosition+",screenY="+topPosition+",height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");
                                                    window.open("http://localhost:8080/SVABangladesh/UserIMG.jsp?msisdn="+msisdn,null,"screenX="+leftPosition+",screenY="+topPosition+",height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");
                                                    
                                                } */
                                                function SameAddress()
                                                {
                                                	//alert("checked");
                                                   if (document.getElementById('address_check').checked) 
                                                  { 
                                                	  
                                                	   var house_num=document.getElementById('cs_permant_address_h_no').value;
                                                	   var street=document.getElementById('cs_permant_address_street').value;
                                                	   var city=document.getElementById('cs_permant_address_city').value;
                                                	   var state=document.getElementById('cs_permant_address_state').value;
                                                	  // alert(house_num);
                                                	   document.getElementById('cs_mailling_address_h_no').value=house_num;
                                                	   document.getElementById('cs_mailling_address_street').value=street;
                                                	   document.getElementById('cs_mailling_address_city').value=city ;
                                                	   document.getElementById('cs_mailling_address_state').value=state ;
                                                	   
                                                  } else{
	                                                   document.getElementById('cs_mailling_address_h_no').value="";
	                                               	   document.getElementById('cs_mailling_address_street').value="";
	                                               	   document.getElementById('cs_mailling_address_city').value="" ;
	                                               	   document.getElementById('cs_mailling_address_state').value="" ;
                                                 }
                                                }
                                                
                                                function refreash() {
													 
                                                	 document.getElementById('ref_number').value="" ;
												}
                                               
                                                </script>
								
								</div>
                                
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        
        <footer id="footer">
            <div class="footer-left">Mobile Banking Platform</div>
            <div class="footer-right"><p>Copyright 2016 - Evidu Private Limited All Rights Reserved.</p></div>
        </footer>
        
    </div>

	<!-- Core Scripts -->
	<script src="assets/js/libs/jquery-1.8.3.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/libs/jquery.placeholder.min.js"></script>
	<script src="assets/js/libs/jquery.mousewheel.min.js"></script>

    <!-- Template Script -->
    <script src="assets/js/template.js"></script>
    <script src="assets/js/setup.js"></script>

    <!-- Customizer, remove if not needed -->
    <script src="assets/js/customizer.js"></script>

    <!-- Uniform Script -->
    <script src="plugins/uniform/jquery.uniform.min.js"></script>
    
    <!-- jquery-ui Scripts -->
    <script src="assets/jui/js/jquery-ui-1.9.2.min.js"></script>
    <script src="assets/jui/jquery-ui.custom.min.js"></script>
    <script src="assets/jui/timepicker/jquery-ui-timepicker.min.js"></script>
	<script src="assets/jui/jquery.ui.touch-punch.min.js"></script>
    
    <!-- Plugin Scripts -->
    
	<!-- Validation -->
	<script src="plugins/validate/jquery.validate.min.js"></script>
	
	<!-- Wizard -->
	<script src="custom-plugins/wizard/wizard.min.js"></script>
    <script src="custom-plugins/wizard/jquery.form.min.js"></script>

   
    <!-- Demo Scripts -->
    <script src="assets/js/demo/form_wizard.js"></script>
	
	<!-- newly added js -->
	
	<script src="plugins/flot/jquery.flot.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.tooltip.min.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.pie.min.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.orderBars.min.js"></script>
	<script src="plugins/flot/plugins/jquery.flot.resize.min.js"></script>

	
	<script src="assets/js/demo/ui_comps.js"></script>
	<!--  --------------------------------------------------------->



</body>

</html>
